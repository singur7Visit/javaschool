package ru.tsystems.logiweb.common.commands;

import ru.tsystems.logiweb.common.entities.IObject;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by dsing on 13.08.14.
 */
public class Command implements ICommand{

    private String command;
    private Map<String,Serializable> params = new HashMap<>();

    @Override
    public String getCommand() {
        return command;
    }

    @Override
    public void setCommand(String command) {
        this.command = command;
    }

    @Override
    public Serializable getParam(String name) {
        return params.get(name);
    }

    @Override
    public void addParam(String name, Serializable param) {
        params.put(name,param);
    }

    @Override
    public void clearParams() {
        params.clear();
    }

    @Override
    public Map<String, Serializable> getNamedParams(String... params) {
        Map<String,Serializable> response = new HashMap<>();
        for(String param : params) {
            Serializable p;
            if(param != null && (p = this.params.get(param)) != null)
                response.put(param,p);
        }
        return response;
    }

}
