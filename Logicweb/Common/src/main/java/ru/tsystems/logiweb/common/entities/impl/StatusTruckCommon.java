package ru.tsystems.logiweb.common.entities.impl;

import ru.tsystems.logiweb.common.entities.IObject;
import ru.tsystems.logiweb.common.entities.IStatus;

/**
 * Created by dsing on 21.08.14.
 */
public class StatusTruckCommon implements IObject,IStatus {
    private long id;

    private String name;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return name;
    }
}
