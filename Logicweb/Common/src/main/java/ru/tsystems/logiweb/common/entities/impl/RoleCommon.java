package ru.tsystems.logiweb.common.entities.impl;

import ru.tsystems.logiweb.common.entities.IObject;
import ru.tsystems.logiweb.common.entities.IStatus;

/**
 * Created by dsing on 15.08.14.
 */
public class RoleCommon implements IObject,IStatus {
    private long id;

    private String name;

    private boolean manageMangers;

    private boolean manageOrders;

    private boolean manageDrivers;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isManageMangers() {
        return manageMangers;
    }

    public void setManageMangers(boolean manageMangers) {
        this.manageMangers = manageMangers;
    }

    public boolean isManageOrders() {
        return manageOrders;
    }

    public void setManageOrders(boolean manageOrders) {
        this.manageOrders = manageOrders;
    }

    public boolean isManageDrivers() {
        return manageDrivers;
    }

    public void setManageDrivers(boolean manageDrivers) {
        this.manageDrivers = manageDrivers;
    }
}
