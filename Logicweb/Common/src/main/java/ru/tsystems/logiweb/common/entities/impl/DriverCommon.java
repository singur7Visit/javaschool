package ru.tsystems.logiweb.common.entities.impl;

import ru.tsystems.logiweb.common.annotations.NonScanner;
import ru.tsystems.logiweb.common.entities.IObject;
import ru.tsystems.logiweb.common.entities.IStatus;

import java.math.BigInteger;

/**
 * Created by dsing on 15.08.14.
 */

public class DriverCommon implements IObject {
    private long id;

    private String licence;

    private String SNM;

    private String password;

    private StatusDriverCommon status;

    private TruckCommon truck;

    private OrderCommon order;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLicence() {
        return licence;
    }

    public void setLicence(String licence) {
        this.licence = licence;
    }

    public String getSNM() {
        return SNM;
    }

    public void setSNM(String SNM) {
        this.SNM = SNM;
    }

    public StatusDriverCommon getStatus() {
        return status;
    }

    public void setStatus(StatusDriverCommon status) {
        this.status = status;
    }

    public TruckCommon getTruck() {
        return truck;
    }

    public void setTruck(TruckCommon truck) {
        this.truck = truck;
    }

    public OrderCommon getOrder() {
        return order;
    }

    public void setOrder(OrderCommon order) {
        this.order = order;
    }

    @NonScanner
    public String getPassword() {
        return password;
    }

    @NonScanner
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return this.SNM;
    }
}