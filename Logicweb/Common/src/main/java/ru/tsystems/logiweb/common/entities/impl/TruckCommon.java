package ru.tsystems.logiweb.common.entities.impl;

import ru.tsystems.logiweb.common.entities.IObject;

import java.util.List;

/**
 * Created by dsing on 14.08.14.
 */

public class TruckCommon implements IObject {

    private long id;

    private String regNumber;

    private int countDrivers;

    private ClassCapacityCommon classCapacity;

    private OrderCommon order;

    private List<DriverCommon> drivers;

    private StatusTruckCommon status;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRegNumber() {
        return regNumber;
    }

    public void setRegNumber(String regNumber) {
        this.regNumber = regNumber;
    }

    public int getCountDrivers() {
        return countDrivers;
    }

    public void setCountDrivers(int countDrivers) {
        this.countDrivers = countDrivers;
    }

    public ClassCapacityCommon getClassCapacity() {
        return classCapacity;
    }

    public void setClassCapacity(ClassCapacityCommon classCapacity) {
        this.classCapacity = classCapacity;
    }

    public List<DriverCommon> getDrivers() {
        return drivers;
    }

    public void setDrivers(List<DriverCommon> drivers) {
        this.drivers = drivers;
    }

    public OrderCommon getOrder() {
        return order;
    }

    public void setOrder(OrderCommon order) {
        this.order = order;
    }

    public StatusTruckCommon getStatus() {
        return status;
    }

    public void setStatus(StatusTruckCommon status) {
        this.status = status;
    }

    @Override
    public String toString(){
        return regNumber;
    }
}
