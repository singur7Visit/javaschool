package ru.tsystems.logiweb.common.entities.impl;

import ru.tsystems.logiweb.common.entities.IObject;

/**
 * Created by dsing on 15.08.14.
 */

public class UserCommon implements IObject {

    private Long id;

    private String login;

    private String password;

    private RoleCommon role;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public RoleCommon getRole() {
        return role;
    }

    public void setRole(RoleCommon role) {
        this.role = role;
    }

    public String getPassword() {
        return password;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
