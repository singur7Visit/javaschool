package ru.tsystems.logiweb.common.commands;

import ru.tsystems.logiweb.common.entities.IObject;

import java.io.Serializable;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by dsing on 13.08.14.
 */
public interface ICommand extends Serializable {

    String getCommand();

    void setCommand(String command);

    Serializable getParam(String name);

    void addParam(String name, Serializable param);

    void clearParams();

    Map<String, Serializable> getNamedParams(String ...params);

}
