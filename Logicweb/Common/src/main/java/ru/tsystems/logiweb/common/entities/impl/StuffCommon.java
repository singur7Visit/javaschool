package ru.tsystems.logiweb.common.entities.impl;

import ru.tsystems.logiweb.common.annotations.NonScanner;
import ru.tsystems.logiweb.common.entities.IObject;

import java.math.BigDecimal;

/**
 * Created by dsing on 15.08.14.
 */
public class StuffCommon implements IObject {
    private long id;

    private String gps;

    private String name;

    private BigDecimal weight;

    private boolean status;

    private OrderCommon order;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getGps() {
        return gps;
    }

    public void setGps(String gps) {
        this.gps = gps;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    public OrderCommon getOrder() {
        return order;
    }

    public void setOrder(OrderCommon order) {
        this.order = order;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        return o!=null && o instanceof StuffCommon && ((StuffCommon)o).name != null && ((StuffCommon)o).name.equals(name);
    }

    @Override
    public String toString() {
        return name;
    }
}