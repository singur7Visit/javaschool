package ru.tsystems.logiweb.common.entities.impl;

import ru.tsystems.logiweb.common.entities.IObject;
import ru.tsystems.logiweb.common.entities.IStatus;

import java.math.BigInteger;
import java.util.List;

/**
 * Created by dsing on 15.08.14.
 */

public class OrderCommon implements IObject {
    private long id;

    private Long numOrder;

    private StatusOrderCommon status;

    private List<DriverCommon> drivers;

    private TruckCommon truck;

    private List<StuffCommon> stuffs;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Long getNumOrder() {
        return numOrder;
    }

    public void setNumOrder(Long numOrder) {
        this.numOrder = numOrder;
    }

    public StatusOrderCommon getStatus() {
        return status;
    }

    public void setStatus(StatusOrderCommon status) {
        this.status = status;
    }

    public List<DriverCommon> getDrivers() {
        return drivers;
    }

    public void setDrivers(List<DriverCommon> drivers) {
        this.drivers = drivers;
    }

    public TruckCommon getTruck() {
        return truck;
    }

    public void setTruck(TruckCommon truck) {
        this.truck = truck;
    }

    public List<StuffCommon> getStuffs() {
        return stuffs;
    }

    public void setStuffs(List<StuffCommon> stuffs) {
        this.stuffs = stuffs;
    }
}
