package ru.tsystems.logiweb.server.dao.jpaimpl;

import org.junit.Test;
import ru.tsystems.logiweb.server.entities.Driver;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import java.util.List;

/**
 * Created by dsing on 19.08.14.
 */
public class DriverDaoImplTest {
    @Test
    public void testGetDriversByLimit() throws Exception {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("Logiweb");
        EntityManager em = emf.createEntityManager();
        List<Driver> drivers = em.createQuery("SELECT d FROM Driver d",Driver.class).getResultList();
        em.close();
        emf.close();
    }
}
