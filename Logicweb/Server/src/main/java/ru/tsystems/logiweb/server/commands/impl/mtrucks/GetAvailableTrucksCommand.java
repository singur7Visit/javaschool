package ru.tsystems.logiweb.server.commands.impl.mtrucks;

import ru.tsystems.logiweb.common.commands.ICommand;
import ru.tsystems.logiweb.server.commands.IServerCommand;
import ru.tsystems.logiweb.server.scanners.ScannerObject;
import ru.tsystems.logiweb.server.service.factory.FactoryService;

import java.io.Serializable;

/**
 * Created by dsing on 22.08.14.
 */
public class GetAvailableTrucksCommand implements IServerCommand {
    @Override
    public ICommand execute(ICommand command) throws Exception {
        command.addParam("Trucks", (Serializable) ScannerObject.scanListServerObjects
                (FactoryService.getTruckService().getAvailableTrucks()));
        command.addParam("Status",Boolean.TRUE);
        return command;
    }

    @Override
    public String getName() {
        return "GetAvailableTrucks";
    }
}
