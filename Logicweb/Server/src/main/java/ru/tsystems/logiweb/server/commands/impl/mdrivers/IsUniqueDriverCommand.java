package ru.tsystems.logiweb.server.commands.impl.mdrivers;

import ru.tsystems.logiweb.common.commands.ICommand;
import ru.tsystems.logiweb.server.commands.IServerCommand;
import ru.tsystems.logiweb.server.service.factory.FactoryService;

/**
 * Created by dsing on 21.08.14.
 */
public class IsUniqueDriverCommand implements IServerCommand {
    @Override
    public ICommand execute(ICommand command) throws Exception {
        String licence = (String) command.getParam("licence");
        Boolean isUnique = FactoryService.getDriverService().isUniqueDriver(licence);
        command.addParam("isUnique",isUnique);
        return command;
    }

    @Override
    public String getName() {
        return "isUniqueDriver";
    }
}
