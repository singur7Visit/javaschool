package ru.tsystems.logiweb.server.dao;

/**
 * Created by dsing on 18.08.14.
 */
public interface GenericDAO<T, ID> {

    /**
     * Save new object in database
     * @param obj saving object
     * @return primary key object which has been saved
     */
    T insert(T obj);

    /**
     * Read object from database
     * @param id primary key object
     * @return object from database
     */
    T select(ID id);


    /**
     * Update object in database
     * @param obj
     * @return updated object
     */
    T update(T obj);

    /**
     * Remove object from database
     * @param obj
     */
    void delete(T obj);



}
