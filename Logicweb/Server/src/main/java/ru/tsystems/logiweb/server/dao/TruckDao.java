package ru.tsystems.logiweb.server.dao;

import ru.tsystems.logiweb.server.entities.ClassCapacity;
import ru.tsystems.logiweb.server.entities.StatusTruck;
import ru.tsystems.logiweb.server.entities.Truck;

import java.util.List;

/**
 * Created by dsing on 18.08.14.
 */
public interface TruckDao extends GenericDAO<Truck,Long> {
    List<Truck> getTrucksByLimit(int from, int to);

    List<Truck> getTrucksByLimitSearch(int from, int to, String search);

    Truck createTruck(Truck truck);

    void removeTruck(Truck truck);

    Truck findByNumber(String number);

    StatusTruck findStatusByName(String nameStatus);

    List<ClassCapacity> getAllClassesCapacity();

    List<Truck> getAvailableTruck();
}
