package ru.tsystems.logiweb.server.commands.impl.mdrivers;

import ru.tsystems.logiweb.common.commands.ICommand;
import ru.tsystems.logiweb.common.entities.IObject;
import ru.tsystems.logiweb.server.commands.IServerCommand;
import ru.tsystems.logiweb.server.scanners.ScannerObject;
import ru.tsystems.logiweb.server.service.factory.FactoryService;

import java.io.Serializable;
import java.util.List;

/**
 * Created by dsing on 19.08.14.
 */
public class GetAllDriverSearchCommand implements IServerCommand {
    @Override
    public ICommand execute(ICommand command) throws Exception {
        int from = (int) command.getParam("from");
        int to = (int) command.getParam("to");
        String searchWord = (String) command.getParam("SearchWord");
        List drivers = FactoryService.getDriverService().searchDrivers(from,to,searchWord);
        List<IObject> driverCommons = ScannerObject.scanListServerObjects(drivers);
        command.addParam("Drivers", (Serializable) driverCommons);
        command.addParam("Status",Boolean.TRUE);
        return command;
    }

    @Override
    public String getName() {
        return "GetAllDriversSearch";
    }
}
