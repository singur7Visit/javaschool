package ru.tsystems.logiweb.server.entities;

import javax.persistence.*;

/**
 * Created by dsing on 14.08.14.
 */

@Entity
@Table(name = "classes_capacity")
public class ClassCapacity implements IServerStatus {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private long id;

    @Column(name="name")
    private String name;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
