package ru.tsystems.logiweb.server.dao.jpaimpl;

import ru.tsystems.logiweb.server.dao.UserDao;
import ru.tsystems.logiweb.server.entities.Role;
import ru.tsystems.logiweb.server.entities.User;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * Created by dsing on 18.08.14.
 */
public class UserDaoImpl extends JpaDAO<User,Long> implements UserDao {

    public UserDaoImpl(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public User getUserByLoginPassword(final String login, final String password) {
        return getSingleResult(entityManager.createQuery(
                "SELECT u FROM User u WHERE u.login = :login AND u.password = :password", User.class)
                .setParameter("login", login)
                .setParameter("password", password)
                .getResultList());
    }

    @Override
    public User createUser(User user) {
        return insert(user);
    }

    @Override
    public void removeUser(User user) {
        delete(user);
    }

    @Override
    public User findByLogin(String login) {
        return getSingleResult(entityManager.createQuery("SELECT u FROM User u WHERE u.login = :login", User.class)
                .setParameter("login", login)
                .getResultList());
    }

    @Override
    public Role getRoleByName(String name) {
        List<Role> resilt = entityManager.createQuery("SELECT r FROM Role r WHERE r.name = :name",Role.class)
                .setParameter("name",name)
                .getResultList();
        return resilt == null || resilt.isEmpty() ? null : resilt.get(0);
    }


}
