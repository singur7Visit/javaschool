package ru.tsystems.logiweb.server.commands.impl.mtrucks;

import ru.tsystems.logiweb.common.commands.ICommand;
import ru.tsystems.logiweb.common.entities.impl.ClassCapacityCommon;
import ru.tsystems.logiweb.server.commands.IServerCommand;
import ru.tsystems.logiweb.server.entities.ClassCapacity;
import ru.tsystems.logiweb.server.scanners.ScannerObject;
import ru.tsystems.logiweb.server.service.factory.FactoryService;

import java.io.Serializable;
import java.util.List;

/**
 * Created by dsing on 21.08.14.
 */
public class GetAllClassesCommand implements IServerCommand {
    @Override
    public ICommand execute(ICommand command) throws Exception {
        List<ClassCapacity> classes = FactoryService.getTruckService().getClasses();
        List<ClassCapacityCommon> classesCommon = ScannerObject.scanListServerObjects(classes);
        command.addParam("classes", (Serializable) classesCommon);
        command.addParam("Status",Boolean.TRUE);
        return command;
    }

    @Override
    public String getName() {
        return "GetAllClassCapacity";
    }
}
