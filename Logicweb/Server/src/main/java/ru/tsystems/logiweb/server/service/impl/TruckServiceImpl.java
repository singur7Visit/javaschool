package ru.tsystems.logiweb.server.service.impl;

import ru.tsystems.logiweb.server.dao.TruckDao;
import ru.tsystems.logiweb.server.dao.factory.FactoryJpaDAO;
import ru.tsystems.logiweb.server.entities.ClassCapacity;
import ru.tsystems.logiweb.server.entities.StatusTruck;
import ru.tsystems.logiweb.server.entities.Truck;
import ru.tsystems.logiweb.server.manager.PersistenceManager;
import ru.tsystems.logiweb.server.service.TruckService;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * Created by dsing on 21.08.14.
 */
public class TruckServiceImpl implements TruckService {
    @Override
    public Truck createTruck(Truck truck) {
        EntityManager em = PersistenceManager.getInstance().getEntityManager();
        em.getTransaction().begin();
        TruckDao truckDao = FactoryJpaDAO.getTruckDAO(em);
        if(!isUniqueTruck(truck.getRegNumber())) {
            em.getTransaction().rollback();
            return null;
        }
        StatusTruck statusTruck = truckDao.findStatusByName("free");
        truck.setStatus(statusTruck);
        Truck newTruck = truckDao.createTruck(truck);
        if(newTruck != null)
            em.getTransaction().commit();
        else {
            em.getTransaction().rollback();
            return null;
        }
        return newTruck;
    }

    @Override
    public boolean removeTruck(Truck truck) {
        EntityManager em = PersistenceManager.getInstance().getEntityManager();
        em.getTransaction().begin();
        TruckDao truckDao = FactoryJpaDAO.getTruckDAO(em);
        Truck foundTruck = truckDao.select(truck.getId());
        if(foundTruck == null) {
            em.getTransaction().rollback();
            return false;
        }
        truckDao.removeTruck(foundTruck);
        em.getTransaction().commit();
        return true;
    }

    @Override
    public boolean isUniqueTruck(String regNumber) {
        return FactoryJpaDAO.getTruckDAO(PersistenceManager.getInstance().getEntityManager())
                .findByNumber(regNumber) == null;
    }

    @Override
    public List<Truck> searchTrucks(int from, int to, String searchLicence) {
        return FactoryJpaDAO.getTruckDAO(PersistenceManager.getInstance().getEntityManager())
                .getTrucksByLimitSearch(from, to, searchLicence);
    }

    @Override
    public List<Truck> getAllTrucks(int from, int to) {
        return FactoryJpaDAO.getTruckDAO(PersistenceManager.getInstance().getEntityManager())
                .getTrucksByLimit(from, to);
    }

    @Override
    public List<ClassCapacity> getClasses() {
        return FactoryJpaDAO.getTruckDAO(PersistenceManager.getInstance().getEntityManager())
                .getAllClassesCapacity();
    }

    @Override
    public List<Truck> getAvailableTrucks() {
        return FactoryJpaDAO.getTruckDAO(PersistenceManager.getInstance().getEntityManager())
                .getAvailableTruck();
    }
}
