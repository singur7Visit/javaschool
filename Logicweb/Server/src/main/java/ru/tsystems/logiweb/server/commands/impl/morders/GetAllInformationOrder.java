package ru.tsystems.logiweb.server.commands.impl.morders;

import ru.tsystems.logiweb.common.commands.ICommand;
import ru.tsystems.logiweb.common.entities.impl.OrderCommon;
import ru.tsystems.logiweb.common.entities.impl.TruckCommon;
import ru.tsystems.logiweb.server.commands.IServerCommand;
import ru.tsystems.logiweb.server.entities.Order;
import ru.tsystems.logiweb.server.scanners.ScannerObject;
import ru.tsystems.logiweb.server.service.factory.FactoryService;

/**
 * Created by dsing on 24.08.14.
 */
public class GetAllInformationOrder implements IServerCommand {
    @Override
    public ICommand execute(ICommand command) throws Exception {
        OrderCommon orderCommon = (OrderCommon) command.getParam("Order");
        Order order = FactoryService.getOrderService().getOrderById(orderCommon.getId());
        OrderCommon fullOrder = ScannerObject.scanServerObject(order, OrderCommon.class);
        fullOrder.setStuffs(ScannerObject.scanListServerObjects(order.getStuffs()));
        fullOrder.setDrivers(ScannerObject.scanListServerObjects(order.getDrivers()));
        fullOrder.setTruck(ScannerObject.scanServerObject(order.getTruck(), TruckCommon.class));
        command.addParam("Order",fullOrder);
        command.addParam("Status",true);
        return command;
    }

    @Override
    public String getName() {
        return "AllInformationOrder";
    }
}
