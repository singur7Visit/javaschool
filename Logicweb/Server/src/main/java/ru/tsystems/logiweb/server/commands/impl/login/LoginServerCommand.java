package ru.tsystems.logiweb.server.commands.impl.login;

import ru.tsystems.logiweb.common.commands.ICommand;
import ru.tsystems.logiweb.common.entities.impl.UserCommon;
import ru.tsystems.logiweb.server.commands.IServerCommand;
import ru.tsystems.logiweb.server.entities.User;
import ru.tsystems.logiweb.server.scanners.ScannerObject;
import ru.tsystems.logiweb.server.service.factory.FactoryService;

/**
 * Created by dsing on 18.08.14.
 */
public class LoginServerCommand implements IServerCommand {
    @Override
    public ICommand execute(ICommand command) throws Exception{
        UserCommon userCommon = (UserCommon) command.getParam("User");
        User user = FactoryService.getUserService().login(userCommon.getLogin(),userCommon.getPassword());
        UserCommon newUser = ScannerObject.scanServerObject(user, UserCommon.class);
        command.addParam("User",newUser);
        command.addParam("Status",Boolean.TRUE);
        return command;
    }

    @Override
    public String getName() {
        return "login";
    }
}
