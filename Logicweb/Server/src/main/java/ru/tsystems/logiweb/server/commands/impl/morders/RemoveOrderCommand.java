package ru.tsystems.logiweb.server.commands.impl.morders;

import ru.tsystems.logiweb.common.commands.ICommand;
import ru.tsystems.logiweb.common.entities.impl.OrderCommon;
import ru.tsystems.logiweb.server.commands.IServerCommand;
import ru.tsystems.logiweb.server.entities.Order;
import ru.tsystems.logiweb.server.scanners.ScannerObject;
import ru.tsystems.logiweb.server.service.factory.FactoryService;

/**
 * Created by dsing on 24.08.14.
 */
public class RemoveOrderCommand implements IServerCommand {
    @Override
    public ICommand execute(ICommand command) throws Exception {
        OrderCommon orderCommon = (OrderCommon) command.getParam("Order");
        Order order = ScannerObject.scanCommonObject(orderCommon, Order.class);
        Boolean response = FactoryService.getOrderService().removeOrder(order);
        command.addParam("Status",response);
        return command;
    }

    @Override
    public String getName() {
        return "RemoveOrder";
    }
}
