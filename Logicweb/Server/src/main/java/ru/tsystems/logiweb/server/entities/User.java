package ru.tsystems.logiweb.server.entities;

import javax.persistence.*;

/**
 * Created by dsing on 15.08.14.
 */

@Entity
@Table(name = "users")
public class User implements IServerObject{
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private Long id;

    @Column(name = "login")
    private String login;

    @Column(name="password")
    private String password;

    @ManyToOne
    @JoinColumn(name="id_role")
    private Role role;

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Role getRole() {
        return role;
    }

    public void setRole(Role role) {
        this.role = role;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
}
