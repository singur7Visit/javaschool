package ru.tsystems.logiweb.server.dao.jpaimpl;

import ru.tsystems.logiweb.server.dao.OrderDao;
import ru.tsystems.logiweb.server.entities.Order;
import ru.tsystems.logiweb.server.entities.StatusOrder;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * Created by dsing on 22.08.14.
 */
public class OrderDaoImpl extends JpaDAO<Order,Long> implements OrderDao {

    public OrderDaoImpl(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public List<Order> getOrdersByLimit(int from, int to) {
        return entityManager.createQuery("SELECT o FROM Order o", Order.class)
                .setFirstResult(from)
                .setMaxResults(to)
                .getResultList();
    }

    @Override
    public List<Order> getOrdersByLimitSearch(int from, int to, String search) {
        return entityManager.createQuery("SELECT o FROM Order o WHERE o.num_order LIKE :number", Order.class)
                .setParameter("number","%" + search + "%")
                .setFirstResult(from)
                .setMaxResults(to)
                .getResultList();
    }

    @Override
    public Order createOrder(Order order) {
        return insert(order);
    }

    @Override
    public Order updateOrder(Order order) {
        return update(order);
    }

    @Override
    public void removeOrder(Order order) {
        delete(order);
    }

    @Override
    public Order findByNumber(Long number) {
        return getSingleResult(entityManager.createQuery("SELECT o FROM Order o WHERE o.regNumber = :number", Order.class)
                .setParameter("number", number)
                .getResultList());
    }

    @Override
    public StatusOrder findStatusByName(String nameStatus) {
        List<StatusOrder> resilt = entityManager.createQuery("SELECT s FROM StatusOrder s WHERE s.name = :name",StatusOrder.class)
                .setParameter("name",nameStatus)
                .getResultList();
        return resilt == null || resilt.isEmpty() ? null : resilt.get(0);
    }

    @Override
    public Order getOrderById(long id) {
        return select(id);
    }
}
