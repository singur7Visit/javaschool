package ru.tsystems.logiweb.server.dao;

import ru.tsystems.logiweb.server.entities.Driver;
import ru.tsystems.logiweb.server.entities.StatusDriver;

import java.util.List;

/**
 * Created by dsing on 19.08.14.
 */
public interface DriverDao extends GenericDAO<Driver,Long> {
    List<Driver> getDriversByLimit(int from, int to);

    List<Driver> getDriversByLimitSearch(int from, int to, String search);

    Driver createDriver(Driver driver);

    void removeDriver(Driver driver);

    Driver findByLicence(String licence);

    StatusDriver findStatusByName(String nameStatus);

    List<Driver> getAvailableDrivers();

}
