package ru.tsystems.logiweb.server.service;

import ru.tsystems.logiweb.server.entities.User;

/**
 * Created by dsing on 20.08.14.
 */
public interface UserService {
    User login(String login, String password);
    User create(User user);
}
