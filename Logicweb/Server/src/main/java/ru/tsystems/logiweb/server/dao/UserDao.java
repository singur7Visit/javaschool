package ru.tsystems.logiweb.server.dao;

import ru.tsystems.logiweb.server.entities.Role;
import ru.tsystems.logiweb.server.entities.User;

/**
 * Created by dsing on 18.08.14.
 */
public interface UserDao extends GenericDAO<User,Long> {
    User getUserByLoginPassword(String login,String password);
    User createUser(User user);
    void removeUser(User user);
    User findByLogin(String login);
    Role getRoleByName(String name);
}
