package ru.tsystems.logiweb.server.dao.jpaimpl;

import ru.tsystems.logiweb.server.dao.GenericDAO;

import javax.persistence.EntityManager;
import java.lang.reflect.ParameterizedType;
import java.util.List;

/**
 * Created by dsing on 18.08.14.
 */
public abstract class JpaDAO <T,ID> implements GenericDAO<T,ID> {

    private Class<T> entityClass;
    protected EntityManager entityManager;

    protected JpaDAO(EntityManager entityManager) {
        ParameterizedType genericSuperclass = (ParameterizedType) getClass().getGenericSuperclass();
        this.entityClass = (Class<T>) genericSuperclass.getActualTypeArguments()[0];
        this.entityManager = entityManager;
    }

    protected T getSingleResult(List<T> result) {
        return result == null || result.isEmpty() ? null : result.get(0);
    }

    @Override
    public T insert(final T obj) {
        entityManager.persist(obj);
        entityManager.flush();
        entityManager.clear();
        return obj;
    }

    @Override
    public T select(final ID id) {
        return entityManager.find(entityClass,id);
    }

    @Override
    public T update(final T obj) {
        return entityManager.merge(obj);
    }

    @Override
    public void delete(final T obj) {
        entityManager.remove(obj);
        entityManager.flush();
    }

}

