package ru.tsystems.logiweb.server.commands.impl.mdrivers;

import ru.tsystems.logiweb.common.commands.ICommand;
import ru.tsystems.logiweb.common.entities.impl.DriverCommon;
import ru.tsystems.logiweb.server.commands.IServerCommand;
import ru.tsystems.logiweb.server.entities.Driver;
import ru.tsystems.logiweb.server.scanners.ScannerObject;
import ru.tsystems.logiweb.server.service.factory.FactoryService;

/**
 * Created by dsing on 19.08.14.
 */
public class RemoveDriverCommand implements IServerCommand {
    @Override
    public ICommand execute(ICommand command) throws Exception {
        DriverCommon requestDriver = (DriverCommon) command.getParam("Driver");
        Driver removeDriver = (Driver) ScannerObject.scanCommonObject(requestDriver, Driver.class);
        command.addParam("Status",Boolean.valueOf(FactoryService.getDriverService().removeDriver(removeDriver)));
        return command;
    }

    @Override
    public String getName() {
        return "RemoveDriver";
    }
}
