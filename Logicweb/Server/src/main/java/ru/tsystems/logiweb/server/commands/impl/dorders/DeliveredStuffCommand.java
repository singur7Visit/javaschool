package ru.tsystems.logiweb.server.commands.impl.dorders;

import ru.tsystems.logiweb.common.commands.ICommand;
import ru.tsystems.logiweb.common.entities.impl.OrderCommon;
import ru.tsystems.logiweb.common.entities.impl.StuffCommon;
import ru.tsystems.logiweb.common.entities.impl.TruckCommon;
import ru.tsystems.logiweb.server.commands.IServerCommand;
import ru.tsystems.logiweb.server.entities.Order;
import ru.tsystems.logiweb.server.entities.Stuff;
import ru.tsystems.logiweb.server.scanners.ScannerObject;
import ru.tsystems.logiweb.server.service.factory.FactoryService;

/**
 * Created by dsing on 24.08.14.
 */
public class DeliveredStuffCommand implements IServerCommand {
    @Override
    public ICommand execute(ICommand command) throws Exception {
        StuffCommon stuffCommon = (StuffCommon) command.getParam("Stuff");
        Stuff stuff = ScannerObject.scanCommonObject(stuffCommon,Stuff.class);
        stuff.setOrder(ScannerObject.scanCommonObject(stuffCommon.getOrder(), Order.class));
        Order order = FactoryService.getOrderService().deliveredStuff(stuff);
        OrderCommon orderCommon = ScannerObject.scanServerObject(order,OrderCommon.class);
        orderCommon.setStuffs(ScannerObject.scanListServerObjects(order.getStuffs()));
        orderCommon.setDrivers(ScannerObject.scanListServerObjects(order.getDrivers()));
        orderCommon.setTruck(ScannerObject.scanServerObject(order.getTruck(), TruckCommon.class));
        if(orderCommon.getStuffs() != null)
            for(StuffCommon st : orderCommon.getStuffs())
                st.setOrder(orderCommon);
        command.clearParams();
        command.addParam("Status",true);
        command.addParam("Order",orderCommon);
        return command;
    }

    @Override
    public String getName() {
        return "DeliveredStuff";
    }
}
