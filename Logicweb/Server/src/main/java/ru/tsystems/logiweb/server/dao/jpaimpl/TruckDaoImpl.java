package ru.tsystems.logiweb.server.dao.jpaimpl;

import ru.tsystems.logiweb.server.dao.TruckDao;
import ru.tsystems.logiweb.server.entities.ClassCapacity;
import ru.tsystems.logiweb.server.entities.StatusTruck;
import ru.tsystems.logiweb.server.entities.Truck;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * Created by dsing on 18.08.14.
 */
public class TruckDaoImpl extends JpaDAO<Truck, Long> implements TruckDao {

    public TruckDaoImpl(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public List<Truck> getTrucksByLimit(int from, int to) {
        return entityManager.createQuery("SELECT t FROM Truck t", Truck.class)
                .setFirstResult(from)
                .setMaxResults(to)
                .getResultList();
    }

    @Override
    public List<Truck> getTrucksByLimitSearch(int from, int to, String search) {
        return entityManager.createQuery("SELECT t FROM Truck t WHERE t.regNumber LIKE :number", Truck.class)
                .setParameter("number","%" + search + "%")
                .setFirstResult(from)
                .setMaxResults(to)
                .getResultList();
    }

    @Override
    public Truck createTruck(Truck truck) {
        return insert(truck);
    }

    @Override
    public void removeTruck(Truck truck) {
        delete(truck);
    }

    @Override
    public Truck findByNumber(String number) {
        return getSingleResult(entityManager.createQuery("SELECT t FROM Truck t WHERE t.regNumber = :number", Truck.class)
                .setParameter("number", number)
                .getResultList());
    }

    @Override
    public StatusTruck findStatusByName(String nameStatus) {
        List<StatusTruck> result = entityManager.createQuery("SELECT s FROM StatusTruck s WHERE s.name=:name",StatusTruck.class)
                .setParameter("name",nameStatus)
                .getResultList();
        return result == null || result.isEmpty() ? null : result.get(0);
    }

    @Override
    public List<ClassCapacity> getAllClassesCapacity() {
        return entityManager.createQuery("SELECT c FROM ClassCapacity c",ClassCapacity.class)
                .getResultList();
    }

    @Override
    public List<Truck> getAvailableTruck() {
        return entityManager.createNativeQuery("SELECT * FROM trucks WHERE id_status = " +
                "(SELECT id from statuses_trucks WHERE name = 'free')",Truck.class).getResultList();
    }
}
