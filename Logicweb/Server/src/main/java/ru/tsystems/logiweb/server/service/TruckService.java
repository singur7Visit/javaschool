package ru.tsystems.logiweb.server.service;

import ru.tsystems.logiweb.server.entities.ClassCapacity;
import ru.tsystems.logiweb.server.entities.Truck;

import java.util.List;

/**
 * Created by dsing on 21.08.14.
 */
public interface TruckService {
    Truck createTruck(Truck driver);
    boolean removeTruck(Truck driver);
    boolean isUniqueTruck(String regNumber);
    List<Truck> searchTrucks(int from, int to, String searchNumber);
    List<Truck> getAllTrucks(int from, int to);
    List<ClassCapacity> getClasses();
    List<Truck> getAvailableTrucks();
}
