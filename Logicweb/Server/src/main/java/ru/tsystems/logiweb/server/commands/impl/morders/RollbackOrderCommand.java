package ru.tsystems.logiweb.server.commands.impl.morders;

import ru.tsystems.logiweb.common.commands.ICommand;
import ru.tsystems.logiweb.server.commands.IServerCommand;
import ru.tsystems.logiweb.server.entities.Order;
import ru.tsystems.logiweb.server.scanners.ScannerObject;
import ru.tsystems.logiweb.server.service.factory.FactoryService;

/**
 * Created by dsing on 22.08.14.
 */
public class RollbackOrderCommand implements IServerCommand {
    @Override
    public ICommand execute(ICommand command) throws Exception {
        Order order = ScannerObject.scanCommonObject(command.getParam("Order"), Order.class);
        command.addParam("Status",FactoryService.getOrderService().rollbackOrder(order));
        return command;
    }

    @Override
    public String getName() {
        return "RollbackOrder";
    }
}
