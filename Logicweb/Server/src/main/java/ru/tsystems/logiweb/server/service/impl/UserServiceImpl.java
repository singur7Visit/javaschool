package ru.tsystems.logiweb.server.service.impl;

import ru.tsystems.logiweb.server.dao.UserDao;
import ru.tsystems.logiweb.server.dao.factory.FactoryJpaDAO;
import ru.tsystems.logiweb.server.entities.User;
import ru.tsystems.logiweb.server.manager.PersistenceManager;
import ru.tsystems.logiweb.server.service.UserService;

import javax.persistence.EntityManager;

/**
 * Created by dsing on 20.08.14.
 */
public class UserServiceImpl implements UserService {

    @Override
    public User login(String login, String password) {
        return FactoryJpaDAO.getUserDAO(PersistenceManager.getInstance().getEntityManager())
                .getUserByLoginPassword(login,password);
    }

    @Override
    public User create(User user) {
        EntityManager em = PersistenceManager.getInstance().getEntityManager();
        em.getTransaction().begin();
        UserDao userDao = FactoryJpaDAO.getUserDAO(em);
        User old = userDao.findByLogin(user.getLogin());
        User newUser = old == null ? userDao.createUser(user) : user;
        em.getTransaction().commit();
        return newUser;
    }
}
