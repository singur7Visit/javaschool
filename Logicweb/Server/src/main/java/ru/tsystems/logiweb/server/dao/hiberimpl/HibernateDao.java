package ru.tsystems.logiweb.server.dao.hiberimpl;

import ru.tsystems.logiweb.server.dao.GenericDAO;

/**
 * Created by dsing on 20.08.14.
 */
public class HibernateDao<T,ID> implements GenericDAO<T, ID> {
    @Override
    public T insert(T obj) {
        return null;
    }

    @Override
    public T select(ID id) {
        return null;
    }

    @Override
    public T update(T obj) {
        return null;
    }

    @Override
    public void delete(T obj) {

    }
}
