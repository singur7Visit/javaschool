package ru.tsystems.logiweb.server.commands.impl.morders;

import ru.tsystems.logiweb.common.commands.ICommand;
import ru.tsystems.logiweb.common.entities.impl.OrderCommon;
import ru.tsystems.logiweb.server.commands.IServerCommand;
import ru.tsystems.logiweb.server.entities.Order;
import ru.tsystems.logiweb.server.scanners.ScannerObject;
import ru.tsystems.logiweb.server.service.factory.FactoryService;

/**
 * Created by dsing on 22.08.14.
 */
public class ConfirmOrderCommand implements IServerCommand {
    @Override
    public ICommand execute(ICommand command) throws Exception {
        OrderCommon orderCommon = (OrderCommon) command.getParam("Order");
        Order order = ScannerObject.scanCommonObject(orderCommon, Order.class);
        order.setStuffs(ScannerObject.scanListCommonObjects(orderCommon.getStuffs()));
        Order newOrder = FactoryService.getOrderService().confirmOrder(order);
        OrderCommon responseOrder = ScannerObject.scanServerObject(newOrder, OrderCommon.class);
        responseOrder.setStuffs(ScannerObject.scanListServerObjects(newOrder.getStuffs()));
        command.addParam("Order",responseOrder);
        command.addParam("Status",Boolean.TRUE);
        return command;
    }

    @Override
    public String getName() {
        return "ConfirmOrder";
    }
}
