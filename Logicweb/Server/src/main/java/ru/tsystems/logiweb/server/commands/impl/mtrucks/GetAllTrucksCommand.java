package ru.tsystems.logiweb.server.commands.impl.mtrucks;

import ru.tsystems.logiweb.common.commands.ICommand;
import ru.tsystems.logiweb.common.entities.IObject;
import ru.tsystems.logiweb.server.commands.IServerCommand;
import ru.tsystems.logiweb.server.scanners.ScannerObject;
import ru.tsystems.logiweb.server.service.factory.FactoryService;

import java.io.Serializable;
import java.util.List;

/**
 * Created by dsing on 18.08.14.
 */
public class GetAllTrucksCommand implements IServerCommand{
    @Override
    public ICommand execute(ICommand command) throws Exception{
        int from = (int) command.getParam("from");
        int to = (int) command.getParam("to");
        List trucks = FactoryService.getTruckService().getAllTrucks(from,to);
        List<IObject> trucksCommon = ScannerObject.scanListServerObjects(trucks);
        command.addParam("Trucks", (Serializable) trucksCommon);
        command.addParam("Status",Boolean.TRUE);
        return command;

    }

    @Override
    public String getName() {
        return "GetAllTrucks";
    }
}
