package ru.tsystems.logiweb.server.commands.impl.mtrucks;

import ru.tsystems.logiweb.common.commands.ICommand;
import ru.tsystems.logiweb.common.entities.impl.TruckCommon;
import ru.tsystems.logiweb.server.commands.IServerCommand;
import ru.tsystems.logiweb.server.entities.Truck;
import ru.tsystems.logiweb.server.scanners.ScannerObject;
import ru.tsystems.logiweb.server.service.factory.FactoryService;

/**
 * Created by dsing on 19.08.14.
 */
public class RemoveTruckCommand implements IServerCommand {
    @Override
    public ICommand execute(ICommand command) throws Exception {
        TruckCommon requestTruck = (TruckCommon) command.getParam("Truck");
        Truck removeTruck = (Truck) ScannerObject.scanCommonObject(requestTruck, Truck.class);
        command.addParam("Status",Boolean.valueOf(FactoryService.getTruckService().removeTruck(removeTruck)));
        return command;
    }

    @Override
    public String getName() {
        return "RemoveTruck";
    }
}
