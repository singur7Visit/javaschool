package ru.tsystems.logiweb.server.commands.impl.morders;

import ru.tsystems.logiweb.common.commands.ICommand;
import ru.tsystems.logiweb.common.entities.IObject;
import ru.tsystems.logiweb.server.commands.IServerCommand;
import ru.tsystems.logiweb.server.scanners.ScannerObject;
import ru.tsystems.logiweb.server.service.factory.FactoryService;

import java.io.Serializable;
import java.util.List;

/**
 * Created by dsing on 22.08.14.
 */
public class GetAllOrderSearchCommand implements IServerCommand {
    @Override
    public ICommand execute(ICommand command) throws Exception {
        int from = (int) command.getParam("from");
        int to = (int) command.getParam("to");
        String searchWord = (String) command.getParam("SearchWord");
        List orders = FactoryService.getOrderService().searchOrders(from,to,searchWord);
        List<IObject> orderCommons = ScannerObject.scanListServerObjects(orders);
        command.addParam("Orders", (Serializable) orderCommons);
        command.addParam("Status",Boolean.TRUE);
        return command;
    }

    @Override
    public String getName() {
        return "GetAllOrdersSearch";
    }
}
