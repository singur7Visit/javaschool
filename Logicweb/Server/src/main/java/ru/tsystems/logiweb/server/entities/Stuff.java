package ru.tsystems.logiweb.server.entities;

import ru.tsystems.logiweb.common.annotations.NonScanner;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import java.math.BigDecimal;

/**
 * Created by dsing on 15.08.14.
 */
@Entity
@Table(name="stuffs")
public class Stuff implements IServerObject {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private long id;

    @Column(name="gps")
    private String gps;

    @Column(name = "name")
    private String name;

    @Column
    @Digits(integer=20, fraction=3,message = "invalid weight")
    private BigDecimal weight;

    @Column
    private boolean status;

    @ManyToOne
    @JoinColumn(name="id_order",  nullable=false)
    private Order order;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getGps() {
        return gps;
    }

    public void setGps(String gps) {
        this.gps = gps;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }

    @NonScanner
    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}