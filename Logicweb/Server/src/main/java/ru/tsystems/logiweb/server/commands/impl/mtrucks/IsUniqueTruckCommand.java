package ru.tsystems.logiweb.server.commands.impl.mtrucks;

import ru.tsystems.logiweb.common.commands.ICommand;
import ru.tsystems.logiweb.server.commands.IServerCommand;
import ru.tsystems.logiweb.server.service.factory.FactoryService;

/**
 * Created by dsing on 21.08.14.
 */
public class IsUniqueTruckCommand implements IServerCommand {
    @Override
    public ICommand execute(ICommand command) throws Exception {
        String regNumber = (String) command.getParam("number");
        Boolean isUnique = FactoryService.getTruckService().isUniqueTruck(regNumber);
        command.addParam("isUnique",isUnique);
        return command;
    }

    @Override
    public String getName() {
        return "isUniqueTruck";
    }
}
