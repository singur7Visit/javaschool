package ru.tsystems.logiweb.server.dao;

import ru.tsystems.logiweb.server.entities.Order;
import ru.tsystems.logiweb.server.entities.StatusOrder;

import java.util.List;

/**
 * Created by dsing on 21.08.14.
 */
public interface OrderDao extends GenericDAO<Order,Long>  {
    List<Order> getOrdersByLimit(int from, int to);

    List<Order> getOrdersByLimitSearch(int from, int to, String search);

    Order createOrder(Order order);

    Order updateOrder(Order order);

    void removeOrder(Order order);

    Order findByNumber(Long number);

    StatusOrder findStatusByName(String nameStatus);

    Order getOrderById(long id);
}
