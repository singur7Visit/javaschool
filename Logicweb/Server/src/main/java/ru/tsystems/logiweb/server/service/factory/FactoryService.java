package ru.tsystems.logiweb.server.service.factory;

import ru.tsystems.logiweb.server.service.DriverService;
import ru.tsystems.logiweb.server.service.OrderService;
import ru.tsystems.logiweb.server.service.TruckService;
import ru.tsystems.logiweb.server.service.UserService;
import ru.tsystems.logiweb.server.service.impl.DriverServiceImpl;
import ru.tsystems.logiweb.server.service.impl.OrderServiceImpl;
import ru.tsystems.logiweb.server.service.impl.TruckServiceImpl;
import ru.tsystems.logiweb.server.service.impl.UserServiceImpl;

/**
 * Created by dsing on 20.08.14.
 */
public class FactoryService {
    private static volatile UserService userService;
    private static volatile DriverService driverService;
    private static volatile TruckService truckService;
    private static volatile OrderService orderService;


    public static UserService getUserService() {
        if(userService == null) {
            synchronized (UserService.class) {
                if(userService == null)
                    userService = new UserServiceImpl();
            }
        }
        return userService;
    }

    public static DriverService getDriverService() {
        if(driverService == null) {
            synchronized (DriverService.class) {
                if(driverService == null)
                    driverService = new DriverServiceImpl();
            }
        }
        return driverService;
    }

    public static TruckService getTruckService() {
        if(truckService == null) {
            synchronized (TruckService.class) {
                if(truckService == null)
                    truckService = new TruckServiceImpl();
            }
        }
        return truckService;
    }

    public static OrderService getOrderService() {
        if(orderService == null) {
            synchronized (OrderService.class) {
                if(orderService == null)
                    orderService = new OrderServiceImpl();
            }
        }
        return orderService;
    }
}
