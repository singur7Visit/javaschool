package ru.tsystems.logiweb.server.connections;

import org.apache.log4j.Logger;
import ru.tsystems.logiweb.common.commands.ICommand;
import ru.tsystems.logiweb.server.commands.IServerCommand;
import ru.tsystems.logiweb.server.manager.PersistenceManager;

import java.io.*;
import java.net.Socket;
import java.util.Map;

/**
 * Created by dsing on 17.08.14.
 */
public class ClientConnection implements Runnable {

    private Socket socket;
    private Map<String, IServerCommand> serverCommands;
    private final static Logger logger = Logger.getLogger(ClientConnection.class);

    public ClientConnection(Socket socket, Map<String, IServerCommand> commands) {
        this.socket = socket;
        this.serverCommands = commands;
    }

    @Override
    public void run() {
        try {
            logger.info("New connection from "+socket.getLocalSocketAddress());
            ObjectInputStream input = new ObjectInputStream(new BufferedInputStream(socket.getInputStream()));
            ICommand request = (ICommand) input.readObject();
            ICommand response = null;
            try {
                response = serverCommands.get(request.getCommand()).execute(request);
            } catch (Exception e) {
                PersistenceManager.getInstance().getEntityManager().getTransaction().rollback();
                logger.error("Cann't get response from JPA. Message : " + e.getMessage());
            }
            response = response == null ? request : response;
            ObjectOutputStream output = new ObjectOutputStream(new BufferedOutputStream(socket.getOutputStream()));
            output.writeObject(response);
            output.flush();
            logger.info("Closing connection with " + socket.getLocalSocketAddress() + "...");
        } catch (ClassNotFoundException | IOException e) {
            logger.error("Error handle user command. Message : " + e.getMessage());
        }finally {
            try {
                socket.close();
            } catch (IOException | NullPointerException e) {
                logger.error("Socket already closed");
            }
        }
    }
}
