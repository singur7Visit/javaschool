package ru.tsystems.logiweb.server;

import ru.tsystems.logiweb.server.gui.ServerGUI;

import javax.swing.*;

/**
 * Created by dsing on 14.08.14.
 */
public class Server {
    private static final int PORT = 3333;
    private static final int COUNT_CONNECTIONS = 1;

    public static void main(String[] args) {
        SwingUtilities.invokeLater(new Runnable() {
            @Override
            public void run() {
                new ServerGUI(COUNT_CONNECTIONS,PORT);
            }
        });
    }
}
