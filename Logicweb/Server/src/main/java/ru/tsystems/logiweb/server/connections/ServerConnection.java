package ru.tsystems.logiweb.server.connections;

import org.apache.log4j.Logger;
import ru.tsystems.logiweb.server.ClassFinder;
import ru.tsystems.logiweb.server.commands.IServerCommand;
import ru.tsystems.logiweb.server.manager.PersistenceManager;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by dsing on 17.08.14.
 */
public class ServerConnection implements Runnable {
    private ExecutorService service;
    private ServerSocket socket;
    private final static Logger logger = Logger.getLogger(ServerConnection.class);
    private Map<String, IServerCommand> commands;
    private int port;
    private boolean interruptFlag = false;
    private int poolCapacity;


    public ServerConnection(int poolCapacity, int port) {
        this.port = port;
        this.poolCapacity = poolCapacity;
        initCommands();
        initEntityManager();
        openSocket();
        logger.info("Connection created");
    }

    public void interrupt() {
        interruptFlag = true;
    }

    private void initEntityManager() {
        PersistenceManager.getInstance().open();
        service = Executors.newFixedThreadPool(poolCapacity);
    }

    public void close() {
        PersistenceManager.getInstance().close();
        interrupt();
        closeSocket();
        service.shutdownNow();
    }

    @Override
    public void run() {
        while(!interruptFlag) {
            try {
                if(socket.isClosed())
                    reInit();
                Socket clientSocket = socket.accept();
                service.submit(new ClientConnection(clientSocket,commands));
            } catch (IOException e) {
                logger.error("Error open socket: " + e.getMessage());
                logger.info("Server closing...");
            }
        }
    }

    public boolean isClosed() {
        return interruptFlag;
    }

    private void closeSocket() {
        try {
            socket.close();
        } catch (IOException e) {
            logger.info("Socket closed");
        }
    }

    private void openSocket() {
        try {
            socket = new ServerSocket(port);
        } catch (IOException | NullPointerException e) {
            logger.error("Error open socket: " + e.getMessage());
        }
    }


    private void initCommands() {
        commands = new HashMap<>();
        List<Class<?>> classes = ClassFinder.find("ru.tsystems.logiweb.server.commands.impl.login");
        classes.addAll(ClassFinder.find("ru.tsystems.logiweb.server.commands.impl.mdrivers"));
        classes.addAll(ClassFinder.find("ru.tsystems.logiweb.server.commands.impl.mtrucks"));
        classes.addAll(ClassFinder.find("ru.tsystems.logiweb.server.commands.impl.morders"));
        classes.addAll(ClassFinder.find("ru.tsystems.logiweb.server.commands.impl.dorders"));
        for(Class clazz : classes) {
            try {
                IServerCommand command = (IServerCommand) clazz.newInstance();
                commands.put(command.getName(),command);
            } catch (InstantiationException | IllegalAccessException e) {
                logger.error("Cann't parse command. Message : " + e.getMessage());
            }
        }
    }

    private void reInit() {
        openSocket();
    }

}
