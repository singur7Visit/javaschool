package ru.tsystems.logiweb.server.service;

import ru.tsystems.logiweb.server.entities.Driver;

import java.util.List;

/**
 * Created by dsing on 20.08.14.
 */
public interface DriverService {
    Driver createDriver(Driver driver, String password);
    boolean removeDriver(Driver driver);
    boolean isUniqueDriver(String licence);
    List<Driver> searchDrivers(int from, int to, String searchLicence);
    List<Driver> getAllDrivers(int from, int to);
    Driver findByLicence(String licence);
    List<Driver> getAvailableDrivers();
    Driver setStatus(Driver driver,List<Driver> otherDrivers);
}
