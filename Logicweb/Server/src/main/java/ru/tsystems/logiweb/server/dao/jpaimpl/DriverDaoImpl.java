package ru.tsystems.logiweb.server.dao.jpaimpl;

import ru.tsystems.logiweb.server.dao.DriverDao;
import ru.tsystems.logiweb.server.entities.Driver;
import ru.tsystems.logiweb.server.entities.StatusDriver;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * Created by dsing on 19.08.14.
 */
public class DriverDaoImpl extends JpaDAO<Driver,Long> implements DriverDao {

    public DriverDaoImpl(EntityManager entityManager) {
        super(entityManager);
    }

    @Override
    public List<Driver> getDriversByLimit(final int from, final int to) {
        return entityManager.createQuery("SELECT d FROM Driver d", Driver.class)
                .setFirstResult(from)
                .setMaxResults(to)
                .getResultList();
    }

    @Override
    public List<Driver> getDriversByLimitSearch(final int from, final int to, final String search) {
        return entityManager.createQuery("SELECT d FROM Driver d WHERE d.licence LIKE :licence", Driver.class)
                        .setParameter("licence","%" + search + "%")
                        .setFirstResult(from)
                        .setMaxResults(to)
                        .getResultList();
    }

    @Override
    public Driver createDriver(Driver driver) {
        return insert(driver);
    }

    @Override
    public void removeDriver(Driver driver) {
        delete(driver);
    }

    @Override
    public Driver findByLicence(String licence) {
        return getSingleResult(entityManager.createQuery("SELECT d FROM Driver d WHERE d.licence = :licence", Driver.class)
                .setParameter("licence", licence)
                .getResultList());
    }

    @Override
    public StatusDriver findStatusByName(String nameStatus) {
        List<StatusDriver> resilt = entityManager.createQuery("SELECT s FROM StatusDriver s WHERE s.name = :name",StatusDriver.class)
                .setParameter("name",nameStatus)
                .getResultList();
        return resilt == null || resilt.isEmpty() ? null : resilt.get(0);
    }

    @Override
    public List<Driver> getAvailableDrivers() {
        return entityManager.createNativeQuery("SELECT * FROM drivers WHERE id_status = " +
                "(SELECT id from statuses_drivers WHERE name = 'free')",Driver.class).getResultList();
    }

}
