package ru.tsystems.logiweb.server.commands.impl.morders;

import ru.tsystems.logiweb.common.commands.ICommand;
import ru.tsystems.logiweb.common.entities.impl.OrderCommon;
import ru.tsystems.logiweb.common.entities.impl.TruckCommon;
import ru.tsystems.logiweb.server.commands.IServerCommand;
import ru.tsystems.logiweb.server.entities.Order;
import ru.tsystems.logiweb.server.scanners.ScannerObject;
import ru.tsystems.logiweb.server.service.factory.FactoryService;

import java.io.Serializable;
import java.util.List;

/**
 * Created by dsing on 22.08.14.
 */
public class GetAllOrdersCommand implements IServerCommand {
    @Override
    public ICommand execute(ICommand command) throws Exception {
        int from = (int) command.getParam("from");
        int to = (int) command.getParam("to");
        List<Order> orders = FactoryService.getOrderService().getAllOrders(from,to);
        List<OrderCommon> orderCommons = ScannerObject.scanListServerObjects(orders);
        for(int i= 0; i<orderCommons.size(); i++) {
            orderCommons.get(i).setTruck(ScannerObject.scanServerObject(orders.get(i).getTruck(), TruckCommon.class));
        }
        command.addParam("Orders", (Serializable) orderCommons);
        command.addParam("Status",Boolean.TRUE);
        return command;
    }

    @Override
    public String getName() {
        return "GetAllOrders";
    }
}
