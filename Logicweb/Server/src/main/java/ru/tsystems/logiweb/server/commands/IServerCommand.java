package ru.tsystems.logiweb.server.commands;

import ru.tsystems.logiweb.common.commands.ICommand;

/**
 * Created by dsing on 17.08.14.
 */
public interface IServerCommand {
    ICommand execute(ICommand command) throws Exception;
    String getName();
}
