package ru.tsystems.logiweb.server.entities;

import javax.persistence.*;
import java.util.List;

/**
 * Created by dsing on 15.08.14.
 */

@Entity
@Table(name = "orders")
public class Order implements IServerObject {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private long id;

    @Column(name = "num_order")
    private Long numOrder;

    @OneToOne
    @JoinColumn(name = "id_status")
    private StatusOrder status;

    @OneToMany(mappedBy = "order")
    private List<Driver> drivers;

    @OneToOne(mappedBy = "order")
    private Truck truck;

    @OneToMany(mappedBy = "order")
    private List<Stuff> stuffs;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Long getNumOrder() {
        return numOrder;
    }

    public void setNumOrder(Long numOrder) {
        this.numOrder = numOrder;
    }

    public StatusOrder getStatus() {
        return status;
    }

    public void setStatus(StatusOrder status) {
        this.status = status;
    }

    public List<Driver> getDrivers() {
        return drivers;
    }

    public void setDrivers(List<Driver> drivers) {
        this.drivers = drivers;
    }

    public Truck getTruck() {
        return truck;
    }

    public void setTruck(Truck truck) {
        this.truck = truck;
    }

    public List<Stuff> getStuffs() {
        return stuffs;
    }

    public void setStuffs(List<Stuff> stuffs) {
        this.stuffs = stuffs;
    }
}
