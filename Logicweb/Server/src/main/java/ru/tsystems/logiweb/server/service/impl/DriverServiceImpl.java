package ru.tsystems.logiweb.server.service.impl;

import ru.tsystems.logiweb.server.dao.DriverDao;
import ru.tsystems.logiweb.server.dao.UserDao;
import ru.tsystems.logiweb.server.dao.factory.FactoryJpaDAO;
import ru.tsystems.logiweb.server.entities.Driver;
import ru.tsystems.logiweb.server.entities.StatusDriver;
import ru.tsystems.logiweb.server.entities.User;
import ru.tsystems.logiweb.server.manager.PersistenceManager;
import ru.tsystems.logiweb.server.service.DriverService;

import javax.persistence.EntityManager;
import java.util.List;

/**
 * Created by dsing on 20.08.14.
 */
public class DriverServiceImpl implements DriverService {

    @Override
    public Driver createDriver(Driver driver, String password) {
        EntityManager em = PersistenceManager.getInstance().getEntityManager();
        em.getTransaction().begin();
        DriverDao driverDao = FactoryJpaDAO.getDriverDAO(em);
        if(!isUniqueDriver(driver.getLicence())) {
            em.getTransaction().rollback();
            return null;
        }
        StatusDriver statusDriver = driverDao.findStatusByName("free");
        driver.setStatus(statusDriver);
        Driver newDriver = driverDao.createDriver(driver);
        UserDao userDao = FactoryJpaDAO.getUserDAO(em);
        User newUser = new User();
        newUser.setRole(userDao.getRoleByName("driver"));
        newUser.setLogin(driver.getLicence());
        newUser.setPassword(password);
        User user = userDao.createUser(newUser);
        if(user != null)
            em.getTransaction().commit();
        else {
            em.getTransaction().rollback();
            return null;
        }
        return newDriver;
    }

    @Override
    public boolean removeDriver(Driver driver) {
        EntityManager em = PersistenceManager.getInstance().getEntityManager();
        em.getTransaction().begin();
        DriverDao driverDao = FactoryJpaDAO.getDriverDAO(em);
        UserDao userDao = FactoryJpaDAO.getUserDAO(em);
        Driver foundDriver = driverDao.select(driver.getId());
        User user = userDao.findByLogin(driver.getLicence());
        if(foundDriver == null) {
            em.getTransaction().rollback();
            return false;
        }
        driverDao.removeDriver(foundDriver);
        userDao.removeUser(user);
        em.getTransaction().commit();
        return true;
    }

    @Override
    public boolean isUniqueDriver(String licence) {
        EntityManager em = PersistenceManager.getInstance().getEntityManager();
        Driver driver = FactoryJpaDAO.getDriverDAO(em).findByLicence(licence);
        return driver == null;
    }

    @Override
    public List<Driver> searchDrivers(int from, int to, String searchLicence) {
        return FactoryJpaDAO.getDriverDAO(PersistenceManager.getInstance().getEntityManager())
                .getDriversByLimitSearch(from, to, searchLicence);
    }

    @Override
    public List<Driver> getAllDrivers(int from, int to) {
        return FactoryJpaDAO.getDriverDAO(PersistenceManager.getInstance().getEntityManager())
                .getDriversByLimit(from, to);
    }

    @Override
    public Driver findByLicence(String licence) {
        return FactoryJpaDAO.getDriverDAO(PersistenceManager.getInstance().getEntityManager())
                .findByLicence(licence);
    }

    @Override
    public List<Driver> getAvailableDrivers() {
        return FactoryJpaDAO.getDriverDAO(PersistenceManager.getInstance().getEntityManager())
                .getAvailableDrivers();
    }

    @Override
    public Driver setStatus(Driver driver, List<Driver> otherDrivers) {
        EntityManager em = PersistenceManager.getInstance().getEntityManager();
        em.getTransaction().begin();
        StatusDriver statusDriver = driver.getStatus();
        for(Driver d : otherDrivers) {
            if(d.getStatus().getName().equals("driving") && statusDriver.getName().equals("driving")) {
                em.getTransaction().rollback();
                return null;
            }
        }
        driver = em.find(Driver.class,driver.getId());
        driver.setStatus(FactoryJpaDAO.getDriverDAO(em).findStatusByName(statusDriver.getName()));
        driver = em.merge(driver);
        em.flush();
        em.getTransaction().commit();
        em.clear();
        return driver;
    }

}
