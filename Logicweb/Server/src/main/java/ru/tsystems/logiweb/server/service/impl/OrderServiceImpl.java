package ru.tsystems.logiweb.server.service.impl;

import ru.tsystems.logiweb.server.dao.DriverDao;
import ru.tsystems.logiweb.server.dao.OrderDao;
import ru.tsystems.logiweb.server.dao.TruckDao;
import ru.tsystems.logiweb.server.dao.factory.FactoryJpaDAO;
import ru.tsystems.logiweb.server.entities.Driver;
import ru.tsystems.logiweb.server.entities.Order;
import ru.tsystems.logiweb.server.entities.Stuff;
import ru.tsystems.logiweb.server.manager.PersistenceManager;
import ru.tsystems.logiweb.server.service.OrderService;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by dsing on 22.08.14.
 */
public class OrderServiceImpl implements OrderService {
    @Override
    public Order createOrder(Order order) {
        EntityManager em = PersistenceManager.getInstance().getEntityManager();
        em.getTransaction().begin();
        OrderDao orderDao = FactoryJpaDAO.getOrderDAO(em);
        if(order.getNumOrder() != null && orderDao.findByNumber(order.getNumOrder()) != null) {
            em.getTransaction().rollback();
            return null;
        }
        Order newOrder = orderDao.createOrder(order);
        newOrder.setNumOrder(order.getId());
        newOrder.setStatus(orderDao.findStatusByName("created"));
        newOrder = orderDao.updateOrder(newOrder);
        em.flush();
        em.getTransaction().commit();
        return newOrder;
    }

    @Override
    public Order confirmOrder(Order order) {
        EntityManager em = PersistenceManager.getInstance().getEntityManager();
        em.getTransaction().begin();
        if(order.getStuffs() == null) {
            Order o = em.find(Order.class,order.getId());
            if(o != null)
                em.remove(o);
            em.getTransaction().rollback();
            return null;
        }
        List<Stuff> stuffs = order.getStuffs();
        order.setStuffs(new ArrayList<Stuff>());
        for(Stuff stuff : stuffs) {
            stuff.setOrder(order);
            order.getStuffs().add(em.merge(stuff));
        }
        OrderDao orderDao = FactoryJpaDAO.getOrderDAO(em);
        order.setStatus(orderDao.findStatusByName("confirmed"));
        Order newOrder = orderDao.updateOrder(order);
        em.flush();
        em.getTransaction().commit();
        em.clear();
        return newOrder;
    }

    @Override
    public Order shipOrder(Order order) {
        EntityManager em = PersistenceManager.getInstance().getEntityManager();
        em.getTransaction().begin();
        if(order.getTruck() == null || order.getDrivers() == null || order.getDrivers().isEmpty()) {
            Order o = em.find(Order.class,order.getId());
            if(o != null)
                em.remove(o);
            em.getTransaction().rollback();
            return null;
        }
        OrderDao orderDao = FactoryJpaDAO.getOrderDAO(em);
        TruckDao truckDao = FactoryJpaDAO.getTruckDAO(em);
        DriverDao driverDao = FactoryJpaDAO.getDriverDAO(em);
        order.setStatus(orderDao.findStatusByName("shipped"));
        for(Driver driver : order.getDrivers()) {
            driver.setStatus(driverDao.findStatusByName("busy"));
            driver.setOrder(order);
            driver.setTruck(order.getTruck());
            em.merge(driver);
        }
        for(Stuff stuff : order.getStuffs()) {
            stuff.setOrder(order);
            em.merge(stuff);
        }
        order.getTruck().setStatus(truckDao.findStatusByName("busy"));
        order.getTruck().setOrder(order);
        order.getTruck().setDrivers(order.getDrivers());
        order.setTruck(em.merge(order.getTruck()));
        Order newOrder = orderDao.updateOrder(order);
        em.flush();
        em.getTransaction().commit();
        em.clear();
        return newOrder;
    }

    @Override
    public Order completeOrder(Order order) {
        EntityManager em = PersistenceManager.getInstance().getEntityManager();
        order = em.find(Order.class,order.getId());
        em.getTransaction().begin();
        if(order.getTruck() == null || order.getDrivers() == null
                || order.getDrivers().isEmpty() || order.getStuffs() == null) {
            em.getTransaction().rollback();
            return null;
        }
        for(Stuff stuff : order.getStuffs()) {
            if (!stuff.isStatus()) {
                em.getTransaction().rollback();
                return null;
            }
        }
        OrderDao orderDao = FactoryJpaDAO.getOrderDAO(em);
        order.setStatus(orderDao.findStatusByName("completed"));
        Order newOrder = orderDao.updateOrder(order);
        em.flush();
        em.getTransaction().commit();
        em.clear();
        return newOrder;
    }

    @Override
    public Order closeOrder(Order order) {
        EntityManager em = PersistenceManager.getInstance().getEntityManager();
        em.getTransaction().begin();
        order = em.find(Order.class,order.getId());
        if(order.getTruck() == null || order.getDrivers() == null
                || order.getDrivers().isEmpty() || order.getStuffs() == null || !"completed".equals(order.getStatus().getName())) {
            em.getTransaction().rollback();
            return null;
        }
        OrderDao orderDao = FactoryJpaDAO.getOrderDAO(em);
        TruckDao truckDao = FactoryJpaDAO.getTruckDAO(em);
        DriverDao driverDao = FactoryJpaDAO.getDriverDAO(em);
        for(Driver driver : order.getDrivers()) {
            if("driving".equals(driver.getStatus().getName())) {
                em.getTransaction().rollback();
                return null;
            }
            else {
                driver.setStatus(driverDao.findStatusByName("free"));
                driver.setOrder(null);
                driver.setTruck(null);
                driverDao.update(driver);
            }
        }
        order.getDrivers().clear();
        order.getTruck().setStatus(truckDao.findStatusByName("free"));
        order.getTruck().setOrder(null);
        order.getTruck().setDrivers(null);
        truckDao.update(order.getTruck());
        order.setStatus(orderDao.findStatusByName("closed"));
        Order newOrder = orderDao.updateOrder(order);
        em.flush();
        em.getTransaction().commit();
        em.clear();
        return newOrder;
    }

    @Override
    public boolean removeOrder(Order order) {
        EntityManager em = PersistenceManager.getInstance().getEntityManager();
        em.getTransaction().begin();
        OrderDao orderDao = FactoryJpaDAO.getOrderDAO(em);
        order = orderDao.select(order.getId());
        TruckDao truckDao = FactoryJpaDAO.getTruckDAO(em);
        DriverDao driverDao = FactoryJpaDAO.getDriverDAO(em);
        if(order.getStuffs()!= null) {
            CopyOnWriteArrayList<Stuff> copyOnWriteArrayList = new CopyOnWriteArrayList<>(order.getStuffs());
            for(Stuff stuff : copyOnWriteArrayList) {
                em.remove(stuff);
            }
        }
        if(order.getTruck() != null) {
            order.getTruck().setStatus(truckDao.findStatusByName("free"));
            order.getTruck().setDrivers(null);
            order.getTruck().setOrder(null);
            em.merge(order.getTruck());
            order.setTruck(null);
        }
        if(order.getDrivers() != null) {
            for(Driver driver : order.getDrivers()) {
                driver.setTruck(null);
                driver.setStatus(driverDao.findStatusByName("free"));
                driver.setTruck(null);
                em.merge(driver);
            }
        }
        order.getDrivers().clear();
        orderDao.removeOrder(order);
        em.flush();
        em.getTransaction().commit();
        em.clear();
        return true;
    }

    @Override
    public boolean isUniqueOrder(Long number) {
        return FactoryJpaDAO.getOrderDAO(PersistenceManager.getInstance().getEntityManager())
                .findByNumber(number) == null;
    }

    @Override
    public List<Order> searchOrders(int from, int to, String id) {
        return FactoryJpaDAO.getOrderDAO(PersistenceManager.getInstance().getEntityManager())
                .getOrdersByLimitSearch(from, to, id);
    }

    @Override
    public List<Order> getAllOrders(int from, int to) {
        return FactoryJpaDAO.getOrderDAO(PersistenceManager.getInstance().getEntityManager())
                .getOrdersByLimit(from,to);
    }

    @Override
    public Boolean rollbackOrder(Order order) {
        EntityManager em = PersistenceManager.getInstance().getEntityManager();
        em.getTransaction().begin();
        if(order.getStatus() == null || "closed".equals(order.getStatus().getName())) {
            em.getTransaction().rollback();
            return false;
        }
        DriverDao driverDao = FactoryJpaDAO.getDriverDAO(em);
        TruckDao truckDao = FactoryJpaDAO.getTruckDAO(em);
        OrderDao orderDao = FactoryJpaDAO.getOrderDAO(em);
        order = orderDao.select(order.getId());
        if(order.getDrivers() != null) {
            for(Driver driver : order.getDrivers()) {
                if(!"free".equals(driver.getStatus().getName())) {
                    driver.setStatus(driverDao.findStatusByName("free"));
                    driverDao.update(driver);
                }
            }
        }
        if(order.getTruck() != null) {
            order.getTruck().setStatus(truckDao.findStatusByName("free"));
            order.getTruck().setOrder(null);
            truckDao.update(order.getTruck());
        }
        orderDao.updateOrder(order);
        Order removingOrder = orderDao.select(order.getId());
        orderDao.removeOrder(removingOrder);
        em.flush();
        em.getTransaction().commit();
        em.clear();
        return true;
    }

    @Override
    public Order getOrderById(long id) {
        return FactoryJpaDAO.getOrderDAO(PersistenceManager.getInstance().getEntityManager()).getOrderById(id);
    }

    @Override
    public Order deliveredStuff(Stuff stuff) {
        EntityManager em = PersistenceManager.getInstance().getEntityManager();
        em.getTransaction().begin();
        Order order = em.find(Order.class,stuff.getOrder().getId());
        for(Stuff st : order.getStuffs()) {
            if(st.getId() == stuff.getId()) {
                st.setStatus(true);
                em.merge(st);
            }
        }
        Order newOrder = em.merge(order);
        em.getTransaction().commit();
        em.clear();
        return newOrder;
    }
}
