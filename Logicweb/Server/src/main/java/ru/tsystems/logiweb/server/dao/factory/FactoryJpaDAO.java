package ru.tsystems.logiweb.server.dao.factory;

import ru.tsystems.logiweb.server.dao.DriverDao;
import ru.tsystems.logiweb.server.dao.OrderDao;
import ru.tsystems.logiweb.server.dao.TruckDao;
import ru.tsystems.logiweb.server.dao.UserDao;
import ru.tsystems.logiweb.server.dao.jpaimpl.DriverDaoImpl;
import ru.tsystems.logiweb.server.dao.jpaimpl.OrderDaoImpl;
import ru.tsystems.logiweb.server.dao.jpaimpl.TruckDaoImpl;
import ru.tsystems.logiweb.server.dao.jpaimpl.UserDaoImpl;

import javax.persistence.EntityManager;

/**
 * Created by dsing on 18.08.14.
 */
public class FactoryJpaDAO {

    public static TruckDao getTruckDAO(EntityManager entityManager) {
        return new TruckDaoImpl(entityManager);
    }

    public static UserDao getUserDAO(EntityManager entityManager) {
        return new UserDaoImpl(entityManager);
    }

    public static DriverDao getDriverDAO(EntityManager entityManager) {
        return new DriverDaoImpl(entityManager);
    }

    public static OrderDao getOrderDAO(EntityManager entityManager) {
        return new OrderDaoImpl(entityManager);
    }


}
