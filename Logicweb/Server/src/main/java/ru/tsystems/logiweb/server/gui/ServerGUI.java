package ru.tsystems.logiweb.server.gui;

import ru.tsystems.logiweb.server.connections.ServerConnection;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * Created by dsing on 17.08.14.
 */
public class ServerGUI extends JFrame {
    private Thread threadConnection;
    private ServerConnection serverConnection;

    public ServerGUI(final int poolCapacity, final int port) {
        setTitle("Logiweb server");
        JPanel jPanel = new JPanel();
        jPanel.setLayout(null);
        setSize(new Dimension(400, 200));
        jPanel.setPreferredSize(new Dimension(400,200));
        setResizable(false);
        setLocation(Toolkit.getDefaultToolkit().getScreenSize().width/3,
                Toolkit.getDefaultToolkit().getScreenSize().height/3);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                if(serverConnection !=null && serverConnection.isClosed()) {
                    serverConnection.close();
                }
            }
        });
        JButton serverStartButton = new JButton("Start");
        JButton serverStopButton = new JButton("Stop");
        final JLabel statusLabel = new JLabel();
        statusLabel.setBounds(150,100,100,20);
        serverStartButton.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(serverConnection !=null && !serverConnection.isClosed())
                    return;
                serverConnection = new ServerConnection(poolCapacity,port);
                threadConnection = new Thread(serverConnection);
                threadConnection.start();
                statusLabel.setText("Server started");
                statusLabel.setForeground(Color.GREEN);
            }
        });

        serverStopButton.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                serverConnection.close();
                statusLabel.setText("Server stoped");
                statusLabel.setForeground(Color.RED);
            }
        });
        serverStartButton.setBounds(63, 33, 100, 50);
        serverStopButton.setBounds(240,33,100,50);
        jPanel.add(serverStartButton);
        jPanel.add(serverStopButton);
        jPanel.add(statusLabel);
        setContentPane(jPanel);
        setVisible(true);


    }

}
