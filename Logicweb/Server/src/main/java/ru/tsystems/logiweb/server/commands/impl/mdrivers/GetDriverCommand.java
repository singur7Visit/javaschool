package ru.tsystems.logiweb.server.commands.impl.mdrivers;

import ru.tsystems.logiweb.common.commands.ICommand;
import ru.tsystems.logiweb.common.entities.impl.DriverCommon;
import ru.tsystems.logiweb.common.entities.impl.OrderCommon;
import ru.tsystems.logiweb.common.entities.impl.StuffCommon;
import ru.tsystems.logiweb.common.entities.impl.TruckCommon;
import ru.tsystems.logiweb.server.commands.IServerCommand;
import ru.tsystems.logiweb.server.entities.Driver;
import ru.tsystems.logiweb.server.scanners.ScannerObject;
import ru.tsystems.logiweb.server.service.factory.FactoryService;

/**
 * Created by dsing on 22.08.14.
 */
public class GetDriverCommand implements IServerCommand {
    @Override
    public ICommand execute(ICommand command) throws Exception {
        Driver driver = FactoryService.getDriverService().findByLicence((String) command.getParam("licence"));
        DriverCommon driverCommon = ScannerObject.scanServerObject(driver, DriverCommon.class);
        driverCommon.setTruck(ScannerObject.scanServerObject(driver.getTruck(), TruckCommon.class));
        driverCommon.setOrder(ScannerObject.scanServerObject(driver.getOrder(), OrderCommon.class));
        if(driverCommon.getOrder() == null) {
            command.addParam("Driver", driverCommon);
            command.addParam("Status",Boolean.TRUE);
            return command;
        }
        driverCommon.getOrder().setDrivers(ScannerObject.scanListServerObjects(driver.getOrder() != null ?
                driver.getOrder().getDrivers() : null));
        driverCommon.getOrder().setStuffs(ScannerObject.scanListServerObjects(driver.getOrder() != null ?
                driver.getOrder().getStuffs() : null));
        if(driverCommon.getOrder() != null && driverCommon.getOrder().getStuffs() != null)
            for(StuffCommon stuff : driverCommon.getOrder().getStuffs()) {
            stuff.setOrder(driverCommon.getOrder());
        }
        command.addParam("Driver", driverCommon);
        command.addParam("Status",Boolean.TRUE);
        return command;
    }

    @Override
    public String getName() {
        return "GetDriver";
    }
}
