package ru.tsystems.logiweb.server.service;

import ru.tsystems.logiweb.server.entities.Order;
import ru.tsystems.logiweb.server.entities.Stuff;

import java.util.List;

/**
 * Created by dsing on 22.08.14.
 */
public interface OrderService {
    Order createOrder(Order order);
    Order confirmOrder(Order order);
    Order shipOrder(Order order);
    Order completeOrder(Order order);
    Order closeOrder(Order order);
    boolean removeOrder(Order order);
    boolean isUniqueOrder(Long number);
    List<Order> searchOrders(int from, int to, String id);
    List<Order> getAllOrders(int from, int to);
    Boolean rollbackOrder(Order order);
    Order getOrderById(long id);
    Order deliveredStuff(Stuff stuff);
}
