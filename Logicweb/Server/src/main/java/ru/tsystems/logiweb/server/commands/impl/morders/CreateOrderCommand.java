package ru.tsystems.logiweb.server.commands.impl.morders;

import ru.tsystems.logiweb.common.commands.ICommand;
import ru.tsystems.logiweb.common.entities.impl.OrderCommon;
import ru.tsystems.logiweb.server.commands.IServerCommand;
import ru.tsystems.logiweb.server.entities.Order;
import ru.tsystems.logiweb.server.scanners.ScannerObject;
import ru.tsystems.logiweb.server.service.factory.FactoryService;

/**
 * Created by dsing on 19.08.14.
 */
public class CreateOrderCommand implements IServerCommand {
    @Override
    public ICommand execute(ICommand command) throws Exception {
        OrderCommon requestOrder = (OrderCommon) command.getParam("Order");
        Order newOrder = ScannerObject.scanCommonObject(requestOrder, Order.class);
        Order savedOrder = FactoryService.getOrderService().createOrder(newOrder);
        OrderCommon responseOrder = ScannerObject.scanServerObject(savedOrder, OrderCommon.class);
        responseOrder.setStuffs(ScannerObject.scanListServerObjects(savedOrder.getStuffs()));
        command.addParam("Status",responseOrder != null);
        command.addParam("Order",responseOrder);
        return command;
    }

    @Override
    public String getName() {
        return "CreateOrder";
    }
}
