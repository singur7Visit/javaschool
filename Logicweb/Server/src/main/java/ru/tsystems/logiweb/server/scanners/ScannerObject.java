package ru.tsystems.logiweb.server.scanners;

import org.apache.log4j.Logger;
import ru.tsystems.logiweb.common.annotations.NonScanner;
import ru.tsystems.logiweb.common.entities.IObject;
import ru.tsystems.logiweb.common.entities.IStatus;
import ru.tsystems.logiweb.server.ClassFinder;
import ru.tsystems.logiweb.server.entities.IServerObject;
import ru.tsystems.logiweb.server.entities.IServerStatus;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Created by dsing on 18.08.14.
 */
public class ScannerObject {

    private final static Logger logger = Logger.getLogger(ScannerObject.class);

    private static Map<Class,Class> mapClasses = new HashMap<>();

    static {
        initClasses();
    }

    public static List scanListCommonObjects (List<? extends IObject> commonObjects) {
        if(commonObjects == null)
            return null;
        List serverObjects = new LinkedList<>();
        for(Object o : commonObjects) {
            Object newObject = scanCommonObject(o,mapClasses.get(o.getClass()));
            if(newObject != null)
                serverObjects.add(newObject);
        }
        return serverObjects;
    }

    public static <T> T scanCommonObject(Object commonObject, Class<T> serverClass) {
        try {
            if(commonObject == null)
                return null;
            T serverObject = serverClass.newInstance();
            for(Method m : commonObject.getClass().getMethods()) {
                if((m.getName().startsWith("is") || m.getName().startsWith("get")) && !m.getName().equals("getClass") && m.getAnnotation(NonScanner.class) == null
                        && (!IObject.class.isAssignableFrom(m.getReturnType()) || IStatus.class.isAssignableFrom(m.getReturnType()))
                        && !List.class.isAssignableFrom(m.getReturnType())) {
                    Object result = new Object();
                    if(IStatus.class.isAssignableFrom(m.getReturnType())) {
                            result = scanCommonObject(m.invoke(commonObject),mapClasses.get(m.getReturnType()));                    }
                    Method method = serverClass.getMethod("set" + m.getName().substring(m.getName().startsWith("is") ? 2 : 3),
                            IStatus.class.isAssignableFrom(m.getReturnType()) ? mapClasses.get(m.getReturnType()): m.getReturnType());
                    method.invoke(serverObject,!IStatus.class.isAssignableFrom(m.getReturnType()) ? m.invoke(commonObject) : result);
                }
            }
            return serverObject;
        } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            logger.error("Scan common object failed. Message : " + e.getMessage());
            return null;
        }

    }



    public static List scanListServerObjects (List serverObjects) {
        if(serverObjects == null)
            return null;
        List commonObjects = new LinkedList();
        for(Object o : serverObjects) {
            Object newObject = scanServerObject(o,mapClasses.get(o.getClass()));
            if(newObject != null)
                commonObjects.add(newObject);
        }
        return commonObjects;
    }

    public static <T> T scanServerObject(Object serverObject, Class<T> commonClass) {
        try {
            if(serverObject == null)
                return null;
            T commonObject = commonClass.newInstance();
            for(Method m : serverObject.getClass().getMethods()) {
                if((m.getName().startsWith("is") || m.getName().startsWith("get")) && !m.getName().equals("getClass")
                        && !IServerObject.class.isAssignableFrom(m.getReturnType())
                        && !List.class.isAssignableFrom(m.getReturnType())) {
                    Object result = new Object();
                    if(IServerStatus.class.isAssignableFrom(m.getReturnType())) {
                        result = scanServerObject(m.invoke(serverObject), mapClasses.get(m.getReturnType()));
                    }
                    Method method = commonClass.getMethod("set" + m.getName().substring(m.getName().startsWith("is") ? 2 : 3),
                            IServerStatus.class.isAssignableFrom(m.getReturnType()) ? mapClasses.get(m.getReturnType()): m.getReturnType());
                    method.invoke(commonObject,!IServerStatus.class.isAssignableFrom(m.getReturnType())  ? m.invoke(serverObject) : result);
                }
            } 
            return commonObject;
        } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            logger.error("Scan server object failed. Message : " + e.getMessage());
            return null;
        }

    }

    private static void initClasses() {
        List<Class<?>> classesServer = ClassFinder.find("ru.tsystems.logiweb.server.entities");
        List<Class<?>> classesCommon = ClassFinder.find("ru.tsystems.logiweb.common.entities.impl");
        for(Class cS : classesServer) {
            for(Class cC: classesCommon) {
                if(!cS.isInterface() && (cS.getSimpleName() + "Common").equals(cC.getSimpleName())) {
                    mapClasses.put(cS,cC);
                    mapClasses.put(cC,cS);
                }
            }
        }

    }
}
