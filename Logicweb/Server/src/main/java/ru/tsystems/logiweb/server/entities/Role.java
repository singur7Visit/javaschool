package ru.tsystems.logiweb.server.entities;

import javax.persistence.*;

/**
 * Created by dsing on 15.08.14.
 */
@Entity
@Table(name = "roles")
public class Role implements IServerStatus{
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private long id;

    @Column(name = "name")
    private String name;

    @Column(name="manage_manager")
    private boolean manageMangers;

    @Column(name = "manage_orders")
    private boolean manageOrders;

    @Column(name = "manage_drivers")
    private boolean manageDrivers;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isManageMangers() {
        return manageMangers;
    }

    public void setManageMangers(boolean manageMangers) {
        this.manageMangers = manageMangers;
    }

    public boolean isManageOrders() {
        return manageOrders;
    }

    public void setManageOrders(boolean manageOrders) {
        this.manageOrders = manageOrders;
    }

    public boolean isManageDrivers() {
        return manageDrivers;
    }

    public void setManageDrivers(boolean manageDrivers) {
        this.manageDrivers = manageDrivers;
    }
}
