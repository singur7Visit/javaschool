package ru.tsystems.logiweb.server.manager;

import org.apache.log4j.Logger;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 * Created by dsing on 19.08.14.
 */
public class PersistenceManager {
    private static PersistenceManager instance = new PersistenceManager();
    private final static Logger logger = Logger.getLogger(PersistenceManager.class);
    private EntityManager entityManager;
    private EntityManagerFactory entityManagerFactory;

    public static PersistenceManager getInstance() {
        return instance;
    }

    private PersistenceManager() {
        entityManagerFactory = Persistence.createEntityManagerFactory("Logiweb");
        entityManager = entityManagerFactory.createEntityManager();
    }

    public EntityManager getEntityManager() {
        return entityManager;
    }

    public void closeEntityManager(EntityManager entityManager) {
        if(entityManager != null && entityManager.isOpen())
            entityManager.close();
    }


    public void open() {
        if(!entityManagerFactory.isOpen()) {
            entityManagerFactory = Persistence.createEntityManagerFactory("Logiweb");
            entityManager = entityManagerFactory.createEntityManager();
        }
    }

    public void close() {
        if(entityManagerFactory.isOpen()) {
            entityManager.close();
            entityManagerFactory.close();
        }
    }

    public void refreshAll() {
        if(entityManagerFactory.isOpen())
            entityManagerFactory.getCache().evictAll();
    }
}
