/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.tsystems.client.logiweb.controllers.manager.drivers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import ru.tsystems.client.logiweb.controllers.AbstractController;
import ru.tsystems.client.logiweb.models.Model;
import ru.tsystems.client.logiweb.models.impl.DriverModel;
import ru.tsystems.client.logiweb.views.AlertDialog;
import ru.tsystems.logiweb.common.entities.impl.DriverCommon;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * FXML Controller class
 *
 * @author dsing
 */
public class ManagerformDriversRemoveController extends AbstractController {
    @FXML
    private AnchorPane pane;
    @FXML
    private Button button_accept;
    @FXML
    private Button button_cancel;
    @FXML
    private Label licence;
    @FXML
    private Button button_close;


    @FXML
    private void actionAccept(ActionEvent event) throws IOException {
        callback.call(object);
        close((Node) event.getSource());

    }

    @FXML
    private void closeAction(ActionEvent event) {
        close((Node) event.getSource());
    }

    @Override
    public Model getModel() {
        return DriverModel.getInstance();
    }

    @Override
    public void init() {

    }

    @Override
    public void preShow() {
        licence.setText(((DriverCommon) object).getLicence());
    }

    @Override
    public List<AbstractController> getChildControllers() {
        return null;
    }
}
