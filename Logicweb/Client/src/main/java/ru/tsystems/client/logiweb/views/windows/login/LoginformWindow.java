package ru.tsystems.client.logiweb.views.windows.login;

import javafx.stage.Stage;
import ru.tsystems.client.logiweb.views.UIClass;

/**
 * Created by dsing on 09.08.14.
 */
public class LoginformWindow extends UIClass {
    public LoginformWindow(Stage stage) throws Exception {
        super(stage,"fxmls/login/loginform.fxml", 365,225);
    }

    @Override
    protected String getLocalNameCSS() {
        return "login.css";
    }

}
