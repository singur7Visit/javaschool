/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.tsystems.client.logiweb.controllers.driver.info;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import ru.tsystems.client.logiweb.controllers.AbstractController;
import ru.tsystems.client.logiweb.models.Model;
import ru.tsystems.logiweb.common.entities.impl.DriverCommon;

/**
 * FXML Controller class
 *
 * @author dsing
 */
public class DriverformInfoTruckController extends AbstractController{
    @FXML
    private AnchorPane childPane;
    @FXML
    private Label numberLabel;
    @FXML
    private Label classLabel;
    @FXML
    private Label countLabel;

    @Override
    public Model getModel() {
        return null;
    }

    @Override
    public void init() {

    }

    @Override
    public void preShow() {
        if(object != null && ((DriverCommon)object).getTruck() != null) {
            numberLabel.setText(((DriverCommon)object).getTruck().getRegNumber() == null ? "" :
                    ((DriverCommon)object).getTruck().getRegNumber());
            countLabel.setText(String.valueOf(((DriverCommon)object).getTruck().getCountDrivers()));
            classLabel.setText(((DriverCommon)object).getTruck().getClassCapacity() != null ?
                    ((DriverCommon)object).getTruck().getClassCapacity().getName() : "");
        }
    }

    @Override
    public List<AbstractController> getChildControllers() {
        return null;
    }
}
