package ru.tsystems.client.logiweb.controllers;

import ru.tsystems.logiweb.common.entities.IObject;

import java.io.IOException;

/**
 * Created by dsing on 13.08.14.
 */
public interface Callback<T> {
    void call(T object) throws IOException;
}
