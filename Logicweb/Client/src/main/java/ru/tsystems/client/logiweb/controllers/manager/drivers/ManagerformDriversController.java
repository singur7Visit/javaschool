/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.tsystems.client.logiweb.controllers.manager.drivers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import ru.tsystems.client.logiweb.controllers.AbstractController;
import ru.tsystems.client.logiweb.controllers.Callback;
import ru.tsystems.client.logiweb.models.impl.DriverModel;
import ru.tsystems.client.logiweb.views.AlertDialog;
import ru.tsystems.client.logiweb.views.UIClass;
import ru.tsystems.client.logiweb.views.windows.manager.drivers.ManagerformDriversAddWindow;
import ru.tsystems.client.logiweb.views.windows.manager.drivers.ManagerformDriversRemoveWindow;
import ru.tsystems.logiweb.common.entities.IObject;
import ru.tsystems.logiweb.common.entities.impl.DriverCommon;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

/**
 * FXML Controller class
 *
 * @author dsing
 */
public class ManagerformDriversController extends AbstractController {
    @FXML
    private Button addDriverButton;
    @FXML
    private Button removeDriverButton;
    @FXML
    private Button refreshButton;
    @FXML
    private TableView<DriverCommon> tableDrivers;

    @FXML
    private TextField searchField;

    @FXML
    private Pagination paginationPane;

    @FXML
    private ComboBox countRows;
    @FXML
    TableColumn snmColumn;
    @FXML
    TableColumn licenceColumn;
    @FXML
    TableColumn statusColumn;



    @FXML
    private void changeCountRows(ActionEvent event) throws Exception {
        updateTable(paginationPane.getCurrentPageIndex(),Integer.parseInt((String) countRows.getSelectionModel().getSelectedItem()));
    }

    @FXML
    private void clickAdd(final ActionEvent event) throws Exception {
        UIClass window = new ManagerformDriversAddWindow();
        window.getController().setCallback(new Callback<DriverCommon>() {
            @Override
            public void call(final DriverCommon object) {
                Map<String,Serializable> response = newTaskTimeout(new Callable<Map<String, Serializable>>() {
                    @Override
                    public Map<String, Serializable> call() throws Exception {
                        return getModel().createDriver(object);
                    }
                },3);
                if(response != null && response.get("Driver") != null && (Boolean) response.get("Status"))
                    tableDrivers.getItems().add((DriverCommon) response.get("Driver"));
                else
                    AlertDialog.error((Stage) ((Node)event.getSource()).getScene().getWindow(),"Error created driver");
            }
        });
        Node node = (Node) event.getSource();
        window.setModality(node, Modality.WINDOW_MODAL);
        window.show();
    }

    @FXML
    private void clickRemove(final ActionEvent event) throws Exception {
        if(tableDrivers.getSelectionModel().getSelectedItem() == null)
            return;
        UIClass window = new ManagerformDriversRemoveWindow();
        window.getController().setCallback(new Callback<DriverCommon>() {
            @Override
            public void call(final DriverCommon object) {
                Boolean response = (Boolean) newTaskTimeout(new Callable<Boolean>() {
                    @Override
                    public Boolean call() throws Exception {
                        return getModel().removeDriver(object);
                    }
                },3);
                if(response == null || !response) {
                    AlertDialog.error((Stage) ((Node)event.getSource()).getScene().getWindow(),"Cann't remove driver");
                }
                updateTable();
            }
        });
        final DriverCommon selectedDriver = tableDrivers.getSelectionModel().getSelectedItem();
        window.getController().setObject(selectedDriver);
        Node node = (Node) event.getSource();
        window.setModality(node, Modality.WINDOW_MODAL);
        window.show();
    }

    @FXML
    private void searchKeyPressed(KeyEvent event) {
        updateTable(searchField.getText());
    }

    @FXML
    private void refreshAction(ActionEvent event) {
        updateTable();
    }

    private void updateTable(String text) {
        int count = Integer.parseInt((String) countRows.getSelectionModel().getSelectedItem());
        if(text != null)
            updateTable(0,count,text);
        else
            updateTable(0,count);
    }

    private void updateTable(final int from, final int to) {
        updateTable(from, to,null);
    }


    @Override
    public DriverModel getModel() {
        return DriverModel.getInstance();
    }

    @Override
    public void init() {
        paginationPane.getStyleClass().add(Pagination.STYLE_CLASS_BULLET);
        countRows.getItems().addAll("20", "50", "100", "200");
        countRows.getSelectionModel().select("20");
        updateTable(0, 20);
        snmColumn.setCellValueFactory(new PropertyValueFactory<DriverCommon, String>("SNM"));
        licenceColumn.setCellValueFactory(new PropertyValueFactory<DriverCommon,String>("licence"));
        statusColumn.setCellValueFactory(new PropertyValueFactory<DriverCommon,String>("status"));
    }



    private void updateTable(final int from, final int to, final String searchWord) {
        List<DriverCommon> drivers = newTaskTimeout(new Callable<List<DriverCommon>>() {
            @Override
            public List<DriverCommon> call() throws Exception {
                return getModel().getAllDrivers(from, to,searchWord);
            }
        },1);
        tableDrivers.getItems().clear();
        if(drivers != null) {
            tableDrivers.getItems().addAll(drivers);
            paginationPane.setPageCount(drivers.size() / 20 + 1);
        }
        else {
            paginationPane.setPageCount(1);
        }
        paginationPane.setCurrentPageIndex(0);
    }

    private void updateTable() {
        updateTable(null);
    }
    @Override
    public void preShow() {
    }

    @Override
    public List<AbstractController> getChildControllers() {
        return null;
    }
}
