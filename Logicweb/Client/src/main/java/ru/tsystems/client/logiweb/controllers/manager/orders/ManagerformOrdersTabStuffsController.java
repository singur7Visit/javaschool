/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.tsystems.client.logiweb.controllers.manager.orders;

import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import ru.tsystems.client.logiweb.controllers.AbstractController;
import ru.tsystems.client.logiweb.models.impl.OrderModel;
import ru.tsystems.logiweb.common.entities.impl.OrderCommon;
import ru.tsystems.logiweb.common.entities.impl.StuffCommon;

import java.math.BigDecimal;
import java.util.List;

/**
 * FXML Controller class
 *
 * @author dsing
 */
public class ManagerformOrdersTabStuffsController extends AbstractController{
    @FXML
    private TableView<StuffCommon> tableStuffs;
    @FXML
    private TableColumn gpsColumn;
    @FXML
    private TableColumn nameColumn;
    @FXML
    private TableColumn weightColumn;
    @FXML
    private TableColumn statusColumn;


    @Override
    public List<AbstractController> getChildControllers() {
        return null;
    }

    @Override
    public OrderModel getModel() {
        return OrderModel.getInstance();
    }

    @Override
    public void init() {
        gpsColumn.setCellValueFactory(new PropertyValueFactory<StuffCommon,String>("gps"));
        nameColumn.setCellValueFactory(new PropertyValueFactory<StuffCommon,String>("name"));
        weightColumn.setCellValueFactory(new PropertyValueFactory<StuffCommon,BigDecimal>("weight"));
        statusColumn.setCellValueFactory(new PropertyValueFactory<StuffCommon,Boolean>("status"));
    }

    @Override
    public void preShow() {
        tableStuffs.getItems().addAll(((OrderCommon)object).getStuffs());
    }
}
