package ru.tsystems.client.logiweb.controllers.manager.trucks;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.KeyEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import ru.tsystems.client.logiweb.controllers.AbstractController;
import ru.tsystems.client.logiweb.controllers.Callback;
import ru.tsystems.client.logiweb.models.Model;
import ru.tsystems.client.logiweb.models.impl.TruckModel;
import ru.tsystems.client.logiweb.views.AlertDialog;
import ru.tsystems.client.logiweb.views.windows.manager.trucks.ManagerformTrucksAddWindow;
import ru.tsystems.client.logiweb.views.windows.manager.trucks.ManagerformTrucksRemoveWindow;
import ru.tsystems.client.logiweb.views.UIClass;
import ru.tsystems.logiweb.common.entities.IObject;
import ru.tsystems.logiweb.common.entities.impl.DriverCommon;
import ru.tsystems.logiweb.common.entities.impl.TruckCommon;

import java.io.Serializable;
import java.net.URL;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.concurrent.Callable;

/**
 * FXML Controller class
 *
 * @author dsing
 */
public class ManagerformTrucksController extends AbstractController {
    @FXML
    private Button addTruckButton;
    @FXML
    private Button editTruckButton;
    @FXML
    private Button removeTruckButton;
    @FXML
    private Button refreshButton;
    @FXML
    private TableView<TruckCommon> tableTrucks;

    @FXML
    private TextField searchField;

    @FXML
    private ComboBox countRows;

    @FXML
    private Pagination paginationPane;

    @FXML
    TableColumn numberColumn;

    @FXML
    TableColumn countDriversColumn;

    @FXML
    TableColumn classColumn;

    @FXML
    TableColumn statusColumn;

    @FXML
    private void searchKeyPressed(KeyEvent event) {
        updateTable(searchField.getText());
    }

    @FXML
    private void refreshAction(ActionEvent event) {
        updateTable();
    }

    @FXML
    private void changeCountRows(ActionEvent event) throws Exception {
        updateTable(paginationPane.getCurrentPageIndex(), Integer.parseInt((String) countRows.getSelectionModel().getSelectedItem()));
    }

    @FXML
    private void clickRemove(final ActionEvent actionEvent) throws Exception {
        if(tableTrucks.getSelectionModel().getSelectedItem() == null)
            return;
        UIClass window = new ManagerformTrucksRemoveWindow();
        window.getController().setCallback(new Callback<TruckCommon>() {
            @Override
            public void call(final TruckCommon object) {
                Boolean response = (Boolean) newTaskTimeout(new Callable<Object>() {
                    @Override
                    public Object call() throws Exception {
                        return getModel().removeTruck(object);
                    }
                },3);
                if(response == null || !response) {
                    AlertDialog.error((Stage) ((Node)actionEvent.getSource()).getScene().getWindow(),"Cann't remove truck");
                }
                updateTable();
            }
        });
        final TruckCommon selectedTruck = tableTrucks.getSelectionModel().getSelectedItem();
        window.getController().setObject(selectedTruck);
        Node node = (Node) actionEvent.getSource();
        window.setModality(node, Modality.WINDOW_MODAL);
        window.show();
    }

    @FXML
    private void clickAdd(final ActionEvent actionEvent) throws Exception {
        UIClass window = new ManagerformTrucksAddWindow();
        window.getController().setCallback(new Callback<TruckCommon>() {
            @Override
            public void call(final TruckCommon object) {
                Map<String,Serializable> response = newTaskTimeout(new Callable<Map<String,Serializable>>() {
                    @Override
                    public Map<String,Serializable> call() throws Exception {
                        return getModel().createTruck(object);
                    }
                },3);
                if(response != null && response.get("Truck") != null && (Boolean) response.get("Status"))
                    tableTrucks.getItems().add((TruckCommon) response.get("Truck"));
                else
                    AlertDialog.error((Stage) ((Node) actionEvent.getSource()).getScene().getWindow(), "Error created truck");
            }
        });
        Node node = (Node) actionEvent.getSource();
        window.setModality(node, Modality.WINDOW_MODAL);
        window.show();
    }

    @Override
    public TruckModel getModel() {
        return TruckModel.getInstance();
    }

    @Override
    public void init() {
        paginationPane.getStyleClass().add(Pagination.STYLE_CLASS_BULLET);
        countRows.getItems().addAll("20", "50", "100", "200");
        countRows.getSelectionModel().select("20");
        updateTable(0, 20);
        numberColumn.setCellValueFactory(new PropertyValueFactory<TruckCommon, String>("regNumber"));
        countDriversColumn.setCellValueFactory(new PropertyValueFactory<TruckCommon, String>("countDrivers"));
        classColumn.setCellValueFactory(new PropertyValueFactory<TruckCommon, String>("classCapacity"));
        statusColumn.setCellValueFactory(new PropertyValueFactory<TruckCommon, String>("status"));
    }

    private void updateTable(final int from, final int to, final String searchWord) {
        List<TruckCommon> trucks = newTaskTimeout(new Callable<List<TruckCommon>>() {
            @Override
            public List<TruckCommon> call() throws Exception {
                return getModel().getAllTrucks(from, to, searchWord);
            }
        },1);
        tableTrucks.getItems().clear();
        if(trucks != null) {
            tableTrucks.getItems().addAll(trucks);
            paginationPane.setPageCount(trucks.size() / 20 + 1);
        }
        else {
            paginationPane.setPageCount(1);
        }
        paginationPane.setCurrentPageIndex(0);
    }

    private void updateTable() {
        updateTable(null);
    }
    private void updateTable(final int from, final int to) {
        updateTable(from, to,null);
    }

    private void updateTable(String text) {
        int count = Integer.parseInt((String) countRows.getSelectionModel().getSelectedItem());
        if(text != null)
            updateTable(0,count,text);
        else
            updateTable(0,count);
    }

    @Override
    public void preShow() {

    }

    @Override
    public List<AbstractController> getChildControllers() {
        return null;
    }
}
