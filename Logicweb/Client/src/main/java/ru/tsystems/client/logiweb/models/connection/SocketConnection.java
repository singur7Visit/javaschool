package ru.tsystems.client.logiweb.models.connection;

import org.apache.log4j.Logger;

import java.io.*;
import java.net.Socket;

/**
 * Created by dsing on 15.08.14.
 */
public class SocketConnection implements IConnection {
    private Socket socket;
    private static final Logger logger = Logger.getLogger(SocketConnection.class);

    public SocketConnection(String host, int port) {
        try {
            socket = new Socket(host,port);
        } catch (IOException e) {
            logger.error("Could not connection to " + host + ":" + port + ". Message : " + e.getMessage());
        }
    }

    @Override
    public ObjectInputStream getObjectInputStream() {
        try {
            return new ObjectInputStream(new BufferedInputStream(socket.getInputStream()));
        } catch (IOException e) {
            logger.error("Error socket :" + e.getMessage());
            return null;
        }
    }

    @Override
    public ObjectOutputStream getObjectOutputStream() {
        try {
            return new ObjectOutputStream(new BufferedOutputStream(socket.getOutputStream()));
        } catch (IOException e) {
            logger.error("Error socket :" + e.getMessage());
            return null;
        }
    }

    @Override
    public void close() {
        try {
            if(socket != null)
                socket.close();
        } catch (IOException e) {
            logger.error("Error socket :" + e.getMessage());
        }
    }
}
