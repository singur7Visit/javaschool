package ru.tsystems.client.logiweb.models.impl;

import org.apache.log4j.Logger;
import ru.tsystems.client.logiweb.models.Model;
import ru.tsystems.logiweb.common.commands.Command;
import ru.tsystems.logiweb.common.commands.ICommand;
import ru.tsystems.logiweb.common.entities.impl.UserCommon;

import java.io.IOException;
import java.io.Serializable;
import java.util.Map;

/**
 * Created by dsing on 16.08.14.
 */
public class UserModel extends Model {
    private static UserModel userModel;
    private UserCommon userCommon;
    private final static Logger logger = Logger.getLogger(UserModel.class);

    public static UserModel getInstance() {
        if(userModel == null)
            userModel = new UserModel();
        return userModel;
    }

    protected UserModel() {

    }

    public Map<String,Serializable> login(String login, String password) {
        ICommand command = new Command();
        UserCommon userCommon = new UserCommon();
        userCommon.setLogin(login);
        userCommon.setPassword(password);
        this.userCommon = userCommon;
        command.addParam("User", userCommon);
        command.addParam("Status",Boolean.FALSE);
        command.setCommand("login");
        try {
            sendCommand(command);
            ICommand response = readResponse();
            return response.getNamedParams("Status","User");
        } catch (IOException | ClassNotFoundException | ClassCastException | NullPointerException e) {
            logger.error("Cann't get info about userCommon from server. Message :" + e.getMessage());
            closeConnection();
            return null;
        }
    }

    public UserCommon getUserCommon() {
        return userCommon;
    }
}
