package ru.tsystems.client.logiweb.views.windows.manager.drivers;

import ru.tsystems.client.logiweb.views.UIClass;

/**
 * Created by dsing on 12.08.14.
 */
public class ManagerformDriversRemoveWindow extends UIClass {
    public ManagerformDriversRemoveWindow() throws Exception {
        super("fxmls/manager/drivers/managerform_drivers_remove.fxml",600,111);
    }

    @Override
    protected String getLocalNameCSS() {
        return "manager_remove_drivers.css";
    }
}
