package ru.tsystems.client.logiweb.views;

import javafx.event.EventHandler;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.input.MouseEvent;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import org.apache.log4j.Logger;
import ru.tsystems.client.logiweb.controllers.IController;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;

/**
 * Created by dsing on 09.08.14.
 */
public abstract class UIClass {

    protected Stage stage;
    private final static Logger logger = Logger.getLogger(UIClass.class);
    private double xOffset = 0;
    private double yOffset = 0;
    private FXMLLoader loader;

    public UIClass(String pathToView, double width, double height) throws Exception {
        this(new Stage(),pathToView, width, height);
    }

    public UIClass(final Stage stage, String pathToView, double width, double height) throws Exception {
        loader = new FXMLLoader(UIClass.class.getResource(pathToView));
        this.stage = stage;
        stage.initStyle(StageStyle.TRANSPARENT);
        stage.setResizable(false);
        Parent root;
        try {
            root = (Parent) loader.load();
        } catch (IOException e) {
            logger.error("Cann't find view. Path to view : " + pathToView);
            throw e;
        }
        delegateMouse(root);
        Scene scene = new Scene(root,width,height);
        if(getCSS() != null)
            scene.getStylesheets().addAll(getCSS().toExternalForm());
        stage.setScene(scene);
    }

    private void delegateMouse(Parent root) {
        root.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                xOffset = event.getSceneX();
                yOffset = event.getSceneY();
            }
        });
        root.setOnMouseDragged(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                stage.setX(event.getScreenX() - xOffset);
                stage.setY(event.getScreenY() - yOffset);
            }
        });
    }

    private URL getCSS() throws Exception {
        URL resource = getClass().getResource("/resources/css/" + getLocalNameCSS());
        if (resource == null)  {
            logger.warn("Cann't find stylesheets file.");
        }
        return resource;
    };

    protected abstract String getLocalNameCSS();

    public void setTitle(String title) {
        getController().setTitle(title);
    }

    public void setModality(Node parentNode, Modality modal) {
        stage.initModality(modal);
        stage.initOwner(parentNode.getScene().getWindow());
    }

    public IController getController() {
        return loader.getController();
    }
    public void show() {
        getController().preShow();
        stage.show();
    }


}
