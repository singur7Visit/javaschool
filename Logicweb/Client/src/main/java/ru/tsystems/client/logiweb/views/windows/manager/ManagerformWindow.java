package ru.tsystems.client.logiweb.views.windows.manager;

import ru.tsystems.client.logiweb.views.UIClass;

/**
 * Created by dsing on 10.08.14.
 */
public class ManagerformWindow extends UIClass {

    public ManagerformWindow() throws Exception {
        super("fxmls/manager/managerform.fxml", 1024,600);
    }

    @Override
    protected String getLocalNameCSS() {
        return "manager.css";
    }


}
