package ru.tsystems.client.logiweb.views.windows.manager.trucks;

import ru.tsystems.client.logiweb.views.UIClass;

/**
 * Created by dsing on 11.08.14.
 */
public class ManagerformTrucksRemoveWindow extends UIClass {
    public ManagerformTrucksRemoveWindow() throws Exception {
        super("fxmls/manager/trucks/managerform_trucks_remove.fxml",600,111);
    }

    @Override
    protected String getLocalNameCSS() {
        return "manager_remove_trucks.css";
    }
}
