/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.tsystems.client.logiweb.controllers.manager.orders;

import java.io.IOException;
import java.math.BigInteger;
import java.net.URL;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.Callable;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import ru.tsystems.client.logiweb.controllers.AbstractController;
import ru.tsystems.client.logiweb.controllers.Callback;
import ru.tsystems.client.logiweb.controllers.IController;
import ru.tsystems.client.logiweb.models.impl.OrderModel;
import ru.tsystems.client.logiweb.views.AlertDialog;
import ru.tsystems.client.logiweb.views.UIClass;
import ru.tsystems.client.logiweb.views.windows.manager.orders.ManagerformOrdersAddStuffsWindow;
import ru.tsystems.logiweb.common.entities.IObject;
import ru.tsystems.logiweb.common.entities.impl.OrderCommon;
import ru.tsystems.logiweb.common.entities.impl.StatusOrderCommon;
import ru.tsystems.logiweb.common.entities.impl.TruckCommon;

/**
 * FXML Controller class
 *
 * @author dsing
 */
public class ManagerformOrdersController extends AbstractController {
    @FXML
    private Pagination paginationPane;
    @FXML
    private TableView<OrderCommon> tableOrders;
    @FXML
    private ScrollPane scrollPane;
    @FXML
    private ToolBar toolbarOrders;
    @FXML
    private AnchorPane paneOnToolbarOrders;
    @FXML
    private Button refreshButton;
    @FXML
    private Button closeOrder;
    @FXML
    private Button removeButton;
    @FXML
    private TextField searchField;
    @FXML
    private ComboBox<Integer> countRows;
    @FXML
    private TableColumn idOrderColumn;
    @FXML
    private TableColumn truckColumnOrders;
    @FXML
    private TableColumn statusColumnOrders;
    @FXML
    private Tab driverTabOrders;
    @FXML
    private Tab stuffsTabOrders;
    @FXML
    private Tab truckTabOrders;
    @FXML
    private ScrollPane scrollPaneTabDrivers;
    @FXML
    private ScrollPane scrollPaneTabStuffs;
    @FXML
    private ScrollPane scrollPaneTabTruck;

    private int currentPage;
    private int countPage;
    private FXMLLoader tabDrivers;
    private FXMLLoader tabStuffs;
    private FXMLLoader tabTruck;




    @Override
    public List<AbstractController> getChildControllers() {
        return tabTruck != null && tabDrivers != null && tabStuffs != null ?Arrays.asList(
                tabTruck.<AbstractController>getController(),
                tabDrivers.<AbstractController>getController(),
                tabStuffs.<AbstractController>getController()) : null;
    }

    @FXML
    private void changeCountRows(ActionEvent event) {
        updateTable();
    }

    @FXML
    private void selectionDriversTabOrders(Event event) throws IOException {
    }

    @FXML
    private void selectionStuffsTabOrders(Event event) throws IOException {
    }

    @FXML
    private void selectionTruckTabOrders(Event event) {
    }

    @FXML
    private void removeOrderAction(final ActionEvent event) {
        if(tableOrders.getSelectionModel().getSelectedItem() == null)
            return;
        Boolean remove = (Boolean) newTaskTimeout(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                return getModel().removeOrder(tableOrders.getSelectionModel().getSelectedItem());
            }
        },3);

        if(remove == null || !remove)
            AlertDialog.error((Stage) ((Node)event.getSource()).getScene().getWindow(),"Cann't this order");
        else
            tableOrders.getItems().remove(tableOrders.getSelectionModel().getSelectedItem());
        repaint(true);
    }

    @FXML
    private void addOrderAction(final ActionEvent event) throws Exception {
        repaint(true);
        UIClass window = new ManagerformOrdersAddStuffsWindow();
        IController controller = window.getController();
        OrderCommon order = newTaskTimeout(new Callable<OrderCommon>() {
            @Override
            public OrderCommon call() throws Exception {
                return getModel().createOrder(new OrderCommon());
            }
        }, 3);
        if (order == null) {
            AlertDialog.error((Stage) ((Node) event.getSource()).getScene().getWindow(), "Cann't create order");
            return;
        }
        controller.setObject(order);
        controller.setCallback(new Callback<OrderCommon>() {
            @Override
            public void call(final OrderCommon o) {
                OrderCommon newOrder = newTaskTimeout(new Callable<OrderCommon>() {
                    @Override
                    public OrderCommon call() throws Exception {
                        return getModel().shipOrder(o);
                    }
                }, 3);
                if (newOrder == null) {
                    AlertDialog.error((Stage) ((Node) event.getSource()).getScene().getWindow(), "Cann't shipped");
                    updateTable();
                }
                else
                    tableOrders.getItems().add(newOrder);
                updateTable();
            }
        });
        window.setModality((Node) event.getSource(), Modality.WINDOW_MODAL);
        window.show();
    }

    @FXML
    private void clickTable(MouseEvent event) throws IOException {
        if (tableOrders.getSelectionModel().getSelectedItem() != null) {
            repaint(false);
            tabDrivers = new FXMLLoader(UIClass.class.getResource("fxmls/manager/orders/tabs/managerform_tab_drivers.fxml"));
            tabStuffs = new FXMLLoader(UIClass.class.getResource("fxmls/manager/orders/tabs/managerform_tab_stuffs.fxml"));
            tabTruck = new FXMLLoader(UIClass.class.getResource("fxmls/manager/orders/tabs/managerform_tab_truck.fxml"));
            truckTabOrders.setContent((Node) tabTruck.load());
            stuffsTabOrders.setContent((Node) tabStuffs.load());
            driverTabOrders.setContent((Node) tabDrivers.load());
            OrderCommon orderCommon = newTaskTimeout(new Callable<OrderCommon>() {
                @Override
                public OrderCommon call() throws Exception {
                    return getModel().getAllInformation(tableOrders.getSelectionModel().getSelectedItem());
                }
            },3);
            ((IController) tabTruck.getController()).setObject(orderCommon);
            ((IController) tabStuffs.getController()).setObject(orderCommon);
            ((IController) tabDrivers.getController()).setObject(orderCommon);
            ((IController) tabTruck.getController()).preShow();
            ((IController) tabStuffs.getController()).preShow();
            ((IController) tabDrivers.getController()).preShow();
        }
    }


    private void repaint(boolean isBig) {
        if (isBig) {
            paginationPane.setPrefWidth(903);
            tableOrders.setPrefWidth(903);
            toolbarOrders.setPrefWidth(903);
            idOrderColumn.setPrefWidth(430);
            truckColumnOrders.setPrefWidth(315);
            statusColumnOrders.setPrefWidth(130);
        } else {
            paginationPane.setPrefWidth(478);
            tableOrders.setPrefWidth(478);
            toolbarOrders.setPrefWidth(478);
            idOrderColumn.setPrefWidth(210);
            truckColumnOrders.setPrefWidth(145);
            statusColumnOrders.setPrefWidth(105);
        }
    }

    @FXML
    private void closeOrderAction(ActionEvent actionEvent) {
        final OrderCommon selectedOrder = tableOrders.getSelectionModel().getSelectedItem();
        if(selectedOrder == null)
            return;
        if(selectedOrder.getStatus() == null || !"completed".equals(selectedOrder.getStatus().getName()))
            return;
        OrderCommon orderCommon = newTaskTimeout(new Callable<OrderCommon>() {
            @Override
            public OrderCommon call() throws Exception {
                return getModel().closeOrder(selectedOrder);
            }
        },3);
        if(orderCommon == null)
            AlertDialog.error((Stage) ((Node)actionEvent.getSource()).getScene().getWindow(),"Cann't closed order");
        else
            AlertDialog.info((Stage) ((Node)actionEvent.getSource()).getScene().getWindow(),"Order closed");

    }

    @Override
    public OrderModel getModel() {
        return OrderModel.getInstance();
    }

    @Override
    public void init() {
        paginationPane.getStyleClass().add(Pagination.STYLE_CLASS_BULLET);
        countRows.getItems().addAll(20, 50, 100, 200);
        countRows.getSelectionModel().select(new Integer(20));
        updateTable(0, 20);
        idOrderColumn.setCellValueFactory(new PropertyValueFactory<OrderCommon, BigInteger>("numOrder"));
        truckColumnOrders.setCellValueFactory(new PropertyValueFactory<OrderCommon, TruckCommon>("truck"));
        statusColumnOrders.setCellValueFactory(new PropertyValueFactory<OrderCommon, StatusOrderCommon>("status"));
        repaint(true);
        paginationPane.currentPageIndexProperty().addListener(new ChangeListener<Number>() {
            @Override
            public void changed(ObservableValue<? extends Number> observableValue, Number number, Number number2) {
                currentPage = number2.intValue();
                updateTable(currentPage * countRows.getSelectionModel().getSelectedItem(),
                        currentPage* countRows.getSelectionModel().getSelectedItem() +
                                countRows.getSelectionModel().getSelectedItem());
            }
        });
    }

    private void updateTable(final int from, final int to, final String searchWord) {
        List<OrderCommon> trucks = newTaskTimeout(new Callable<List<OrderCommon>>() {
            @Override
            public List<OrderCommon> call() throws Exception {
                return getModel().getAllOrders(from, to, searchWord);
            }
        }, 1);
        tableOrders.getItems().clear();
        if (trucks != null) {
            tableOrders.getItems().addAll(trucks);
            if(currentPage == 0 && trucks.size() / countRows.getSelectionModel().getSelectedItem() + 1 > countPage) {
                countPage = trucks.size() / countRows.getSelectionModel().getSelectedItem() + 1;
                paginationPane.setPageCount(trucks.size() / 20 + 1);
            }
        } else {
            paginationPane.setPageCount(1);
        }
    }

    private void updateTable() {
        updateTable(null);
    }

    private void updateTable(final int from, final int to) {
        updateTable(from, to, null);
    }

    private void updateTable(String text) {
        int count = countRows.getSelectionModel().getSelectedItem();
        if (text != null)
            updateTable(0, count, text);
        else
            updateTable(0, count);
    }

    @Override
    public void preShow() {

    }
}
