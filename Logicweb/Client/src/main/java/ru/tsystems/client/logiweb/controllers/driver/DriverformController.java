/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.tsystems.client.logiweb.controllers.driver;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import ru.tsystems.client.logiweb.controllers.AbstractController;
import ru.tsystems.client.logiweb.controllers.IController;
import ru.tsystems.client.logiweb.models.impl.DriverModel;
import ru.tsystems.client.logiweb.views.UIClass;
import ru.tsystems.logiweb.common.entities.impl.DriverCommon;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.Callable;

/**
 * FXML Controller class
 *
 * @author dsing
 */
public class DriverformController extends AbstractController {
    @FXML
    private AnchorPane pane;
    @FXML
    private Button closeButton;
    @FXML
    private Button minimizeButton;
    @FXML
    private Label info;
    @FXML
    private Label management;
    @FXML
    private ScrollPane contentPane;

    private FXMLLoader inf;
    private FXMLLoader manage;

    @FXML
    private void onClickInfo(MouseEvent event) throws IOException {
        clearClasses();
        info.getStyleClass().addAll("pressed");
        contentPane.setContent(getInfoNode());
    }

    @FXML
    private void closeAction(ActionEvent event) {
        close((Node) event.getSource());
    }

    @FXML
    private void minimizeAction(ActionEvent event) {
        minimize((Node) event.getSource());
    }

    @FXML
    private void onClickManagement(MouseEvent event) {
        clearClasses();
        management.getStyleClass().addAll("pressed");
        contentPane.setContent(getManagementNode());
    }

    private void clearClasses() {
        info.getStyleClass().removeAll("pressed");
        management.getStyleClass().removeAll("pressed");
    }

    @Override
    public DriverModel getModel() {
        return DriverModel.getInstance();
    }

    @Override
    public void init() {

    }

    @Override
    public void preShow() {
        contentPane.setContent(getInfoNode());
    }

    private Node getInfoNode() {
        try {

            inf = new FXMLLoader(UIClass.class.getResource("fxmls/driver/info/driverform_info.fxml"));
            Node n = (Node) inf.load();
            DriverCommon driver = newTaskTimeout(new Callable<DriverCommon>() {
                @Override
                public DriverCommon call() throws Exception {
                    return getModel().getDriver(((DriverCommon)object).getLicence());
                }
            },3);
            object = driver == null ? object : driver;
            ((IController)inf.getController()).setObject(object);
            ((IController)inf.getController()).preShow();
            return n;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    private Node getManagementNode() {
        try {
            manage = new FXMLLoader(UIClass.class.getResource("fxmls/driver/management/driverform_management.fxml"));
            Node n = (Node) manage.load();
            DriverCommon driver = newTaskTimeout(new Callable<DriverCommon>() {
                @Override
                public DriverCommon call() throws Exception {
                    return getModel().getDriver(((DriverCommon)object).getLicence());
                }
            },3);
            object = driver == null ? object : driver;
            ((IController)manage.getController()).setObject(object);
            ((IController)manage.getController()).preShow();
            return n;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public List<AbstractController> getChildControllers() {
        return null;
    }
}
