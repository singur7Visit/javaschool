package ru.tsystems.client.logiweb.models.impl;

import org.apache.log4j.Logger;
import ru.tsystems.client.logiweb.models.Model;

/**
 * Created by dsing on 19.08.14.
 */
public class ManagerModel extends Model {
    private static ManagerModel managerModel;
    private final static Logger logger = Logger.getLogger(DriverModel.class);

    public static ManagerModel getInstance() {
        if(managerModel == null)
            managerModel = new ManagerModel();
        return managerModel;
    }

    protected ManagerModel() {

    }
}
