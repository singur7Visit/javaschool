package ru.tsystems.client.logiweb.models.impl;

import com.sun.org.apache.xpath.internal.operations.Bool;
import org.apache.log4j.Logger;
import ru.tsystems.client.logiweb.models.Model;
import ru.tsystems.logiweb.common.commands.Command;
import ru.tsystems.logiweb.common.commands.ICommand;
import ru.tsystems.logiweb.common.entities.impl.DriverCommon;
import ru.tsystems.logiweb.common.entities.impl.OrderCommon;
import ru.tsystems.logiweb.common.entities.impl.StuffCommon;

import java.io.IOException;
import java.io.Serializable;
import java.sql.Driver;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by dsing on 14.08.14.
 */
public class DriverModel extends Model {
    private static DriverModel driverModel;
    private final static Logger logger = Logger.getLogger(DriverModel.class);

    public static DriverModel getInstance() {
        if(driverModel == null)
            driverModel = new DriverModel();
        return driverModel;
    }

    protected DriverModel() {

    }

    public DriverCommon getDriver(String licence) {
        ICommand command = new Command();
        command.addParam("licence",licence);
        command.setCommand("GetDriver");
        try {
            sendCommand(command);
            ICommand response = readResponse();
            if(response != null && !(Boolean)response.getParam("Status"))
                logger.warn("Driver cann't be removing. Message : " + response.getParam("Message"));
            return (DriverCommon) response.getParam("Driver");
        } catch (IOException | ClassNotFoundException | NullPointerException e) {
            logger.error("Cann't remove driver. Message : " + e.getMessage());
            return null;
        } finally {
            closeConnection();
        }
    }

    public DriverCommon setStatus(Driver driver, String status) {
        ICommand command = new Command();
        command.addParam("status",status);
        command.setCommand("UpdateStatusDriver");
        try {
            sendCommand(command);
            ICommand response = readResponse();
            if(response != null && !(Boolean)response.getParam("Status"))
                logger.warn("Driver cann't be removing. Message : " + response.getParam("Message"));
            return (DriverCommon) response.getParam("Driver");
        } catch (IOException | ClassNotFoundException | NullPointerException e) {
            logger.error("Cann't remove driver. Message : " + e.getMessage());
            return null;
        } finally {
            closeConnection();
        }
    }

    public List<DriverCommon> getAllDrivers(int from, int to,String searchWord) {
        ICommand command = new Command();
        if(searchWord != null) {
            command.setCommand("GetAllDriversSearch");
            command.addParam("SearchWord",searchWord);
        }
        else
            command.setCommand("GetAllDrivers");
        command.addParam("from",from);
        command.addParam("to",to);
        try {
            sendCommand(command);
            ICommand response = readResponse();
            return (List<DriverCommon>) response.getParam("Drivers");
        } catch (IOException | ClassNotFoundException | ClassCastException | NullPointerException e) {
            logger.error("Cann't get Drivers from server. Message : " + e.getMessage());
            return null;
        }
        finally {
            closeConnection();
        }
    }

    public Map<String,Serializable> createDriver(DriverCommon driver) {
        ICommand command = new Command();
        command.addParam("Driver",driver);
        command.setCommand("CreateDriver");
        try {
            sendCommand(command);
            ICommand response = readResponse();
            if(response != null && !(Boolean)response.getParam("Status"))
                logger.warn("Driver cann't be create. Message : " + response.getParam("Message"));
            return response.getNamedParams("Status","Driver");
        } catch (IOException | ClassNotFoundException | NullPointerException e) {
            logger.error("Cann't be create driver. Message : " + e.getMessage());
            return null;
        } finally {
            closeConnection();
        }
    }

    public Boolean removeDriver(DriverCommon driver) {
        ICommand command = new Command();
        command.addParam("Driver",driver);
        command.setCommand("RemoveDriver");
        try {
            sendCommand(command);
            ICommand response = readResponse();
            if(response != null && !(Boolean)response.getParam("Status"))
                logger.warn("Driver cann't be removing. Message : " + response.getParam("Message"));
            return (Boolean) response.getParam("Status");
        } catch (IOException | ClassNotFoundException | NullPointerException e) {
            logger.error("Cann't remove driver. Message : " + e.getMessage());
            return null;
        } finally {
            closeConnection();
        }
    }

    public Boolean isUniqueDriver(String licence) {
        ICommand command = new Command();
        command.addParam("licence",licence);
        command.setCommand("isUniqueDriver");
        try {
            sendCommand(command);
            ICommand response = readResponse();
            return (Boolean) response.getParam("isUnique");
        } catch (IOException | ClassNotFoundException | NullPointerException e) {
            logger.error("Cann't remove driver. Message : " + e.getMessage());
            return null;
        } finally {
            closeConnection();
        }
    }

    public OrderCommon deliveredStuff(StuffCommon stuffCommon) {
        ICommand command = new Command();
        command.setCommand("DeliveredStuff");
        command.addParam("Stuff",stuffCommon);
        try {
            sendCommand(command);
            ICommand response = readResponse();
            return (OrderCommon) response.getParam("Order");
        } catch (IOException | ClassNotFoundException | ClassCastException | NullPointerException e) {
            logger.error("Cann't get Drivers from server. Message : " + e.getMessage());
            return null;
        }
        finally {
            closeConnection();
        }
    }

    public List<DriverCommon> getAvailableDrivers() {
        ICommand command = new Command();
        command.setCommand("GetAvailableDrivers");
        try {
            sendCommand(command);
            ICommand response = readResponse();
            return (List<DriverCommon>) response.getParam("Drivers");
        } catch (IOException | ClassNotFoundException | ClassCastException | NullPointerException e) {
            logger.error("Cann't get Drivers from server. Message : " + e.getMessage());
            return null;
        }
        finally {
            closeConnection();
        }
    }
}
