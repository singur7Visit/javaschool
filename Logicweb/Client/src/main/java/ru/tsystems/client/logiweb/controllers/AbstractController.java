package ru.tsystems.client.logiweb.controllers;

import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.stage.Stage;
import org.apache.log4j.Logger;
import ru.tsystems.logiweb.common.entities.IObject;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.*;

/**
 * Created by dsing on 13.08.14.
 */
public abstract class AbstractController implements IController {

    public Label title;

    private ExecutorService timeout;

    protected Callback backCallback;
    protected Callback callback;
    protected IObject object;
    protected IObject windowObject;
    private final static Logger logger =  Logger.getLogger(AbstractController.class);

    protected void minimize(Node source) {
        Stage stage  = (Stage) source.getScene().getWindow();
        stage.close();
    }

    protected void close(Node source) {
        closeController(this);
        Stage stage = (Stage) source.getScene().getWindow();
        stage.close();
    }
    @Override
    public void setCallback(Callback callback) {
        this.callback = callback;
    }
    @Override
    public void setBackCallback(Callback callback) {
        this.backCallback = callback;
    }

    public void setObject(IObject object) {
        this.object = object;
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        timeout = Executors.newFixedThreadPool(1);
        init();
    }

    protected <T> T newTaskTimeout(Callable<T> c, long time) {
        Future<T> future = timeout.submit(c);
        try {
            return future.get(24, TimeUnit.SECONDS);
        } catch (InterruptedException | ExecutionException | TimeoutException e) {
            future.cancel(true);
            return null;
        }
        finally {
            getModel().closeConnection();
        }
    }

    private void stopAllTasks() {
        if(getModel() != null)
            getModel().closeConnection();
        if(timeout != null)
            timeout.shutdownNow();
    }

    public abstract List<AbstractController> getChildControllers();

    @Override
    public void setTitle(String title) {
        if(this.title != null)
            this.title.setText(title);
    }

    private void closeControllers(List<AbstractController> controllers) {
        for(AbstractController controller : controllers) {
            if(controller != null) {
                if(controller.getChildControllers() != null)
                    closeControllers(controller.getChildControllers());
                controller.stopAllTasks();
            }
        }
    }

    private void closeController(AbstractController controller) {
        if(controller.getChildControllers() != null)
            closeControllers(controller.getChildControllers());
        controller.stopAllTasks();
    }
}
