/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.tsystems.client.logiweb.controllers.driver.info;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import ru.tsystems.client.logiweb.controllers.AbstractController;
import ru.tsystems.client.logiweb.models.Model;
import ru.tsystems.client.logiweb.models.impl.DriverModel;
import ru.tsystems.logiweb.common.entities.impl.DriverCommon;
import ru.tsystems.logiweb.common.entities.impl.StatusDriverCommon;

/**
 * FXML Controller class
 *
 * @author dsing
 */
public class DriverformInfoDriversController extends AbstractController{
    @FXML
    private AnchorPane childPane;
    @FXML
    private TableView<DriverCommon> driversTable;
    @FXML
    private TableColumn snmColumn;
    @FXML
    private TableColumn licenceColumn;
    @FXML
    private TableColumn statusColumn;


    @Override
    public DriverModel getModel() {
        return null;
    }

    @Override
    public void init() {
        snmColumn.setCellValueFactory(new PropertyValueFactory<DriverCommon,String>("SNM"));
        licenceColumn.setCellValueFactory(new PropertyValueFactory<DriverCommon,String>("licence"));
        statusColumn.setCellValueFactory(new PropertyValueFactory<DriverCommon,StatusDriverCommon>("status"));
    }

    @Override
    public void preShow() {
        if(object != null && ((DriverCommon)object).getOrder() != null && ((DriverCommon)object).getOrder().getDrivers() != null) {
            for(DriverCommon driverCommon : (((DriverCommon) object).getOrder().getDrivers())) {
                if(!driverCommon.getLicence().equals(((DriverCommon)object).getLicence())) {
                    driversTable.getItems().add(driverCommon);
                }
            }
        }
    }

    @Override
    public List<AbstractController> getChildControllers() {
        return null;
    }
}
