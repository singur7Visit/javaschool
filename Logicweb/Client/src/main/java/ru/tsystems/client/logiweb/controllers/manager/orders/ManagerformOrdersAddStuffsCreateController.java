/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.tsystems.client.logiweb.controllers.manager.orders;

import java.math.BigDecimal;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import ru.tsystems.client.logiweb.controllers.AbstractController;
import ru.tsystems.client.logiweb.controllers.Callback;
import ru.tsystems.client.logiweb.models.Model;
import ru.tsystems.client.logiweb.models.impl.OrderModel;
import ru.tsystems.client.logiweb.views.AlertDialog;
import ru.tsystems.client.logiweb.views.UIClass;
import ru.tsystems.client.logiweb.views.windows.manager.orders.ManagerformAddStuffsWindow;
import ru.tsystems.logiweb.common.entities.impl.OrderCommon;
import ru.tsystems.logiweb.common.entities.impl.StuffCommon;

/**
 * FXML Controller class
 *
 * @author dsing
 */
public class ManagerformOrdersAddStuffsCreateController extends AbstractController {
    @FXML
    private AnchorPane pane;
    @FXML
    private Button closeButton;
    @FXML
    private Button createButton;
    @FXML
    private Button cancelButton;
    @FXML
    private TextField gpsField;
    @FXML
    private TextField nameField;
    @FXML
    private TextField weightField;


    @FXML
    private void closeAction(ActionEvent event) {
        close((Node) event.getSource());
    }

    @FXML
    private void createAction(ActionEvent event) throws Exception {
        StuffCommon stuffCommon = new StuffCommon();
        stuffCommon.setGps(gpsField.getText());
        stuffCommon.setName(nameField.getText());
        try {
            stuffCommon.setWeight(BigDecimal.valueOf(Double.valueOf(weightField.getText())));
        } catch (Exception e) {
            AlertDialog.error((Stage) ((Node) event.getSource()).getScene().getWindow(),"Wrong format weigth");
            return;
        }
        callback.call(stuffCommon);
        close((Node) event.getSource());
    }


    @Override
    public OrderModel getModel() {
        return OrderModel.getInstance();
    }

    @Override
    public void init() {

    }

    @Override
    public void preShow() {
    }

    @Override
    public List<AbstractController> getChildControllers() {
        return null;
    }
}
