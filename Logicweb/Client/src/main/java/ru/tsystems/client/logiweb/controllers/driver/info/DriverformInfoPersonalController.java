/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.tsystems.client.logiweb.controllers.driver.info;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import ru.tsystems.client.logiweb.controllers.AbstractController;
import ru.tsystems.client.logiweb.models.Model;
import ru.tsystems.logiweb.common.entities.impl.DriverCommon;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

/**
 * FXML Controller class
 *
 * @author dsing
 */
public class DriverformInfoPersonalController extends AbstractController {
    @FXML
    private AnchorPane pane;
    @FXML
    private Label snmLabel;
    @FXML
    private Label licenceLabel;
    @FXML
    private Label statusLabel;

    @Override
    public Model getModel() {
        return null;
    }

    @Override
    public void init() {

    }

    @Override
    public void preShow() {
        if(((DriverCommon)object) != null) {
            snmLabel.setText(((DriverCommon)object).getSNM());
            licenceLabel.setText(((DriverCommon)object).getLicence());
            statusLabel.setText(((DriverCommon)object).getStatus() == null  ? "" : ((DriverCommon)object).getStatus().getName());
        }
    }

    @Override
    public List<AbstractController> getChildControllers() {
        return null;
    }
}
