
package ru.tsystems.client.logiweb.controllers.manager.trucks;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import ru.tsystems.client.logiweb.controllers.AbstractController;
import ru.tsystems.client.logiweb.controllers.IController;
import ru.tsystems.client.logiweb.models.Model;
import ru.tsystems.client.logiweb.models.impl.TruckModel;
import ru.tsystems.logiweb.common.entities.impl.TruckCommon;

/**
 * FXML Controller class
 *
 * @author dsing
 */
public class ManagerformTrucksRemoveController extends AbstractController {
    @FXML
    private AnchorPane pane;
    @FXML
    private Button button_accept;
    @FXML
    private Button button_cancel;
    @FXML
    private Button button_close;

    @FXML
    private Label number;

    @FXML
    private void actionAccept(ActionEvent event) throws IOException {
        callback.call(object);
        close((Node) event.getSource());
    }

    @FXML
    private void closeAction(ActionEvent event) {
        close((Node) event.getSource());
    }

    @Override
    public TruckModel getModel() {
        return TruckModel.getInstance();
    }

    @Override
    public void init() {

    }

    @Override
    public void preShow() {
        number.setText(((TruckCommon)object).getRegNumber());
    }

    @Override
    public List<AbstractController> getChildControllers() {
        return null;
    }
}
