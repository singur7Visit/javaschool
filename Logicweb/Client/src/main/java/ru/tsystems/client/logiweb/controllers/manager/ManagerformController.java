package ru.tsystems.client.logiweb.controllers.manager;

import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;
import ru.tsystems.client.logiweb.controllers.AbstractController;
import ru.tsystems.client.logiweb.controllers.IController;
import ru.tsystems.client.logiweb.models.Model;
import ru.tsystems.client.logiweb.models.impl.ManagerModel;
import ru.tsystems.client.logiweb.views.UIClass;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;

/**
 * Created by dsing on 10.08.14.
 */
public class ManagerformController extends AbstractController {
    @FXML
    private Label trucks;
    @FXML
    private Label drivers;
    @FXML
    private Label orders;
    @FXML
    private Button close_button;
    @FXML
    private Button minimize_button;
    @FXML
    private ScrollPane contentPane;

    private FXMLLoader parentTrucksLoader;
    private FXMLLoader parentDriversLoader;
    private FXMLLoader parentOrdersLoader;


    @FXML
    private void onClickDriver(MouseEvent event) throws IOException {
        clearClasses();
        drivers.getStyleClass().add("pressed");
        contentPane.setContent(getDriversNode());

    }

    @FXML
    private void onClickTruck(MouseEvent event) throws IOException {
        clearClasses();
        trucks.getStyleClass().add("pressed");
        contentPane.setContent(getTrucksNode());

    }

    @FXML
    private void onClickOrder(MouseEvent event) {
        clearClasses();
        orders.getStyleClass().add("pressed");
        contentPane.setContent(getOrdersNode());
    }

    @FXML
    private void closeAction(ActionEvent event) {
        close((Node) event.getSource());
        System.exit(0);
    }

    @FXML
    private void minimizeAction(ActionEvent event) {
        minimize((Node) event.getSource());
    }

    private void clearClasses() {
        drivers.getStyleClass().remove("pressed");
        trucks.getStyleClass().remove("pressed");
        orders.getStyleClass().remove("pressed");
    }

    public Node getTrucksNode() {
        try {
            parentTrucksLoader = new FXMLLoader(UIClass.class.getResource("fxmls/manager/trucks/managerform_trucks.fxml"));
            return (Node) parentTrucksLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Node getDriversNode() {
        try {
            parentDriversLoader = new FXMLLoader(UIClass.class.getResource("fxmls/manager/drivers/managerform_drivers.fxml"));
            return (Node) parentDriversLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    public Node getOrdersNode() {
        try {
            parentOrdersLoader = new FXMLLoader(UIClass.class.getResource("fxmls/manager/orders/managerform_orders.fxml"));
            return (Node) parentOrdersLoader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }



    @Override
    public ManagerModel getModel() {
        return ManagerModel.getInstance();
    }

    @Override
    public void init() {
        contentPane.setContent(getTrucksNode());
    }

    @Override
    public void preShow() {

    }

    @Override
    public List<AbstractController> getChildControllers() {
        List<AbstractController> controllers = new ArrayList<>();
        if(parentDriversLoader != null)
            controllers.add(parentDriversLoader.<AbstractController>getController());
        if(parentTrucksLoader != null)
            controllers.add(parentTrucksLoader.<AbstractController>getController());
        if(parentOrdersLoader != null)
            controllers.add(parentOrdersLoader.<AbstractController>getController());
        return controllers;
    }
}
