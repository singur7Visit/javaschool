package ru.tsystems.client.logiweb.controllers.login;


import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.AnchorPane;
import javafx.util.Duration;
import ru.tsystems.client.logiweb.controllers.AbstractController;
import ru.tsystems.client.logiweb.models.Model;
import ru.tsystems.client.logiweb.models.impl.DriverModel;
import ru.tsystems.client.logiweb.models.impl.UserModel;
import ru.tsystems.client.logiweb.views.UIClass;
import ru.tsystems.client.logiweb.views.windows.driver.DriverformWindow;
import ru.tsystems.client.logiweb.views.windows.manager.ManagerformWindow;
import ru.tsystems.logiweb.common.entities.impl.DriverCommon;
import ru.tsystems.logiweb.common.entities.impl.UserCommon;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.concurrent.Callable;

/**
 * FXML Controller class
 *
 * @author dsing
 */
public class LoginformController extends AbstractController {
    @FXML
    private AnchorPane pane;
    @FXML
    private Button close_button;
    @FXML
    private Button minimize_button;
    @FXML
    private TextField loginField;
    @FXML
    private TextField passwordField;
    @FXML
    private Label messageLabel;

    @FXML
    private void loginAction(ActionEvent event) throws Exception {
        final String login = loginField.getText();
        final String password = passwordField.getText();
        Map<String,Serializable> response = null;
        if(isValid(login,password))
            response = (Map<String,Serializable>) newTaskTimeout(new Callable<Object>() {
                @Override
                public Object call() throws Exception {
                    return getModel().login(login, password);
                }
            },5);
        if(isValid(login,password) && response == null) {
            messageLabel.setText("No connection from server!");
            resetLabel();
        }
        else if (!isValid(login,password) || response.get("User") == null) {
            messageLabel.setText("Login or password are not valid!");
            resetLabel();
        }
        else {
            UIClass window;
            final UserCommon userCommon = (UserCommon)response.get("User");
            if(userCommon.getRole().isManageDrivers()) {
                window = new ManagerformWindow();
                window.setTitle("Manager Logiweb :" + userCommon.getLogin());
            }
            else {
                window = new DriverformWindow();
                DriverCommon driver = newTaskTimeout(new Callable<DriverCommon>() {
                    @Override
                    public DriverCommon call() throws Exception {
                        return DriverModel.getInstance().getDriver(userCommon.getLogin());
                    }
                }, 3);
                window.getController().setObject(driver);
                window.setTitle("Driver Logiweb :" + userCommon.getLogin());
            }
            window.show();
            close((Node) event.getSource());
        }
    }



    @FXML
    private void closeAction(ActionEvent event) {
        close((Node) event.getSource());
        Platform.exit();
    }

    @FXML
    private void minimizeAction(ActionEvent event) {
        minimize((Node) event.getSource());
    }

    @Override
    public UserModel getModel() {
        return UserModel.getInstance();
    }

    @Override
    public void init() {

    }

    @Override
    public void preShow() {

    }

    private void resetLabel() {
        Timeline timeline = new Timeline(
                new KeyFrame(
                        Duration.millis(2500), new EventHandler<ActionEvent>() {
                    @Override
                    public void handle(ActionEvent event) {
                        messageLabel.setText("");
                    }
                }));
        timeline.play();
    }

    private boolean isValid(String login, String password) {
        return login!=null && password!=null && !login.isEmpty() && !password.isEmpty();
    }

    @Override
    public List<AbstractController> getChildControllers() {
        return null;
    }
}
