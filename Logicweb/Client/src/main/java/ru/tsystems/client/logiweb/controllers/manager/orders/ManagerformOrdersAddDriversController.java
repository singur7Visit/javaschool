/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.tsystems.client.logiweb.controllers.manager.orders;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ListView;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import ru.tsystems.client.logiweb.controllers.AbstractController;
import ru.tsystems.client.logiweb.models.Model;
import ru.tsystems.client.logiweb.models.impl.DriverModel;
import ru.tsystems.client.logiweb.models.impl.OrderModel;
import ru.tsystems.client.logiweb.views.AlertDialog;
import ru.tsystems.client.logiweb.views.UIClass;
import ru.tsystems.logiweb.common.entities.impl.DriverCommon;
import ru.tsystems.logiweb.common.entities.impl.OrderCommon;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * FXML Controller class
 *
 * @author dsing
 */
public class ManagerformOrdersAddDriversController extends AbstractController {
    @FXML
    private AnchorPane pane;
    @FXML
    private Button finishButton;
    @FXML
    private Button cancelButton;
    @FXML
    private Button backButton;
    @FXML
    private Button closeButton;
    @FXML
    private ListView<DriverCommon> listDrivers;
    @FXML
    private TableView<DriverCommon> tableDrivers;
    @FXML
    private TableColumn snmColumn;
    @FXML
    private TableColumn licenceColumn;
    @FXML
    private Button addButton;
    @FXML
    private Button removeButton;

    @FXML
    private void finishAction(ActionEvent event) throws IOException {
        callback.call(object);
        close((Node) event.getSource());
    }

    @FXML
    private void closeAction(ActionEvent event) {
        Boolean status = OrderModel.getInstance().rollbackOrder((OrderCommon) object);
        if(status == null || !status)
            AlertDialog.error((Stage) ((Node) event.getSource()).getScene().getWindow(), "Cann't rollback order");
        close((Node) event.getSource());
    }

    @FXML
    private void backAction(ActionEvent event) throws IOException {
        backCallback.call(object);
    }

    @FXML
    private void addAction(ActionEvent event) {
        DriverCommon selected = tableDrivers.getSelectionModel().getSelectedItem();
        if(selected == null) {
            addButton.setDisable(true);
            return;
        }
        listDrivers.getItems().add(selected);
        ((OrderCommon) object).getDrivers().add(selected);
        tableDrivers.getItems().remove(selected);
        if(tableDrivers.getItems().isEmpty() || ((OrderCommon) object).getTruck().getCountDrivers() == listDrivers.getItems().size())
            addButton.setDisable(true);
        removeButton.setDisable(false);
    }

    @FXML
    private void removeAction(ActionEvent event) {
        DriverCommon selected = listDrivers.getSelectionModel().getSelectedItem();
        if(selected == null) {
            removeButton.setDisable(true);
            return;
        }
        listDrivers.getItems().remove(selected);
        ((OrderCommon) object).getDrivers().remove(selected);
        tableDrivers.getItems().add(selected);
        if(listDrivers.getItems().isEmpty())
            removeButton.setDisable(true);
    }

    @Override
    public List<AbstractController> getChildControllers() {
        return null;
    }

    @Override
    public DriverModel getModel() {
        return DriverModel.getInstance();
    }

    @Override
    public void init() {
        snmColumn.setCellValueFactory(new PropertyValueFactory<DriverCommon,String>("SNM"));
        licenceColumn.setCellValueFactory(new PropertyValueFactory<DriverCommon,String>("licence"));
        List<DriverCommon> availableDrivers = newTaskTimeout(new Callable<List<DriverCommon>>() {
            @Override
            public List<DriverCommon> call() throws Exception {
                return getModel().getAvailableDrivers();
            }
        },3);
        if(availableDrivers != null) {
            tableDrivers.getItems().addAll(availableDrivers);
        }
    }

    @Override
    public void preShow() {
        if(((OrderCommon)object).getDrivers() != null)
            listDrivers.getItems().addAll(((OrderCommon)object).getDrivers());
        else
            ((OrderCommon)object).setDrivers(new ArrayList<DriverCommon>());
        removeButton.setDisable(true);
    }
}
