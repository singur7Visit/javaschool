package ru.tsystems.client.logiweb.views.windows.manager.orders;

import javafx.stage.Stage;
import ru.tsystems.client.logiweb.views.UIClass;

/**
 * Created by dsing on 14.08.14.
 */
public class ManagerformOrdersAddStuffsWindow extends UIClass {
    public ManagerformOrdersAddStuffsWindow() throws Exception {
        super("fxmls/manager/orders/create/managerfrom_orders_add_create.fxml",422,421);
    }

    @Override
    protected String getLocalNameCSS() {
        return "manager_add_orders.css";
    }
}
