package ru.tsystems.client.logiweb.views.windows.manager.trucks;

import ru.tsystems.client.logiweb.views.UIClass;

/**
 * Created by dsing on 11.08.14.
 */
public class ManagerformTrucksAddWindow extends UIClass {
    public ManagerformTrucksAddWindow() throws Exception {
        super("fxmls/manager/trucks/managerform_trucks_add.fxml",306,400);
    }

    @Override
    protected String getLocalNameCSS() {
        return "manager_add_trucks.css";
    }
}
