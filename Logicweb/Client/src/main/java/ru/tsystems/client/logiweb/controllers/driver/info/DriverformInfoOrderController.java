/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.tsystems.client.logiweb.controllers.driver.info;

import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import ru.tsystems.client.logiweb.controllers.AbstractController;
import ru.tsystems.client.logiweb.models.impl.DriverModel;
import ru.tsystems.logiweb.common.entities.impl.DriverCommon;
import ru.tsystems.logiweb.common.entities.impl.StuffCommon;

import java.math.BigDecimal;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

/**
 * FXML Controller class
 *
 * @author dsing
 */
public class DriverformInfoOrderController extends AbstractController {
    @FXML
    private AnchorPane pane;
    @FXML
    private Label numberLabel;
    @FXML
    private TableView<StuffCommon> stuffsTable;
    @FXML
    private TableColumn nameColumn;
    @FXML
    private TableColumn gpsColumn;
    @FXML
    private TableColumn weightColumn;
    @FXML
    private TableColumn statusColumn;

    @Override
    public DriverModel getModel() {
        return null;
    }

    @Override
    public void init() {
        nameColumn.setCellValueFactory(new PropertyValueFactory<StuffCommon,String>("name"));
        gpsColumn.setCellValueFactory(new PropertyValueFactory<StuffCommon,String>("gps"));
        weightColumn.setCellValueFactory(new PropertyValueFactory<StuffCommon,BigDecimal>("weight"));
        statusColumn.setCellValueFactory(new PropertyValueFactory<StuffCommon,Boolean>("status"));
    }

    @Override
    public void preShow() {
        if( ((DriverCommon)object).getOrder() == null
                || ((DriverCommon)object).getOrder().getNumOrder() == null)
            numberLabel.setText("No order!");
        else
            numberLabel.setText(String.valueOf(((DriverCommon)object).getOrder().getNumOrder()));
        if(((DriverCommon)object).getOrder() != null && ((DriverCommon)object).getOrder().getNumOrder() != null)
            numberLabel.setText(String.valueOf(((DriverCommon)object).getOrder().getNumOrder()));
        if(((DriverCommon)object).getOrder() != null && ((DriverCommon)object).getOrder().getStuffs() != null)
            stuffsTable.getItems().addAll(((DriverCommon)object).getOrder().getStuffs());
    }

    @Override
    public List<AbstractController> getChildControllers() {
        return null;
    }
}
