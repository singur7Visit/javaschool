/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.tsystems.client.logiweb.controllers.driver.management;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import ru.tsystems.client.logiweb.controllers.AbstractController;
import ru.tsystems.client.logiweb.models.impl.DriverModel;
import ru.tsystems.client.logiweb.models.impl.OrderModel;
import ru.tsystems.client.logiweb.views.AlertDialog;
import ru.tsystems.logiweb.common.entities.impl.DriverCommon;
import ru.tsystems.logiweb.common.entities.impl.OrderCommon;
import ru.tsystems.logiweb.common.entities.impl.StuffCommon;

import java.util.List;
import java.util.concurrent.Callable;

/**
 * FXML Controller class
 *
 * @author dsing
 */
public class DriverformManagementOrderController extends AbstractController {
    @FXML
    private AnchorPane childPane;
    @FXML
    private TableView<StuffCommon> tableStuffs;
    @FXML
    private Button completeButton;
    @FXML
    private Button closeOrder;
    @FXML
    private TableColumn nameColumn;
    @FXML
    private TableColumn statusColumn;


    @FXML
    private void completeAction(ActionEvent event) {
        final StuffCommon stuff = tableStuffs.getSelectionModel().getSelectedItem();
        if(stuff == null )  {
            return;
        }
        final OrderCommon order = newTaskTimeout(new Callable<OrderCommon>() {
            @Override
            public OrderCommon call() throws Exception {
                return getModel().deliveredStuff(stuff);
            }
        },3);
        if(order == null) {
            AlertDialog.error((Stage) ((Node)event.getSource()).getScene().getWindow(),"Cann't delivered stuff");
            return;
        }
        tableStuffs.getItems().clear();
        for(StuffCommon stuffCommon : order.getStuffs()) {
            tableStuffs.getItems().add(stuffCommon);
        }
        for (StuffCommon stuffCommon : order.getStuffs()) {
            if(!stuffCommon.isStatus())
                return;
        }
        OrderCommon completeOrder = newTaskTimeout(new Callable<OrderCommon>() {
            @Override
            public OrderCommon call() throws Exception {
                return OrderModel.getInstance().completeOrder(order);
            }
        },3);
        if(completeOrder == null) {
            AlertDialog.error((Stage) ((Node)event.getSource()).getScene().getWindow(),"Cann't delivered order");
        }
        else
            AlertDialog.info((Stage) ((Node)event.getSource()).getScene().getWindow(),"Order delivered");
    }

    @FXML
    private void closeOrderAction(ActionEvent event) {
        boolean isClosing = true;
        for(StuffCommon stuffCommon : tableStuffs.getItems()) {
            isClosing&=stuffCommon.isStatus();
        }
        if(isClosing)
            return;
        for(StuffCommon stuffCommon : tableStuffs.getItems()) {
            stuffCommon.setStatus(true);
            stuffCommon.setOrder(((DriverCommon)object).getOrder());
        }
        OrderCommon completeOrder = newTaskTimeout(new Callable<OrderCommon>() {
            @Override
            public OrderCommon call() throws Exception {
                return OrderModel.getInstance().completeOrder(((DriverCommon)object).getOrder());
            }
        },3);
        if(completeOrder == null) {
            AlertDialog.error((Stage) ((Node)event.getSource()).getScene().getWindow(),"Cann't delivered order");
        }
        else
            AlertDialog.info((Stage) ((Node)event.getSource()).getScene().getWindow(),"Order delivered");


    }

    @Override
    public DriverModel getModel() {
        return DriverModel.getInstance();
    }

    @Override
    public void init() {
        nameColumn.setCellValueFactory(new PropertyValueFactory<StuffCommon,String>("name"));
        statusColumn.setCellValueFactory(new PropertyValueFactory<StuffCommon,Boolean>("status"));
        tableStuffs.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<StuffCommon>() {
            @Override
            public void changed(ObservableValue<? extends StuffCommon> observableValue, StuffCommon stuffCommon, StuffCommon stuffCommon2) {
                if(tableStuffs.getSelectionModel().getSelectedItem() == null)
                    completeButton.setDisable(true);
                else
                    completeButton.setDisable(false);
            }
        });
        completeButton.setDisable(true);
    }

    @Override
    public void preShow() {
        if(object != null && ((DriverCommon)object).getOrder() != null && ((DriverCommon)object).getOrder().getStuffs()!= null)
            tableStuffs.getItems().addAll(((DriverCommon)object).getOrder().getStuffs());
    }

    @Override
    public List<AbstractController> getChildControllers() {
        return null;
    }
}
