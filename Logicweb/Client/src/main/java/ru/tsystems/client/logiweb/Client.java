package ru.tsystems.client.logiweb;

import javafx.application.Application;
import javafx.stage.Stage;
import ru.tsystems.client.logiweb.views.windows.login.LoginformWindow;

/**
 * Created by dsing on 09.08.14.
 */
public class Client extends Application {

    public static void main(String[] args) {
        launch(args);
    }
    @Override
    public void start(Stage stage) throws Exception {
        new LoginformWindow(stage).show();
    }
}
