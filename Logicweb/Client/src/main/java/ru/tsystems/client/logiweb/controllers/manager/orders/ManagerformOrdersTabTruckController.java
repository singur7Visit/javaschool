/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.tsystems.client.logiweb.controllers.manager.orders;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.layout.AnchorPane;
import ru.tsystems.client.logiweb.controllers.AbstractController;
import ru.tsystems.client.logiweb.models.Model;
import ru.tsystems.logiweb.common.entities.impl.OrderCommon;

/**
 * FXML Controller class
 *
 * @author dsing
 */
public class ManagerformOrdersTabTruckController extends AbstractController {
    @FXML
    private AnchorPane paneTruckTabs;
    @FXML
    private Label numberLabel;
    @FXML
    private Label countDriversLabel;
    @FXML
    private Label classLabel;
    @FXML
    private Label statusLabel;


    @Override
    public List<AbstractController> getChildControllers() {
        return null;
    }

    @Override
    public Model getModel() {
        return null;
    }

    @Override
    public void init() {

    }

    @Override
    public void preShow() {
        if (object != null && ((OrderCommon) object).getTruck() != null) {
            numberLabel.setText(String.valueOf(((OrderCommon) object).getTruck().getRegNumber()));
            countDriversLabel.setText(String.valueOf(((OrderCommon) object).getTruck().getCountDrivers()));
            classLabel.setText(String.valueOf(((OrderCommon) object).getTruck().getClassCapacity()));
            statusLabel.setText(String.valueOf(((OrderCommon) object).getTruck().getStatus()));
        }
    }
}
