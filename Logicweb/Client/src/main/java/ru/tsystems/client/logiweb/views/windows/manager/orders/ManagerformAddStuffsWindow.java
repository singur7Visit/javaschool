package ru.tsystems.client.logiweb.views.windows.manager.orders;

import ru.tsystems.client.logiweb.views.UIClass;

/**
 * Created by dsing on 22.08.14.
 */
public class ManagerformAddStuffsWindow extends UIClass {
    public ManagerformAddStuffsWindow() throws Exception {
        super("fxmls/manager/orders/create/managerfrom_orders_add_create_stuff.fxml",292,226);
    }

    @Override
    protected String getLocalNameCSS() {
        return "manager_add_orders.css";
    }
}
