package ru.tsystems.client.logiweb.models;

import ru.tsystems.client.logiweb.models.connection.IConnection;
import ru.tsystems.client.logiweb.models.connection.SocketConnection;
import ru.tsystems.logiweb.common.commands.ICommand;
import ru.tsystems.logiweb.common.entities.IObject;

import java.io.*;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by dsing on 13.08.14.
 */
public abstract class Model{

    private final static String HOST = "localhost";
    private final static int PORT = 3333;
    private  IConnection connection;


    protected ICommand readResponse() throws IOException, ClassNotFoundException {
        ObjectInputStream input = createConnection().getObjectInputStream();
        closeConnection();
        return input == null ? null : (ICommand) input.readObject();

    }

    protected void sendCommand(ICommand command) throws IOException {
        closeConnection();
        IConnection connection = createConnection();
        ObjectOutputStream output = connection.getObjectOutputStream();
        if(output != null) {
            output.writeObject(command);
            output.flush();
        }
        else {
            closeConnection();
        }
    }

    private IConnection createConnection() {
        return connection == null ? (connection =new SocketConnection(HOST,PORT) ) : connection;
    }

    public void closeConnection() {
        if(connection != null)
            connection.close();
        connection = null;
    }


}
