/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.tsystems.client.logiweb.controllers.manager.trucks;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.input.KeyEvent;
import javafx.stage.Stage;
import ru.tsystems.client.logiweb.controllers.AbstractController;
import ru.tsystems.client.logiweb.models.Model;
import ru.tsystems.client.logiweb.models.impl.TruckModel;
import ru.tsystems.client.logiweb.views.AlertDialog;
import ru.tsystems.logiweb.common.entities.impl.ClassCapacityCommon;
import ru.tsystems.logiweb.common.entities.impl.DriverCommon;
import ru.tsystems.logiweb.common.entities.impl.TruckCommon;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import java.util.concurrent.Callable;
import java.util.regex.Pattern;

/**
 * FXML Controller class
 *
 * @author dsing
 */
public class ManagerformTrucksAddController extends AbstractController {
    @FXML
    private Button close_button;
    @FXML
    private Button warning_button_num;
    @FXML
    private Button warning_button_class;
    @FXML
    private Button cancel_button;
    @FXML
    private Button add_button;
    @FXML
    private TextField field_number;
    @FXML
    private TextField field_count;
    @FXML
    private ChoiceBox<ClassCapacityCommon> classBox;

    private static final String validMessageNumber = "Number is not valid (2 letter + 5 number)";
    private static final String validMessageClass = "Count is not valid (Only numbers)";
    private static final Tooltip tooltipNumber = new Tooltip();
    private static final Tooltip tooltipClass = new Tooltip();

    @Override
    public TruckModel getModel() {
        return TruckModel.getInstance();
    }

    public void init() {
        warning_button_num.getStyleClass().add("error");
        warning_button_class.getStyleClass().add("error");
        add_button.setDisable(true);
        tooltipNumber.setText(validMessageNumber);
        tooltipClass.setText(validMessageClass);
        warning_button_num.setTooltip(tooltipNumber);
        warning_button_class.setTooltip(tooltipClass);
        List<ClassCapacityCommon> classes = (List<ClassCapacityCommon>) newTaskTimeout(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                return getModel().getAllClassCapacity();
            }
        },3);
        if(classes != null) {
            classBox.getItems().addAll(classes);
            classBox.getSelectionModel().select(0);
        }
    }

    @Override
    public void preShow() {

    }

    @FXML
    private void closeAction(ActionEvent actionEvent) {
        close((Node) actionEvent.getSource());
    }


    final Pattern xNumber = Pattern.compile("[A-Za-z]{2}\\d{5}");
    private boolean isValidNumber() {
        return field_number.getText().length() == 7 && xNumber.matcher(field_number.getText()).matches();
    }

    final Pattern xCount = Pattern.compile("\\d{1,99999}");
    private boolean isValidClass() {
        return xCount.matcher(field_count.getText()).matches();
    }

    private boolean isValidFields() {
        return isValidNumber() && isValidClass();
    }

    @FXML
    private void addAction(ActionEvent actionEvent) throws IOException {
        Boolean isUniqueTruck = (Boolean) newTaskTimeout(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                return getModel().isUniqueTruck(field_number.getText());
            }
        }, 3);
        if (isUniqueTruck == null || !isUniqueTruck) {
            AlertDialog.error((Stage) ((Node) actionEvent.getSource()).getScene().getWindow(), "Number is not unique");
            warning_button_num.getStyleClass().add("error");
            tooltipNumber.setText("Licence is not unique");
            warning_button_num.setTooltip(tooltipNumber);
            add_button.setDisable(false);
            return;
        }
        TruckCommon truck = new TruckCommon();
        truck.setRegNumber(field_number.getText());
        truck.setClassCapacity(classBox.getSelectionModel().getSelectedItem());
        truck.setCountDrivers(Integer.parseInt(field_count.getText()));
        callback.call(truck);
        close((Node) actionEvent.getSource());
    }

    @FXML
    private void number_key_pressed(KeyEvent event) {
        warning_button_num.getStyleClass().removeAll("error");
        warning_button_num.setTooltip(null);
        if(!isValidNumber()) {
            warning_button_num.getStyleClass().add("error");
            tooltipNumber.setText(validMessageNumber);
            warning_button_num.setTooltip(tooltipNumber);
        }
        add_button.setDisable(!isValidFields());
    }

    @FXML
    private void count_key_pressed(KeyEvent event) {
        warning_button_class.getStyleClass().removeAll("error");
        warning_button_class.setTooltip(null);
        if(!isValidClass()) {
            warning_button_class.getStyleClass().add("error");
            tooltipClass.setText(validMessageClass);
            warning_button_class.setTooltip(tooltipClass);
        }
        add_button.setDisable(!isValidFields());
    }

    @Override
    public List<AbstractController> getChildControllers() {
        return null;
    }
}
