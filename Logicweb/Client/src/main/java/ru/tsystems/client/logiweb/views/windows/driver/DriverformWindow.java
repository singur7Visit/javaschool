package ru.tsystems.client.logiweb.views.windows.driver;

import ru.tsystems.client.logiweb.views.UIClass;

/**
 * Created by dsing on 14.08.14.
 */
public class DriverformWindow extends UIClass {
    public DriverformWindow() throws Exception {
        super("fxmls/driver/driverform.fxml", 600,400);
    }

    @Override
    protected String getLocalNameCSS() {
        return "driver.css";
    }
}
