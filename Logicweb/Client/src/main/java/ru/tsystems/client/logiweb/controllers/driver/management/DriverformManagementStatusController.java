package ru.tsystems.client.logiweb.controllers.driver.management;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.scene.control.ToggleButton;
import javafx.scene.layout.AnchorPane;
import ru.tsystems.client.logiweb.controllers.AbstractController;
import ru.tsystems.client.logiweb.models.impl.DriverModel;
import ru.tsystems.logiweb.common.entities.impl.DriverCommon;

import java.util.List;

/**
 * FXML Controller class
 *
 * @author dsing
 */
public class DriverformManagementStatusController extends AbstractController {
    @FXML
    private AnchorPane childPane;
    @FXML
    private ToggleButton shiftButton;
    @FXML
    private ToggleButton drivingButton;
    @FXML
    private Label messageLabel;


    @FXML
    private void shiftAction(ActionEvent event) {
        DriverCommon driver = (DriverCommon) object;
        drivingButton.setSelected(false);
        shiftButton.setSelected(true);
    }

    @FXML
    private void drivingAction(ActionEvent event) {
        shiftButton.setSelected(false);
        drivingButton.setSelected(true);
    }

    @Override
    public DriverModel getModel() {
        return DriverModel.getInstance();
    }

    @Override
    public void init() {

    }

    @Override
    public void preShow() {
        if(object == null || ((DriverCommon) object).getOrder() == null) {
            shiftButton.setDisable(true);
            drivingButton.setDisable(true);
            messageLabel.setText("No order!");
        }
        else {
            if(((DriverCommon) object).getOrder().getStatus() != null) {
                shiftButton.setSelected(((DriverCommon) object).getStatus().getName().equals("busy"));
                drivingButton.setSelected(((DriverCommon) object).getStatus().getName().equals("driving"));
            }
        }
    }

    @Override
    public List<AbstractController> getChildControllers() {
        return null;
    }
}
