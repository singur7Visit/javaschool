
package ru.tsystems.client.logiweb.controllers.manager.orders;

import java.io.IOException;
import java.net.URL;
import java.util.*;
import java.util.concurrent.Callable;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Modality;
import javafx.stage.Stage;
import ru.tsystems.client.logiweb.controllers.AbstractController;
import ru.tsystems.client.logiweb.controllers.Callback;
import ru.tsystems.client.logiweb.controllers.IController;
import ru.tsystems.client.logiweb.models.Model;
import ru.tsystems.client.logiweb.models.impl.OrderModel;
import ru.tsystems.client.logiweb.views.AlertDialog;
import ru.tsystems.client.logiweb.views.UIClass;
import ru.tsystems.client.logiweb.views.windows.manager.orders.ManagerformAddStuffsWindow;
import ru.tsystems.client.logiweb.views.windows.manager.orders.ManagerformOrdersAddStuffsWindow;
import ru.tsystems.logiweb.common.entities.IObject;
import ru.tsystems.logiweb.common.entities.impl.OrderCommon;
import ru.tsystems.logiweb.common.entities.impl.StuffCommon;
import ru.tsystems.logiweb.common.entities.impl.UserCommon;

/**
 * FXML Controller class
 *
 * @author dsing
 */
public class ManagerformOrdersAddStuffsController extends AbstractController {
    @FXML
    private AnchorPane addPane;
    @FXML
    private ListView<StuffCommon> listStuffs;
    @FXML
    private Button nextButton;
    @FXML
    private Button cancelButton;
    @FXML
    private Button closeButton;
    @FXML
    private Button createStuffButton;
    @FXML
    private Button removeStuffButton;
    @FXML
    private Button cloneStuffButton;
    @FXML
    private Label numberOrder;

    private ObservableList<Node> children;


    @FXML
    private void next(ActionEvent event) throws IOException {
            IObject o = newTaskTimeout(new Callable<OrderCommon>() {
                @Override
                public OrderCommon call() throws Exception {
                    return getModel().confirmOrder((OrderCommon) object);
                }
            },3);
            if(o == null) {
                AlertDialog.error((Stage) ((Node)event.getSource()).getScene().getWindow(),"Cann't confirm order");
                return;
            }
        else
            object = o;
        children = addPane.getChildren();
        children.clear();
        FXMLLoader loader = new FXMLLoader(UIClass.class.getResource("fxmls/manager/orders/create/managerfrom_orders_add_truck.fxml"));
        addPane.getChildren().addAll((Node)loader.load());
        ((IController) loader.getController()).setCallback(callback);
        ((IController) loader.getController()).setObject(object);
        ((IController) loader.getController()).setBackCallback(new Callback<OrderCommon>() {
            @Override
            public void call(OrderCommon o) throws IOException {
                object = o;
                FXMLLoader loader = new FXMLLoader(UIClass.class.getResource("fxmls/manager/orders/create/managerfrom_orders_add_create.fxml"));
                Node n = (Node) loader.load();
                ((IController)loader.getController()).setObject(o);
                ((IController)loader.getController()).setCallback(callback);
                ((IController)loader.getController()).preShow();
                children.clear();
                children.addAll(n);
            }
        });

    }

    @FXML
    private void closeAction(ActionEvent event) {
        Boolean status = getModel().rollbackOrder((OrderCommon) object);
        if(status == null || !status)
            AlertDialog.error((Stage) ((Node) event.getSource()).getScene().getWindow(),"Cann't rollback order");
        close((Node) event.getSource());
    }

    @FXML
    private void createStuff(ActionEvent event) throws Exception {
        UIClass window = new ManagerformAddStuffsWindow();
        window.getController().setObject(object);
        window.getController().setCallback(new Callback<StuffCommon>() {
            @Override
            public void call(StuffCommon o) {
                if(((OrderCommon)object).getStuffs() == null)
                    ((OrderCommon)object).setStuffs(new ArrayList<StuffCommon>());
                o.setOrder((OrderCommon)object);
                ((OrderCommon)object).getStuffs().add(o);
                listStuffs.getItems().add(o);
            }
        });
        window.setModality((Node) event.getSource(), Modality.WINDOW_MODAL);
        window.show();
    }

    @FXML
    private void removeStuff(ActionEvent event) {
        ((OrderCommon)object).getStuffs().add(listStuffs.getSelectionModel().getSelectedItem());
        listStuffs.getItems().remove(listStuffs.getSelectionModel().getSelectedItem());
    }

    @FXML
    private void clone(ActionEvent event) {
        ((OrderCommon)object).getStuffs().add(listStuffs.getSelectionModel().getSelectedItem());
        listStuffs.getItems().add(listStuffs.getSelectionModel().getSelectedItem());
    }

    @FXML
    private void onClickedListStuffs(MouseEvent event) {
        if(listStuffs.getSelectionModel().getSelectedItem() != null) {
            cloneStuffButton.setDisable(false);
            removeStuffButton.setDisable(false);
        }
    }

    @Override
    public OrderModel getModel() {
        return OrderModel.getInstance();
    }

    @Override
    public void init() {
        cloneStuffButton.setDisable(true);
        removeStuffButton.setDisable(true);
    }

    @Override
    public void preShow() {
        numberOrder.setText(((OrderCommon)object).getNumOrder().toString());
        if(((OrderCommon)object).getStuffs() != null)
            listStuffs.getItems().addAll(((OrderCommon)object).getStuffs());
    }

    @Override
    public List<AbstractController> getChildControllers() {
        return null;
    }
}
