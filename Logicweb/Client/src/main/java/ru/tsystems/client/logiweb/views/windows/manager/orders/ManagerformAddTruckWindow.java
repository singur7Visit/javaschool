package ru.tsystems.client.logiweb.views.windows.manager.orders;

import javafx.stage.Stage;
import ru.tsystems.client.logiweb.views.UIClass;

/**
 * Created by dsing on 14.08.14.
 */
public class ManagerformAddTruckWindow extends UIClass {
    public ManagerformAddTruckWindow(String pathToView, String title, double width, double height) throws Exception {
        super(pathToView, width, height);
    }

    public ManagerformAddTruckWindow(Stage stage, String pathToView, String title, double width, double height) throws Exception {
        super(stage, pathToView, width, height);
    }

    @Override
    protected String getLocalNameCSS() {
        return "mananagerform_add_truck.css";
    }
}
