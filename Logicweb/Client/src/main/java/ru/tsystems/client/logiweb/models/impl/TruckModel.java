package ru.tsystems.client.logiweb.models.impl;

import org.apache.log4j.Logger;
import ru.tsystems.client.logiweb.models.Model;
import ru.tsystems.logiweb.common.commands.Command;
import ru.tsystems.logiweb.common.commands.ICommand;
import ru.tsystems.logiweb.common.entities.impl.ClassCapacityCommon;
import ru.tsystems.logiweb.common.entities.impl.TruckCommon;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Created by dsing on 14.08.14.
 */
public class TruckModel extends Model {
    private static TruckModel truckModel;
    private final static Logger logger = Logger.getLogger(TruckModel.class);

    public static TruckModel getInstance() {
        if(truckModel == null)
            truckModel = new TruckModel();
        return truckModel;
    }

    protected TruckModel() {

    }

    public List<TruckCommon> getAllTrucks(int from, int to,String searchWord) {
        ICommand command = new Command();
        if(searchWord != null) {
            command.setCommand("GetAllTrucksSearch");
            command.addParam("SearchWord",searchWord);
        }
        else
            command.setCommand("GetAllTrucks");
        command.addParam("from",from);
        command.addParam("to",to);
        try {
            sendCommand(command);
            ICommand response = readResponse();
            return (List<TruckCommon>) response.getParam("Trucks");
        } catch (IOException | ClassNotFoundException | ClassCastException | NullPointerException e) {
            logger.error("Cann't get Trucks from server. Message : " + e.getMessage());
            return null;
        }
        finally {
            closeConnection();
        }
    }

    public Map<String,Serializable> createTruck(TruckCommon truck) {
        ICommand command = new Command();
        command.addParam("Truck",truck);
        command.setCommand("CreateTruck");
        try {
            sendCommand(command);
            ICommand response = readResponse();
            if(response != null && !(Boolean)response.getParam("Status"))
                logger.warn("Truck cann't be create. Message : " + response.getParam("Message"));
            return response.getNamedParams("Status","Truck");
        } catch (IOException | ClassNotFoundException | NullPointerException e) {
            logger.error("Cann't be create truck. Message : " + e.getMessage());
            return null;
        } finally {
            closeConnection();
        }
    }

    public Boolean removeTruck(TruckCommon truck) {
        ICommand command = new Command();
        command.addParam("Truck",truck);
        command.setCommand("RemoveTruck");
        try {
            sendCommand(command);
            ICommand response = readResponse();
            if(response != null && !(Boolean)response.getParam("Status"))
                logger.warn("Truck cann't be removing. Message : " + response.getParam("Message"));
            return (Boolean) response.getParam("Status");
        } catch (IOException | ClassNotFoundException | NullPointerException e) {
            logger.error("Cann't remove truck. Message : " + e.getMessage());
            return null;
        } finally {
            closeConnection();
        }
    }

    public Boolean isUniqueTruck(String number) {
        ICommand command = new Command();
        command.addParam("number",number);
        command.setCommand("isUniqueTruck");
        try {
            sendCommand(command);
            ICommand response = readResponse();
            return (Boolean) response.getParam("isUnique");
        } catch (IOException | ClassNotFoundException | NullPointerException e) {
            logger.error("Message : " + e.getMessage());
            return null;
        } finally {
            closeConnection();
        }
    }

    public List<ClassCapacityCommon> getAllClassCapacity() {
        ICommand command = new Command();
        command.setCommand("GetAllClassCapacity");
        try {
            sendCommand(command);
            ICommand response = readResponse();
            return (List<ClassCapacityCommon>) response.getParam("classes");
        } catch (IOException | ClassNotFoundException | NullPointerException e) {
            logger.error("Message : " + e.getMessage());
            return null;
        } finally {
            closeConnection();
        }
    }

    public List<TruckCommon> getAvailableTrucks() {
        ICommand command = new Command();
        command.setCommand("GetAvailableTrucks");
        try {
            sendCommand(command);
            ICommand response = readResponse();
            return (List<TruckCommon>) response.getParam("Trucks");
        } catch (IOException | ClassNotFoundException | ClassCastException | NullPointerException e) {
            logger.error("Cann't get Drivers from server. Message : " + e.getMessage());
            return null;
        }
        finally {
            closeConnection();
        }
    }
}
