/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.tsystems.client.logiweb.controllers.manager.orders;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import ru.tsystems.client.logiweb.controllers.AbstractController;
import ru.tsystems.client.logiweb.controllers.Callback;
import ru.tsystems.client.logiweb.controllers.IController;
import ru.tsystems.client.logiweb.models.impl.OrderModel;
import ru.tsystems.client.logiweb.models.impl.TruckModel;
import ru.tsystems.client.logiweb.views.AlertDialog;
import ru.tsystems.client.logiweb.views.UIClass;
import ru.tsystems.logiweb.common.entities.IObject;
import ru.tsystems.logiweb.common.entities.impl.OrderCommon;
import ru.tsystems.logiweb.common.entities.impl.StatusTruckCommon;
import ru.tsystems.logiweb.common.entities.impl.TruckCommon;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.Callable;

/**
 * FXML Controller class
 *
 * @author dsing
 */
public class ManagerformOrdersAddTruckController extends AbstractController {
    @FXML
    private TableView<TruckCommon> tableTrucks;
    @FXML
    private Button nextButton;
    @FXML
    private Button cancelButton;
    @FXML
    private Button backButton;
    @FXML
    private Button closeButton;
    @FXML
    private TableColumn numberColumn;
    @FXML
    private TableColumn countDriverColumn;
    @FXML
    private TableColumn classColumn;
    @FXML
    private AnchorPane pane;

    private ObservableList<Node> children;


    @FXML
    private void onClickRow(MouseEvent event) {
        if(tableTrucks.getSelectionModel().getSelectedItem() != null)
            nextButton.setDisable(false);
        else
            nextButton.setDisable(true);
    }

    @FXML
    private void nextAction(ActionEvent event) throws IOException {
        ((OrderCommon)object).setTruck(tableTrucks.getSelectionModel().getSelectedItem());
        children = pane.getChildren();
        children.clear();
        FXMLLoader loader = new FXMLLoader(UIClass.class.getResource("fxmls/manager/orders/create/managerfrom_orders_add_drivers.fxml"));
        children.addAll((Node) loader.load());
        ((IController) loader.getController()).setCallback(callback);
        ((IController) loader.getController()).setObject(object);
        ((IController) loader.getController()).setBackCallback(new Callback<OrderCommon>() {
            @Override
            public void call(OrderCommon o) throws IOException {
                object = o;
                FXMLLoader loader = new FXMLLoader(UIClass.class.getResource("fxmls/manager/orders/create/managerfrom_orders_add_truck.fxml"));
                Node n = (Node) loader.load();
                ((IController)loader.getController()).setObject(o);
                ((IController)loader.getController()).setCallback(callback);
                ((IController)loader.getController()).setBackCallback(backCallback);
                ((IController)loader.getController()).preShow();
                children.clear();
                children.addAll(n);
            }
        });
        ((IController) loader.getController()).preShow();
    }


    @FXML
    private void backAction(ActionEvent event) throws IOException {
        backCallback.call(object);
    }

    @FXML
    private void closeAction(ActionEvent event) {
        Boolean status = OrderModel.getInstance().rollbackOrder((OrderCommon) object);
        if(status == null || !status)
            AlertDialog.error((Stage) ((Node) event.getSource()).getScene().getWindow(), "Cann't rollback order");
        close((Node) event.getSource());
    }

    @Override
    public void setObject(IObject object) {
        super.setObject(object);
        if(object != null && object instanceof OrderCommon && ((OrderCommon) object).getTruck() != null)
            tableTrucks.getSelectionModel().select(((OrderCommon) object).getTruck());
    }

    @Override
    public List<AbstractController> getChildControllers() {
        return null;
    }

    @Override
    public TruckModel getModel() {
        return TruckModel.getInstance();
    }

    @Override
    public void init() {
        numberColumn.setCellValueFactory(new PropertyValueFactory<TruckCommon,String>("regNumber"));
        countDriverColumn.setCellValueFactory(new PropertyValueFactory<TruckCommon,Integer>("countDrivers"));
        classColumn.setCellValueFactory(new PropertyValueFactory<TruckCommon,StatusTruckCommon>("status"));
        List<TruckCommon> availableTrucks = newTaskTimeout(new Callable<List<TruckCommon>>() {
            @Override
            public List<TruckCommon> call() throws Exception {
                return getModel().getAvailableTrucks();
            }
        },3);
        if(availableTrucks != null)
            tableTrucks.getItems().addAll(availableTrucks);
        nextButton.setDisable(true);
    }

    @Override
    public void preShow() {

    }
}
