/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.tsystems.client.logiweb.controllers.manager.drivers;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import ru.tsystems.client.logiweb.controllers.AbstractController;
import ru.tsystems.client.logiweb.models.impl.DriverModel;
import ru.tsystems.client.logiweb.views.AlertDialog;
import ru.tsystems.logiweb.common.entities.impl.DriverCommon;

import java.io.IOException;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.regex.Pattern;

/**
 * FXML Controller class
 *
 * @author dsing
 */
public class ManagerformDriversAddController extends AbstractController {
    @FXML
    private AnchorPane pane;
    @FXML
    private Button close_button;
    @FXML
    private Button add_button;
    @FXML
    private Button cancel_button;
    @FXML
    private TextField field_licence;
    @FXML
    private Button warning_button_licence;
    @FXML
    private TextField field_surname;
    @FXML
    private TextField field_name;
    @FXML
    private TextField field_middlename;

    @FXML
    private TextField passwordField;

    private static final String validMessage = "Licence is not valid (11 numbers)";
    private final static Tooltip tooltipLicence = new Tooltip();


    @Override
    public DriverModel getModel() {
        return DriverModel.getInstance();
    }

    public void init() {
        warning_button_licence.getStyleClass().add("error");
        add_button.setDisable(true);
        tooltipLicence.setText(validMessage);
        warning_button_licence.setTooltip(tooltipLicence);
    }

    @Override
    public void preShow() {

    }

    @FXML
    private void closeAction(ActionEvent event) {
        Node source = (Node) event.getSource();
        Stage stage = (Stage) source.getScene().getWindow();
        stage.close();
    }

    @FXML
    private void addAction(ActionEvent event) throws IOException {
        Boolean isUniqueLicence = (Boolean) newTaskTimeout(new Callable<Object>() {
            @Override
            public Object call() throws Exception {
                return getModel().isUniqueDriver(field_licence.getText());
            }
        }, 3);
        if (isUniqueLicence == null || !isUniqueLicence) {
            AlertDialog.error((Stage) ((Node)event.getSource()).getScene().getWindow(),"Licence is not unique");
            warning_button_licence.getStyleClass().add("error");
            tooltipLicence.setText("Licence is not unique");
            warning_button_licence.setTooltip(tooltipLicence);
            add_button.setDisable(false);
            return;
        }
        DriverCommon driver = new DriverCommon();
        driver.setSNM(field_surname.getText() + " " + field_name.getText() + " " + field_middlename.getText());
        driver.setLicence(field_licence.getText());
        driver.setPassword(passwordField.getText());
        callback.call(driver);
        close((Node) event.getSource());
    }

    @FXML
    private void licence_key_pressed(KeyEvent event) {
        warning_button_licence.getStyleClass().removeAll("error");
        warning_button_licence.setTooltip(null);
        boolean isValid;
        if (isValid = !isValidLicence()) {
            warning_button_licence.getStyleClass().add("error");
            tooltipLicence.setText(validMessage);
            warning_button_licence.setTooltip(tooltipLicence);
        }
        add_button.setDisable(isValid);
    }

    Pattern xLicence = Pattern.compile("\\d{11}");

    private boolean isValidLicence() {
        return xLicence.matcher(field_licence.getText()).matches();
    }

    @Override
    public List<AbstractController> getChildControllers() {
        return null;
    }
}
