/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.tsystems.client.logiweb.controllers.driver.info;

import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.control.Tab;
import ru.tsystems.client.logiweb.controllers.AbstractController;
import ru.tsystems.client.logiweb.controllers.IController;
import ru.tsystems.client.logiweb.models.impl.DriverModel;
import ru.tsystems.client.logiweb.views.UIClass;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

/**
 * FXML Controller class
 *
 * @author dsing
 */
public class DriverformInfoController extends AbstractController {
    @FXML
    private Tab personalTab;
    @FXML
    private Tab orderTab;
    @FXML
    private Tab driversTab;
    @FXML
    private Tab truckTab;

    private IController loaderPersonal;

    /**
     * Initializes the controller class.
     */

    @FXML
    private void personalTabSelection(Event event) throws IOException {
        FXMLLoader loader = new FXMLLoader(UIClass.class.getResource("fxmls/driver/info/driverform_info_personal.fxml"));
        Node n = (Node) loader.load();
        loaderPersonal = loader.getController();
        loaderPersonal.setObject(object);
        loaderPersonal.preShow();
        personalTab.setContent(n);
    }

    @FXML
    private void orderTabSelection(Event event) throws IOException {
        FXMLLoader loader= new FXMLLoader(UIClass.class.getResource("fxmls/driver/info/driverform_info_order.fxml"));
        Node n = (Node) loader.load();
        IController loaderOrder = loader.getController();
        loaderOrder.setObject(object);
        loaderOrder.preShow();
        orderTab.setContent(n);
    }

    @FXML
    private void driversTabSelection(Event event) throws IOException {
        FXMLLoader loader = new FXMLLoader(UIClass.class.getResource("fxmls/driver/info/driverform_info_drivers.fxml"));
        Node n = (Node) loader.load();
        IController loaderDriver = loader.getController();
        loaderDriver.setObject(object);
        loaderDriver.preShow();
        driversTab.setContent(n);
    }

    @FXML
    private void truckTabSelection(Event event) throws IOException {
        FXMLLoader loader = new FXMLLoader(UIClass.class.getResource("fxmls/driver/info/driverform_info_truck.fxml"));
        Node n = (Node) loader.load();
        IController loaderTruck = loader.getController();
        loaderTruck.setObject(object);
        loaderTruck.preShow();
        truckTab.setContent(n);
    }

    @Override
    public DriverModel getModel() {
        return null;
    }

    @Override
    public void init() {

    }

    @Override
    public void preShow() {
        loaderPersonal.setObject(object);
        loaderPersonal.preShow();
    }

    @Override
    public List<AbstractController> getChildControllers() {
        return null;
    }
}
