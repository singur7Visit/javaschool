package ru.tsystems.client.logiweb.views.windows.manager.drivers;

import ru.tsystems.client.logiweb.views.UIClass;

/**
 * Created by dsing on 12.08.14.
 */
public class ManagerformDriversAddWindow extends UIClass {
    public ManagerformDriversAddWindow() throws Exception {
        super("fxmls/manager/drivers/managerform_drivers_add.fxml", 306,400);
    }

    @Override
    protected String getLocalNameCSS() {
        return "manager_add_drivers.css";
    }
}
