package ru.tsystems.client.logiweb.models.connection;

import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

/**
 * Created by dsing on 15.08.14.
 */
public interface IConnection {

    ObjectInputStream getObjectInputStream();

    ObjectOutputStream getObjectOutputStream();

    void close();

}
