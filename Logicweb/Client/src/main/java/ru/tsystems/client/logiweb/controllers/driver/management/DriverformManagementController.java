/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.tsystems.client.logiweb.controllers.driver.management;

import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.event.Event;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import ru.tsystems.client.logiweb.controllers.AbstractController;
import ru.tsystems.client.logiweb.controllers.IController;
import ru.tsystems.client.logiweb.models.Model;
import ru.tsystems.client.logiweb.views.UIClass;

/**
 * FXML Controller class
 *
 * @author dsing
 */
public class DriverformManagementController extends AbstractController {
    @FXML
    private TabPane tabPane;
    @FXML
    private Tab orderTab;
    @FXML
    private Tab statusTab;

    IController order;

    @FXML
    private void orderTabSelection(Event event) throws IOException {
        FXMLLoader loader = new FXMLLoader(UIClass.class.getResource("fxmls/driver/management/driverform_management_order.fxml"));
        Node n = (Node) loader.load();
        order = loader.getController();
        order.setObject(object);
        order.preShow();
        orderTab.setContent(n);
    }

    @FXML
    private void statusTabSelection(Event event) throws IOException {
        FXMLLoader loader = new FXMLLoader(UIClass.class.getResource("fxmls/driver/management/driverform_management_status.fxml"));
        Node n = (Node) loader.load();
        IController status = loader.getController();
        status.setObject(object);
        status.preShow();
        statusTab.setContent(n);
    }

    @Override
    public Model getModel() {
        return null;
    }

    @Override
    public void init() {

    }

    @Override
    public void preShow() {
        order.setObject(object);
        order.preShow();
    }

    @Override
    public List<AbstractController> getChildControllers() {
        return null;
    }
}
