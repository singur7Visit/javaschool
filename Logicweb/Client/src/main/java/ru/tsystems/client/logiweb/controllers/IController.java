package ru.tsystems.client.logiweb.controllers;

import javafx.fxml.Initializable;
import ru.tsystems.client.logiweb.models.Model;
import ru.tsystems.logiweb.common.entities.IObject;

/**
 * Created by dsing on 10.08.14.
 */
public interface IController extends Initializable{
    void setCallback(Callback callback);
    void setBackCallback(Callback callback);
    void setObject (IObject object);
    Model getModel();
    void init();
    void preShow();
    void setTitle(String title);
}
