/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package ru.tsystems.client.logiweb.controllers.manager.orders;

import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;

import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import ru.tsystems.client.logiweb.controllers.AbstractController;
import ru.tsystems.client.logiweb.models.Model;
import ru.tsystems.client.logiweb.models.impl.DriverModel;
import ru.tsystems.logiweb.common.entities.impl.DriverCommon;
import ru.tsystems.logiweb.common.entities.impl.OrderCommon;
import ru.tsystems.logiweb.common.entities.impl.StatusDriverCommon;

/**
 * FXML Controller class
 *
 * @author dsing
 */
public class ManagerformOrderTabDriversController extends AbstractController {
    @FXML
    private TableView<DriverCommon> tableDrivers;
    @FXML
    private TableColumn snmColumn;
    @FXML
    private TableColumn licenceColumn;
    @FXML
    private TableColumn statusColumn;


    @Override
    public List<AbstractController> getChildControllers() {
        return null;
    }

    @Override
    public DriverModel getModel() {
        return DriverModel.getInstance();
    }

    @Override
    public void init() {
        snmColumn.setCellValueFactory(new PropertyValueFactory<DriverCommon, String>("SNM"));
        licenceColumn.setCellValueFactory(new PropertyValueFactory<DriverCommon, String>("licence"));
        statusColumn.setCellValueFactory(new PropertyValueFactory<DriverCommon, StatusDriverCommon>("status"));
    }

    @Override
    public void preShow() {
        if (((OrderCommon) object).getDrivers() != null)
            tableDrivers.getItems().addAll(((OrderCommon) object).getDrivers());
    }
}
