package ru.tsystems.client.logiweb.models.impl;

import org.apache.log4j.Logger;
import ru.tsystems.client.logiweb.models.Model;
import ru.tsystems.logiweb.common.commands.Command;
import ru.tsystems.logiweb.common.commands.ICommand;
import ru.tsystems.logiweb.common.entities.impl.OrderCommon;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Map;

/**
 * Created by dsing on 14.08.14.
 */
public class OrderModel extends Model {
    private static OrderModel orderModel;
    private final static Logger logger = Logger.getLogger(OrderModel.class);

    public static OrderModel getInstance() {
        if (orderModel == null)
            orderModel = new OrderModel();
        return orderModel;
    }

    protected OrderModel() {

    }

    public List<OrderCommon> getAllOrders(int from, int to, String searchWord) {
        ICommand command = new Command();
        if (searchWord != null) {
            command.setCommand("GetAllOrdersSearch");
            command.addParam("SearchWord", searchWord);
        } else
            command.setCommand("GetAllOrders");
        command.addParam("from", from);
        command.addParam("to", to);
        try {
            sendCommand(command);
            ICommand response = readResponse();
            return (List<OrderCommon>) response.getParam("Orders");
        } catch (IOException | ClassNotFoundException | ClassCastException | NullPointerException e) {
            logger.error("Cann't get Orders from server. Message : " + e.getMessage());
            return null;
        } finally {
            closeConnection();
        }
    }

    public OrderCommon shipOrder(OrderCommon order) {
        ICommand command = new Command();
        command.addParam("Order", order);
        command.setCommand("ShipOrder");
        try {
            sendCommand(command);
            ICommand response = readResponse();
            if (response != null && !(Boolean) response.getParam("Status"))
                logger.warn("Order cann't be ship. Message : " + response.getParam("Message"));
            return (OrderCommon) response.getParam("Order");
        } catch (IOException | ClassNotFoundException | NullPointerException e) {
            logger.error("Cann't be ship order. Message : " + e.getMessage());
            return null;
        } finally {
            closeConnection();
        }
    }

    public OrderCommon confirmOrder(OrderCommon order) {
        ICommand command = new Command();
        command.addParam("Order", order);
        command.setCommand("ConfirmOrder");
        try {
            sendCommand(command);
            ICommand response = readResponse();
            if (response != null && !(Boolean) response.getParam("Status"))
                logger.warn("Order cann't be confirm. Message : " + response.getParam("Message"));
            return (OrderCommon) response.getParam("Order");
        } catch (IOException | ClassNotFoundException | NullPointerException e) {
            logger.error("Cann't be confirm order. Message : " + e.getMessage());
            return null;
        } finally {
            closeConnection();
        }
    }

    public OrderCommon completeOrder(OrderCommon order) {
        ICommand command = new Command();
        command.addParam("Order", order);
        command.setCommand("CompleteOrder");
        try {
            sendCommand(command);
            ICommand response = readResponse();
            if (response != null && !(Boolean) response.getParam("Status"))
                logger.warn("Order cann't be complete. Message : " + response.getParam("Message"));
            return (OrderCommon) response.getParam("Order");
        } catch (IOException | ClassNotFoundException | NullPointerException e) {
            logger.error("Cann't be complete order. Message : " + e.getMessage());
            return null;
        } finally {
            closeConnection();
        }
    }

    public OrderCommon closeOrder(OrderCommon order) {
        ICommand command = new Command();
        command.addParam("Order", order);
        command.setCommand("CloseOrder");
        try {
            sendCommand(command);
            ICommand response = readResponse();
            if (response != null && !(Boolean) response.getParam("Status"))
                logger.warn("Order cann't be close. Message : " + response.getParam("Message"));
            return (OrderCommon) response.getParam("Order");
        } catch (IOException | ClassNotFoundException | NullPointerException e) {
            logger.error("Cann't be close order. Message : " + e.getMessage());
            return null;
        } finally {
            closeConnection();
        }
    }

    public OrderCommon createOrder(OrderCommon order) {
        ICommand command = new Command();
        command.addParam("Order", order);
        command.setCommand("CreateOrder");
        try {
            sendCommand(command);
            ICommand response = readResponse();
            if (response != null && !(Boolean) response.getParam("Status")) {
                logger.warn("Order cann't be create. Message : " + response.getParam("Message"));
                throw new IOException();
            }
            return (OrderCommon) response.getParam("Order");
        } catch (IOException | ClassNotFoundException | NullPointerException e) {
            logger.error("Cann't be create order. Message : " + e.getMessage());
            return null;
        } finally {
            closeConnection();
        }
    }

    public Boolean rollbackOrder(OrderCommon order) {
        ICommand command = new Command();
        command.addParam("Order", order);
        command.setCommand("RollbackOrder");
        try {
            sendCommand(command);
            ICommand response = readResponse();
            if (response != null && !(Boolean) response.getParam("Status"))
                logger.warn("Driver cann't be rollback. Message : " + response.getParam("Message"));
            return (Boolean) response.getParam("Status");
        } catch (IOException | ClassNotFoundException | NullPointerException e) {
            logger.error("Cann't rollback driver. Message : " + e.getMessage());
            return null;
        } finally {
            closeConnection();
        }
    }

    public OrderCommon getAllInformation(OrderCommon orderCommon) {
        ICommand command = new Command();
        command.addParam("Order", orderCommon);
        command.setCommand("AllInformationOrder");
        try {
            sendCommand(command);
            ICommand response = readResponse();
            if (response != null && !(Boolean) response.getParam("Status"))
                logger.warn("Driver cann't be rollback. Message : " + response.getParam("Message"));
            return (OrderCommon) response.getParam("Order");
        } catch (IOException | ClassNotFoundException | NullPointerException e) {
            logger.error("Cann't rollback driver. Message : " + e.getMessage());
            return null;
        } finally {
            closeConnection();
        }
    }

    public Boolean removeOrder(OrderCommon order) {
        ICommand command = new Command();
        command.addParam("Order", order);
        command.setCommand("RemoveOrder");
        try {
            sendCommand(command);
            ICommand response = readResponse();
            if (response != null && !(Boolean) response.getParam("Status"))
                logger.warn("Driver cann't be removing. Message : " + response.getParam("Message"));
            return (Boolean) response.getParam("Status");
        } catch (IOException | ClassNotFoundException | NullPointerException e) {
            logger.error("Cann't remove driver. Message : " + e.getMessage());
            return null;
        } finally {
            closeConnection();
        }
    }
}
