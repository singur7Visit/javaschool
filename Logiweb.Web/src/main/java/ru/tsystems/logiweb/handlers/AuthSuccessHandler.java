package ru.tsystems.logiweb.handlers;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Collection;

/**
 * Created by dsing on 05.09.14.
 */
public class AuthSuccessHandler implements AuthenticationSuccessHandler {

    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    @Override
    public void onAuthenticationSuccess(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, Authentication authentication) throws IOException, ServletException {
        handle(httpServletRequest, httpServletResponse, authentication);
        clearAuthAttribute(httpServletRequest);
    }

    protected void handle(HttpServletRequest request, HttpServletResponse response,
                          Authentication authentication) throws IOException {
        String targetUrl = targetUrl(authentication);
        if(response.isCommitted())
            return;
        request.getSession().setAttribute("User", authentication.getPrincipal());
        redirectStrategy.sendRedirect(request, response, targetUrl);
    }

    private void clearAuthAttribute(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if(session != null)
            session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
    }

    protected String targetUrl(Authentication authentication) {
        Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
        for(GrantedAuthority authority : authorities) {
            switch (authority.getAuthority()) {
                case "ROLE_ADMIN" :
                    return "/admin";
                case "ROLE_MANAGER" :
                    return "/manager";
                case "ROLE_DRIVER" :
                    return "/driver";
            }
        }
        return "/index?error";
    }
}
