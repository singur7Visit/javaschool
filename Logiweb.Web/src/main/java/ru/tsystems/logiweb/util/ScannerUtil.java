package ru.tsystems.logiweb.util;

import org.apache.log4j.Logger;

import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.List;

/**
 * Util for connect dto and entity
 */
public class ScannerUtil {
    private final static Logger logger = Logger.getLogger(ScannerUtil.class);
    public static <T,V> List<T> scanEntityList(List<V> list, Class<T> clazz) {
        List<T> newList = new ArrayList<>();
        try {
            for(V v : list)
                newList.add(clazz.getConstructor(v.getClass()).newInstance(v));
        } catch (InstantiationException | IllegalAccessException | NoSuchMethodException | InvocationTargetException e) {
            logger.error(e.getMessage());
        }
        return newList;
    }

}
