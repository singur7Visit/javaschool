package ru.tsystems.logiweb.controllers.driver;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import javax.servlet.http.HttpSession;

/**
 * Driver controller for views
 */
@Controller
@RequestMapping("/driver")
public class ViewsDriverController {

    @RequestMapping({"","/index"})
    public String index(){
        return "views/driver/index";
    }

    @RequestMapping(value = "/regDriver", method = RequestMethod.POST)
    public String regDriver(@RequestParam(value = "licence") String licence,
                            HttpSession session) {
        session.setAttribute("driver",licence);
        return "redirect:index";
    }

    @RequestMapping(value = "/logout")
    public String logout(HttpSession session) {
        session.removeAttribute("driver");
        return "redirect:index";
    }

    @RequestMapping(value = "/manage")
    public String manager(HttpSession session) {
        String view;
        if(session.getAttribute("driver") != null)
            view = "views/driver/manage";
        else
            view = "redirect:index";
        return view;
    }

}
