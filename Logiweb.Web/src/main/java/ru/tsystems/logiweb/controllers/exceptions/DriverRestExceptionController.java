package ru.tsystems.logiweb.controllers.exceptions;

import org.apache.log4j.Logger;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.tsystems.logiweb.dto.response.ErrorResponseRestMessage;
import ru.tsystems.logiweb.service.exceptions.drivers.DriverBusyException;
import ru.tsystems.logiweb.service.exceptions.drivers.DriverNotFoundException;
import ru.tsystems.logiweb.service.exceptions.drivers.DriverStatusException;
import ru.tsystems.logiweb.service.exceptions.drivers.NotUniqueDriverException;

import javax.servlet.http.HttpServletRequest;

/**
 * Handler for service driver exception
 */
@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class DriverRestExceptionController extends RestExceptionController {

    private final static Logger logger = Logger.getLogger(DriverRestExceptionController.class);

    @ExceptionHandler(DriverBusyException.class)
    @ResponseBody
    public ErrorResponseRestMessage busyTruckException(DriverBusyException e) {
        String message = String.format(getMessage("busy", "delete"), e.getId());
        logger.error(message);
        return new ErrorResponseRestMessage().setMessage(message);
    }

    @ExceptionHandler(DriverNotFoundException.class)
    @ResponseBody
    public ErrorResponseRestMessage driverNotFoundException(HttpServletRequest request,DriverNotFoundException e) {
        String message = String.format(getMessage("notFound", request.getRequestURI().contains("delete") ? "delete" : "get"), e.getId());
        logger.error(message);
        return new ErrorResponseRestMessage().setMessage(message);
    }

    @ExceptionHandler(NotUniqueDriverException.class)
    @ResponseBody
    public ErrorResponseRestMessage driverNotUniqueException(NotUniqueDriverException e) {
        String message = String.format(getMessage("notUnique", "create"), e.getDrivingLicence());
        logger.error(message);
        return new ErrorResponseRestMessage().setMessage(message);
    }

    @ExceptionHandler(DriverStatusException.class)
    @ResponseBody
    public ErrorResponseRestMessage driverStatusException(DriverStatusException e) {
        String message = String.format(getMessage("wrong", "status"), e.getDrivingLicence());
        logger.error(message);
        return new ErrorResponseRestMessage().setMessage(message);
    }

    @Override
    String getTypeMessage() {
        return "error";
    }

    @Override
    String getPrefixMessage() {
        return "driver";
    }
}
