package ru.tsystems.logiweb.controllers;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpSession;

/**
 * Created by dsing on 05.09.14.
 */
@Controller
public class LoginController {

    @RequestMapping("/login")
    public String login(Model model) {
        return "redirect:/index";
    }
}
