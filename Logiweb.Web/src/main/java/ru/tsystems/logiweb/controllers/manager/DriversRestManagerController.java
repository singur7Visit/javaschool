package ru.tsystems.logiweb.controllers.manager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.tsystems.logiweb.dto.DriverDTO;
import ru.tsystems.logiweb.dto.response.ErrorResponseRestMessage;
import ru.tsystems.logiweb.dto.response.ResponseRestMessage;
import ru.tsystems.logiweb.dto.response.SuccessResponseRestMessage;
import ru.tsystems.logiweb.entities.Driver;
import ru.tsystems.logiweb.service.DriverService;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;

/**
 * Manager controller for actions with drivers
 */
@RestController
@RequestMapping("/manager/drivers")
public class DriversRestManagerController {

    @Autowired
    private DriverService driverService;

    @RequestMapping(method = RequestMethod.GET,value = "/all")
    public List<DriverDTO> getDrivers() {
        List<DriverDTO> allDrivers = new ArrayList<>();
        for(Driver driver : driverService.getAllDrivers())
            allDrivers.add(new DriverDTO(driver));
        return allDrivers;
    }


    @RequestMapping(method = RequestMethod.PUT,value = "/add",
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseRestMessage addDriver(@RequestBody @Valid DriverDTO driver) {
        Driver d = new Driver();
        d.setLicence(driver.getLicence());
        d.setSNM(driver.getSNM());
        Long id = driverService.createDriver(d);
        return driver == null || id == 0 ?
                new ErrorResponseRestMessage().setMessage(String.format("Cann't create driver with register number : %s",
                        d.getLicence()))
                : new SuccessResponseRestMessage().setMessage("Success");
    }

    @RequestMapping(method = RequestMethod.DELETE,value = "/delete/{id}")
    public ResponseRestMessage removeDriver(@PathVariable("id") long id) {
        boolean response = driverService.removeDriver(id);
        return !response ? new ErrorResponseRestMessage().setMessage(String.format("Cann't delete driver with id : %d",id))
                : new SuccessResponseRestMessage().setMessage("Success");
    }

    @RequestMapping(method = RequestMethod.GET,value = "/available")
    public List<DriverDTO> getAvailableDrivers() {
        List<DriverDTO> availableDrivers = new ArrayList<>();
        for(Driver driver : driverService.getAvailableDrivers())
            availableDrivers.add(new DriverDTO(driver));
        return availableDrivers;
    }



}
