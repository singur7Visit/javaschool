package ru.tsystems.logiweb.controllers.manager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.tsystems.logiweb.dto.OrderDTO;
import ru.tsystems.logiweb.dto.StuffDTO;
import ru.tsystems.logiweb.dto.request.ListStuffs;
import ru.tsystems.logiweb.dto.request.RequestShip;
import ru.tsystems.logiweb.dto.response.ErrorResponseRestMessage;
import ru.tsystems.logiweb.dto.response.ResponseRestMessage;
import ru.tsystems.logiweb.dto.response.SuccessResponseRestMessage;
import ru.tsystems.logiweb.entities.Order;
import ru.tsystems.logiweb.entities.Stuff;
import ru.tsystems.logiweb.service.OrderService;
import ru.tsystems.logiweb.util.ScannerUtil;

import java.util.ArrayList;
import java.util.List;

/**
 * Manager controller for actions with drivers
 */
@RestController
@RequestMapping("/manager/orders")
public class OrdersRestManagerController {

    @Autowired
    private OrderService orderService;

    @RequestMapping(method = RequestMethod.GET,value = "/all")
    public List<OrderDTO> getOrders() {
        return ScannerUtil.scanEntityList(orderService.getAllOrders(), OrderDTO.class);
    }


    @RequestMapping(method = RequestMethod.PUT,value = "/create",
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseRestMessage addOrder() {
        Order order = orderService.createOrder();
        return order == null || order.getId() == 0 ?
                new ErrorResponseRestMessage().setMessage(String.format("Cann't create order with register number : %s",
                        order != null ? order.getNumOrder() : ""))
                : new SuccessResponseRestMessage().setMessage("Success");
    }

    @RequestMapping(method = RequestMethod.PUT,value = "/{id}/confirm",
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseRestMessage confirmOrder (@PathVariable long id, @RequestBody ArrayList<Stuff> stuffs){
        Order order = orderService.confirmOrder(id, stuffs);
        return order == null || order.getId() == 0 ?
                new ErrorResponseRestMessage().setMessage(String.format("Cann't create order with register number : %s",
                        order != null ? order.getNumOrder() : ""))
                : new SuccessResponseRestMessage().setMessage("Success");
    }
    @RequestMapping(method = RequestMethod.PUT,value = "/{id}/ship",
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseRestMessage shipOrder (@PathVariable long id, @RequestBody RequestShip requestShip){
        Order order = orderService.shipOrder(id,requestShip.getIdsDrivers(),requestShip.getIdTruck());
        return order == null || order.getId() == 0 ?
                new ErrorResponseRestMessage().setMessage(String.format("Cann't create order with register number : %s",
                        order != null ? order.getNumOrder() : ""))
                : new SuccessResponseRestMessage().setMessage("Success");
    }

    @RequestMapping(method = RequestMethod.PUT,value = "/{id}/close")
    public ResponseRestMessage closeOrder(@PathVariable long id) {
        Order order = orderService.closeOrder(id);
        return order == null || order.getId() == 0 ?
                new ErrorResponseRestMessage().setMessage(String.format("Cann't close order with id : %s",
                        order != null ? order.getNumOrder() : ""))
                : new SuccessResponseRestMessage().setMessage("Success");
    }

    @RequestMapping(method = RequestMethod.DELETE,value = "/delete/{id}")
    public ResponseRestMessage removeOrder(@PathVariable("id") long id) {
        boolean response = orderService.removeOrder(id);
        return !response ? new ErrorResponseRestMessage().setMessage(String.format("Cann't delete order with id : %d",id))
                : new SuccessResponseRestMessage().setMessage("Success");
    }





}
