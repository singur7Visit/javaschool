package ru.tsystems.logiweb.controllers.exceptions;

import org.apache.log4j.Logger;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.tsystems.logiweb.dto.response.ErrorResponseRestMessage;
import ru.tsystems.logiweb.service.exceptions.trucks.TruckBusyException;
import ru.tsystems.logiweb.service.exceptions.trucks.NotUniqueTruckException;
import ru.tsystems.logiweb.service.exceptions.trucks.TruckNotFoundException;

import javax.servlet.http.HttpServletRequest;

/**
 * Handler for truck service exception
 */
@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class TruckRestExceptionsController extends RestExceptionController {

    private final static Logger logger = Logger.getLogger(TruckRestExceptionsController.class);

    @ExceptionHandler(TruckBusyException.class)
    @ResponseBody
    public ErrorResponseRestMessage busyTruckException(TruckBusyException e) {
        String message = String.format(getMessage("busy", "delete"), e.getId());
        logger.error(message);
        return new ErrorResponseRestMessage().setMessage(message);
    }

    @ExceptionHandler(TruckNotFoundException.class)
    @ResponseBody
    public ErrorResponseRestMessage truckNotFoundException(HttpServletRequest request,TruckNotFoundException e) {
        String message = String.format(getMessage("notFound", request.getRequestURI().contains("delete") ? "delete" : "get"), e.getId());
        logger.error(message);
        return new ErrorResponseRestMessage().setMessage(message);
    }

    @ExceptionHandler(NotUniqueTruckException.class)
    @ResponseBody
    public ErrorResponseRestMessage truckNotUniqueException(NotUniqueTruckException e) {
        String message = String.format(getMessage("notUnique", "create"), e.getRegNumber());
        logger.error(message);
        return new ErrorResponseRestMessage().setMessage(message);
    }

    @Override
    String getTypeMessage() {
        return "error";
    }

    @Override
    String getPrefixMessage() {
        return "truck";
    }
}
