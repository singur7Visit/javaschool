package ru.tsystems.logiweb.controllers.manager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.tsystems.logiweb.dto.ClassCapacityDTO;
import ru.tsystems.logiweb.dto.TruckDTO;
import ru.tsystems.logiweb.dto.response.ErrorResponseRestMessage;
import ru.tsystems.logiweb.dto.response.ResponseRestMessage;
import ru.tsystems.logiweb.dto.response.SuccessResponseRestMessage;
import ru.tsystems.logiweb.entities.ClassCapacity;
import ru.tsystems.logiweb.entities.Truck;
import ru.tsystems.logiweb.service.TruckService;
import ru.tsystems.logiweb.util.ScannerUtil;

import javax.validation.Valid;
import java.util.List;

/**
 * Manager controller for actions with drivers
 */
@RestController
@RequestMapping("/manager/trucks")
public class TrucksRestManagerController {

    @Autowired
    private TruckService truckService;

    @RequestMapping(method = RequestMethod.GET,value = "/all")
    public List<TruckDTO> getTrucks() {
        return ScannerUtil.scanEntityList(truckService.getAllTrucks(),TruckDTO.class);
    }

    @RequestMapping(method = RequestMethod.GET,value = "/classes/all")
    public List<ClassCapacityDTO> getClassesCapacity() {
        return ScannerUtil.scanEntityList(truckService.getClasses(), ClassCapacityDTO.class);
    }

    @RequestMapping(method = RequestMethod.PUT,value = "/add",
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseRestMessage addTruck(@RequestBody @Valid TruckDTO truck) {
        Truck newTruck = new Truck();
        ClassCapacity classCapacity = new ClassCapacity();
        classCapacity.setName(truck.getClassCapacity().getName());
        newTruck.setClassCapacity(classCapacity); newTruck.setCountDrivers(truck.getCountDrivers());
        newTruck.setRegNumber(truck.getRegNumber());
        Long id = truckService.createTruck(newTruck);
        return truck == null || id == 0 ?
                new ErrorResponseRestMessage().setMessage(String.format("Cann't create truck with register number : %s",
                        truck != null ? truck.getRegNumber() : ""))
                : new SuccessResponseRestMessage().setMessage("Success");
    }

    @RequestMapping(method = RequestMethod.DELETE,value = "/delete/{id}")
    public ResponseRestMessage removeTruck(@PathVariable("id") long id) {
        boolean response = truckService.removeTruck(id);
        return !response ? new ErrorResponseRestMessage().setMessage(String.format("Cann't delete truck with id : %d",id))
                : new SuccessResponseRestMessage().setMessage("Success");
    }

    @RequestMapping(method = RequestMethod.GET,value = "/available")
    public List<TruckDTO> getAvailableTrucks() {
        return ScannerUtil.scanEntityList(truckService.getAvailableTrucks(),TruckDTO.class);
    }

}
