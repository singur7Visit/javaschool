package ru.tsystems.logiweb.controllers.driver;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import ru.tsystems.logiweb.dto.*;
import ru.tsystems.logiweb.dto.response.DriverInfoResponseDTO;
import ru.tsystems.logiweb.dto.response.ErrorResponseRestMessage;
import ru.tsystems.logiweb.dto.response.ResponseRestMessage;
import ru.tsystems.logiweb.dto.response.SuccessResponseRestMessage;
import ru.tsystems.logiweb.entities.Driver;
import ru.tsystems.logiweb.entities.StatusDriver;
import ru.tsystems.logiweb.entities.Stuff;
import ru.tsystems.logiweb.service.DriverService;
import ru.tsystems.logiweb.service.OrderService;
import ru.tsystems.logiweb.service.exceptions.drivers.DriverNotFoundException;

import java.util.ArrayList;
import java.util.List;

/**
 * Driver controller for actions
 */

@RestController
@RequestMapping("/driver")
public class RestDriverController {

    @Autowired
    private DriverService driverService;

    @Autowired
    private OrderService orderService;

    @RequestMapping("/{licence}/info")
    public DriverInfoResponseDTO getInfo(@PathVariable String licence) {
        DriverInfoResponseDTO response = new DriverInfoResponseDTO();
        Driver driver = driverService.findByLicence(licence);
        response.setDriver(new DriverDTO(driver));
        if (driver == null)
            throw new DriverNotFoundException(licence);
        if(driver.getOrder() == null)
            return response;
        response.setNumOrder(driver.getOrder().getNumOrder());
        List<DriverDTO> coDrivers = new ArrayList<>();
        for(Driver d : driverService.getCoDrivers(driver))
            coDrivers.add(new DriverDTO(d));
        response.setCoDrivers(coDrivers);
        List<StuffDTO> stuffs = new ArrayList<>();
        for(Stuff stuff : driver.getOrder().getStuffs())
            stuffs.add(new StuffDTO(stuff));
        response.setStuffs(stuffs);
        response.setTruck(new TruckDTO(driver.getTruck()));
        return response;
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/stuff/{id}/deliver")
    public ResponseRestMessage deliverStuff(@PathVariable("id") long id) {
        boolean response = orderService.deliveredStuff(id);
        return !response ?
                new ErrorResponseRestMessage().setMessage(String.format("Cann't delivered stuff with id: %d",id))
                : new SuccessResponseRestMessage().setMessage("Success");
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/order/{number}/complete")
    public ResponseRestMessage completeOrder(@PathVariable("number") Long number) {
        boolean response = orderService.completeOrder(number);
        return !response ?
                new ErrorResponseRestMessage().setMessage(String.format("Cann't complete order with number: %d",number))
                : new SuccessResponseRestMessage().setMessage("Success");
    }

    @RequestMapping(method = RequestMethod.PUT, value = "/{licence}/status",
            produces = MediaType.APPLICATION_JSON_VALUE,
            consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseRestMessage setStatus(@PathVariable("licence") String licence,@RequestBody StatusDriverDTO status) {
        StatusDriver statusDriver = new StatusDriver();
        statusDriver.setName(status.getName());
        boolean response = driverService.setStatus(licence,statusDriver);
        return !response ?
                new ErrorResponseRestMessage().
                        setMessage(String.format("Cann't change status for driver with licecne: %s",licence))
                : new SuccessResponseRestMessage().setMessage("Success");
    }
}
