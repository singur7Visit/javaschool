package ru.tsystems.logiweb.controllers.exceptions;

import org.apache.log4j.Logger;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.tsystems.logiweb.dto.response.ErrorResponseRestMessage;

/**
 * Handler for usually exception
 */
@ControllerAdvice
public class DefaultRestHandlerException extends RestExceptionController {

    private final static Logger logger = Logger.getLogger(DefaultRestHandlerException.class);
    @ExceptionHandler(Throwable.class)
    @Order(Ordered.LOWEST_PRECEDENCE)
    @ResponseBody
    public ErrorResponseRestMessage handleException(Throwable e) {
        String message = getMessage("error","action");
        logger.error(message + e.getMessage());
        return new ErrorResponseRestMessage().setMessage(message);
    }

    @Override
    String getTypeMessage() {
        return "error";
    }

    @Override
    String getPrefixMessage() {
        return "server";
    }
}
