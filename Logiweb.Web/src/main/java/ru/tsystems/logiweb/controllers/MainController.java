package ru.tsystems.logiweb.controllers;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Collection;

/**
 * Created by dsing on 04.09.14.
 */

@Controller
public class MainController {

    @RequestMapping({"/index.html", "/index","/"})
    public String index() {
        return handleUser(SecurityContextHolder.getContext().getAuthentication().getAuthorities());
    }


    private String handleUser(Collection<? extends GrantedAuthority> authorities) {
        for(GrantedAuthority grantedAuthority : authorities) {
            switch (grantedAuthority.getAuthority()) {
                case "ROLE_ADMIN" :
                    return "redirect:/admin";
                case "ROLE_MANAGER" :
                    return "redirect:/manager";
                case "ROLE_USER" :
                    return "redirect:/driver";
                default:
                    return "views/login";
            }
        }
        return null;
    }
}
