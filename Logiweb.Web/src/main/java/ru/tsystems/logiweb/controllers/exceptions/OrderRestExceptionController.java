package ru.tsystems.logiweb.controllers.exceptions;

import org.apache.log4j.Logger;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.tsystems.logiweb.dto.response.ErrorResponseRestMessage;
import ru.tsystems.logiweb.service.exceptions.orders.*;

/**
 * Handler for service order exception
 */
@ControllerAdvice
@Order(Ordered.HIGHEST_PRECEDENCE)
public class OrderRestExceptionController extends RestExceptionController {

    private static Logger logger = Logger.getLogger(OrderRestExceptionController.class);

    @ExceptionHandler(OrderNotFoundException.class)
    @ResponseBody
    public ErrorResponseRestMessage orderNotFoundException(OrderNotFoundException e) {
        String message = String.format(getMessage("notFound", "get"), e.getId());
        logger.error(message);
        return new ErrorResponseRestMessage().setMessage(message);
    }

    @ExceptionHandler(OrderEmptyListStuffsException.class)
    @ResponseBody
    public ErrorResponseRestMessage orderEmptyListStuffsException(OrderEmptyListStuffsException e) {
        String message = String.format(getMessage("noStuffs", "confirm"), e.getId());
        logger.error(message);
        return new ErrorResponseRestMessage().setMessage(message);
    }

    @ExceptionHandler(OrderNotUniqueException.class)
    @ResponseBody
    public ErrorResponseRestMessage orderNotUniqueException(OrderNotUniqueException e) {
        String message = String.format(getMessage("notUnique", "create"), e.getNumber());
        logger.error(message);
        return new ErrorResponseRestMessage().setMessage(message);
    }

    @ExceptionHandler(OrderShipCountDriversException.class)
    @ResponseBody
    public ErrorResponseRestMessage orderShipCountDriversException(OrderShipCountDriversException e) {
        String message = String.format(getMessage("countDrivers", "ship"),
                e.getCountTruckDrivers(), e.getCountDrivers());
        logger.error(message);
        return new ErrorResponseRestMessage().setMessage(message);
    }
    @ExceptionHandler(OrderStatusDriverException.class)
    @ResponseBody
    public ErrorResponseRestMessage orderCloseStatusDriverException(OrderStatusDriverException e) {
        String message = String.format(getMessage("statusDriver", "close"),e.getId());
        logger.error(message);
        return new ErrorResponseRestMessage().setMessage(message);
    }


    @ExceptionHandler(OrderShipException.class)
    @ResponseBody
    public ErrorResponseRestMessage orderShipException(OrderShipException e) {
        String message;
        if(e.getTruck() == null)
            message = getMessage("emptyTruck","ship");
        else
            message = getMessage("emptyDrivers","ship");
        logger.error(message);
        return new ErrorResponseRestMessage().setMessage(message);
    }

    @Override
    String getTypeMessage() {
        return "error";
    }

    @Override
    String getPrefixMessage() {
        return "order";
    }
}
