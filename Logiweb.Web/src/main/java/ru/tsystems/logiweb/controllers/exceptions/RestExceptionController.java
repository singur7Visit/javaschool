package ru.tsystems.logiweb.controllers.exceptions;

import org.apache.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import ru.tsystems.logiweb.dto.response.ErrorResponseRestMessage;

import java.util.Locale;

/**
 * abstract handler
 */
public abstract class RestExceptionController {

    private final static Logger logger = Logger.getLogger(RestExceptionController.class);

    @Autowired
    private MessageSource messageSource;

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseBody
    public ErrorResponseRestMessage handleMethodArgumentNotValid(MethodArgumentNotValidException e) {
        String message = e.getBindingResult().getFieldError().getDefaultMessage();
        logger.error(message);
        return new ErrorResponseRestMessage().setMessage(message);
    }


    protected String getMessage(String name,String action) {
        return getMessage(name, action,getPrefixMessage());
    }

    protected String getMessage(String name,String action,String prefix) {
        return messageSource.getMessage(new StringBuilder()
                .append(getTypeMessage())
                .append('.')
                .append(prefix)
                .append('.')
                .append(action)
                .append('.')
                .append(name)
                .toString(),null,Locale.getDefault());
    }

    abstract String getTypeMessage();

    abstract String getPrefixMessage();
}
