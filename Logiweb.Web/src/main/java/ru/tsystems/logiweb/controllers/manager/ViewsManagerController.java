package ru.tsystems.logiweb.controllers.manager;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Manager controller for views;
 */
@Controller
@RequestMapping("/manager")
public class ViewsManagerController {

    @RequestMapping({"","/index"})
    public String index() {
        return "views/manager/index";
    }

    @RequestMapping("/trucks")
    public String trucks() {
        return "views/manager/trucks";
    }

    @RequestMapping("/drivers")
    public String drivers() {
        return "views/manager/drivers";
    }

    @RequestMapping("/orders")
    public String orders() {
        return "views/manager/orders";
    }
}
