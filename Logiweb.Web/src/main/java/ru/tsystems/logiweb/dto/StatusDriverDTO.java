package ru.tsystems.logiweb.dto;

import com.fasterxml.jackson.annotation.JsonValue;
import ru.tsystems.logiweb.entities.StatusDriver;

/**
 * DTO for entity
 * @see StatusDriver
 */
public class StatusDriverDTO {
    private String name;

    public StatusDriverDTO(){}

    public StatusDriverDTO(StatusDriver statusDriver) {
        this.name = statusDriver == null ? "" : statusDriver.getName();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonValue
    public String toString() {
        return name;
    }
}
