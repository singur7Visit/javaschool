package ru.tsystems.logiweb.dto.response;

import ru.tsystems.logiweb.dto.DriverDTO;
import ru.tsystems.logiweb.dto.StuffDTO;
import ru.tsystems.logiweb.dto.TruckDTO;
import ru.tsystems.logiweb.entities.Driver;
import ru.tsystems.logiweb.entities.Stuff;
import ru.tsystems.logiweb.entities.Truck;

import java.util.List;

/**
 * DTO fro get info about driver
 */
public class DriverInfoResponseDTO {

    private List<StuffDTO> stuffs;
    private TruckDTO truck;
    private List<DriverDTO> coDrivers;
    private DriverDTO driver;
    private Long numOrder;

    public List<StuffDTO> getStuffs() {
        return stuffs;
    }

    public void setStuffs(List<StuffDTO> stuffs) {
        this.stuffs = stuffs;
    }

    public TruckDTO getTruck() {
        return truck;
    }

    public void setTruck(TruckDTO truck) {
        this.truck = truck;
    }

    public List<DriverDTO> getCoDrivers() {
        return coDrivers;
    }

    public void setCoDrivers(List<DriverDTO> coDrivers) {
        this.coDrivers = coDrivers;
    }

    public DriverDTO getDriver() {
        return driver;
    }

    public void setDriver(DriverDTO driver) {
        this.driver = driver;
    }

    public Long getNumOrder() {
        return numOrder;
    }

    public void setNumOrder(Long numOrder) {
        this.numOrder = numOrder;
    }
}
