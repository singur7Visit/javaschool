package ru.tsystems.logiweb.dto;

import com.fasterxml.jackson.annotation.JsonValue;
import ru.tsystems.logiweb.entities.ClassCapacity;

/**
 * DTO for entity
 * @see ClassCapacity
 */
public class ClassCapacityDTO {

    public ClassCapacityDTO(){}

    public ClassCapacityDTO(ClassCapacity classCapacity) {
        this.name = classCapacity.getName();
    }

    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    @JsonValue
    public String toString() {
        return name;
    }
}
