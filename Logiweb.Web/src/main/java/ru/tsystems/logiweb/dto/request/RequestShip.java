package ru.tsystems.logiweb.dto.request;

import java.io.Serializable;

/**
 * Created by dsing on 19.09.14.
 */
public class RequestShip implements Serializable{
    private long idTruck;
    private long[] idsDrivers;

    public long getIdTruck() {
        return idTruck;
    }

    public void setIdTruck(long idTruck) {
        this.idTruck = idTruck;
    }

    public long[] getIdsDrivers() {
        return idsDrivers;
    }

    public void setIdsDrivers(long[] idsDrivers) {
        this.idsDrivers = idsDrivers;
    }
}
