package ru.tsystems.logiweb.dto;

import com.fasterxml.jackson.annotation.JsonValue;
import ru.tsystems.logiweb.entities.StatusOrder;

/**
 * DTO for entity
 * @see ru.tsystems.logiweb.entities.StatusOrder
 */
public class StatusOrderDTO {

    private String name;

    public StatusOrderDTO(){}

    public StatusOrderDTO(StatusOrder statusOrder) {
        this.name = statusOrder.getName();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonValue
    public String toString() {
        return name;
    }
}
