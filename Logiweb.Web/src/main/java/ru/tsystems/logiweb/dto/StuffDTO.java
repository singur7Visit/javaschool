package ru.tsystems.logiweb.dto;

import ru.tsystems.logiweb.entities.Stuff;

import java.math.BigDecimal;

/**
 * DTO for entity
 * @see Stuff
 */
public class StuffDTO {

    public StuffDTO(){}

    public StuffDTO(Stuff stuff) {
        this.id = stuff.getId();
        this.gps = stuff.getGps();
        this.name = stuff.getName();
        this.weight = stuff.getWeight();
        this.status = stuff.isStatus();
    }

    private long id;

    private String gps;

    private String name;

    private BigDecimal weight;

    private boolean status;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getGps() {
        return gps;
    }

    public void setGps(String gps) {
        this.gps = gps;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getWeight() {
        return weight;
    }

    public void setWeight(BigDecimal weight) {
        this.weight = weight;
    }


    public boolean isStatus() {
        return status;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }
}
