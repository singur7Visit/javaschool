package ru.tsystems.logiweb.dto;

import ru.tsystems.logiweb.entities.Driver;

import javax.validation.constraints.Size;

/**
 * DTO for entity
 * @see ru.tsystems.logiweb.entities.Driver
 */
public class DriverDTO {

    public DriverDTO() {}

    public DriverDTO(Driver driver) {
        this.id = driver.getId();
        this.licence = driver.getLicence();
        this.SNM = driver.getSNM();
        this.status = new StatusDriverDTO(driver.getStatus());
    }

    private long id;

    @Size(min = 11, max = 11, message = "Licence is not valid (11 numbers)")
    private String licence;

    @Size(max = 128)
    private String SNM;

    private StatusDriverDTO status;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLicence() {
        return licence;
    }

    public void setLicence(String licence) {
        this.licence = licence;
    }

    public String getSNM() {
        return SNM;
    }

    public void setSNM(String SNM) {
        this.SNM = SNM;
    }

    public StatusDriverDTO getStatus() {
        return status;
    }

    public void setStatus(StatusDriverDTO status) {
        this.status = status;
    }
}
