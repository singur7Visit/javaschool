package ru.tsystems.logiweb.dto.response;

/**
 * Created by dsing on 15.09.14.
 */
public class SuccessResponseRestMessage extends ResponseRestMessage<SuccessResponseRestMessage> {
    public SuccessResponseRestMessage() {
        setType(SUCCESS);
    }
}
