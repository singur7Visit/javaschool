package ru.tsystems.logiweb.dto;

import ru.tsystems.logiweb.entities.Truck;

import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;

/**
 * DTO for entity
 * @see ru.tsystems.logiweb.entities.Truck
 */
public class TruckDTO {
    private long id;

    public TruckDTO() {}

    public TruckDTO(Truck truck) {
        this.id = truck.getId();
        this.classCapacity = new ClassCapacityDTO(truck.getClassCapacity());
        this.countDrivers = truck.getCountDrivers();
        this.status = new StatusTruckDTO(truck.getStatus());
        this.regNumber = truck.getRegNumber();
    }

    @Pattern(regexp = "[A-Za-z]{2}[0-9]{5}", message = "Number is not valid (2 letter + 5 number)")
    private String regNumber;

    @Min(value = 1, message = "Count drivers must be > 0")
    private int countDrivers;

    private ClassCapacityDTO classCapacity;

    private StatusTruckDTO status;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRegNumber() {
        return regNumber;
    }

    public void setRegNumber(String regNumber) {
        this.regNumber = regNumber;
    }

    public int getCountDrivers() {
        return countDrivers;
    }

    public void setCountDrivers(int countDrivers) {
        this.countDrivers = countDrivers;
    }

    public ClassCapacityDTO getClassCapacity() {
        return classCapacity;
    }

    public void setClassCapacity(ClassCapacityDTO classCapacity) {
        this.classCapacity = classCapacity;
    }

    public StatusTruckDTO getStatus() {
        return status;
    }

    public void setStatus(StatusTruckDTO status) {
        this.status = status;
    }
}
