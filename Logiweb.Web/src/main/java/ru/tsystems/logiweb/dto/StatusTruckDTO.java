package ru.tsystems.logiweb.dto;

import com.fasterxml.jackson.annotation.JsonValue;
import ru.tsystems.logiweb.entities.StatusTruck;

/**
 * DTO for entity
 * @see ru.tsystems.logiweb.entities.StatusTruck
 */
public class StatusTruckDTO {
    private String name;

    public StatusTruckDTO(){}

    public StatusTruckDTO(StatusTruck statusTruck) {
        this.name = statusTruck.getName();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @JsonValue
    public String toString() {
        return name;
    }
}
