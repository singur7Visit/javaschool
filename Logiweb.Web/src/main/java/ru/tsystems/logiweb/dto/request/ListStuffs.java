package ru.tsystems.logiweb.dto.request;

import ru.tsystems.logiweb.dto.StuffDTO;
import ru.tsystems.logiweb.entities.Stuff;

import java.util.ArrayList;

/**
 * Created by dsing on 18.09.14.
 */
public class ListStuffs extends ArrayList<Stuff> {
    private static final long serialVersionUID = 1L;

    public ListStuffs() { super(); }
}
