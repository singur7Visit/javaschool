package ru.tsystems.logiweb.dto;

import ru.tsystems.logiweb.entities.Driver;
import ru.tsystems.logiweb.entities.Order;
import ru.tsystems.logiweb.entities.Stuff;

import java.util.ArrayList;
import java.util.List;

/**
 * DTO for entity
 * @see ru.tsystems.logiweb.entities.Order
 */
public class OrderDTO {

    public OrderDTO(){}

    public OrderDTO(Order order) {
        this.id = order.getId();
        this.numOrder = order.getNumOrder();
        this.status = new StatusOrderDTO(order.getStatus());
        this.drivers = new ArrayList<>();
        for(Driver driver : order.getDrivers())
            this.drivers.add(new DriverDTO(driver));
        this.truck = order.getTruck() == null ? null : new TruckDTO(order.getTruck());
        this.stuffs = new ArrayList<>();
        for(Stuff stuff : order.getStuffs())
            this.stuffs.add(new StuffDTO(stuff));
     }

    private long id;

    private Long numOrder;

    private StatusOrderDTO status;

    private List<DriverDTO> drivers;

    private TruckDTO truck;

    private List<StuffDTO> stuffs;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Long getNumOrder() {
        return numOrder;
    }

    public void setNumOrder(Long numOrder) {
        this.numOrder = numOrder;
    }

    public StatusOrderDTO getStatus() {
        return status;
    }

    public void setStatus(StatusOrderDTO status) {
        this.status = status;
    }

    public List<DriverDTO> getDrivers() {
        return drivers;
    }

    public void setDrivers(List<DriverDTO> drivers) {
        this.drivers = drivers;
    }

    public TruckDTO getTruck() {
        return truck;
    }

    public void setTruck(TruckDTO truck) {
        this.truck = truck;
    }

    public List<StuffDTO> getStuffs() {
        return stuffs;
    }

    public void setStuffs(List<StuffDTO> stuffs) {
        this.stuffs = stuffs;
    }
}
