package ru.tsystems.logiweb.dto.response;

/**
 * Created by dsing on 15.09.14.
 */
@SuppressWarnings("unchecked")
public class ResponseRestMessage<T> {

    public static final int SUCCESS = 0;
    public static final int ERROR = 1;

    private int type;

    private String message;

    public int getType() {
        return type;
    }

    public T setType(int type) {
        this.type = type;
        return (T) this;
    }

    public String getMessage() {
        return message;
    }

    public T setMessage(String message) {
        this.message = message;
        return (T) this;
    }
}
