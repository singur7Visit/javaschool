package ru.tsystems.logiweb.dto.response;

/**
 * Created by dsing on 15.09.14.
 */
public class ErrorResponseRestMessage extends ResponseRestMessage<ErrorResponseRestMessage>{
    public ErrorResponseRestMessage() {
        setType(ERROR);
    }
}
