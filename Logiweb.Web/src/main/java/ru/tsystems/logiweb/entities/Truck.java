package ru.tsystems.logiweb.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.Fetch;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.Pattern;
import java.util.List;

/**
 * Created by dsing on 14.08.14.
 */

@Entity
@Table(name = "trucks")
public class Truck {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private long id;

    @Column(name = "regnumber")
    private String regNumber;

    @Column(name = "count_drivers")
    private int countDrivers;

    @ManyToOne
    @JoinColumn(name = "id_class")
    private ClassCapacity classCapacity;

    @OneToMany(mappedBy = "truck", fetch = FetchType.LAZY)
    private List<Driver> drivers;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id_order")
    private Order order;

    @OneToOne
    @JoinColumn(name = "id_status")
    private StatusTruck status;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getRegNumber() {
        return regNumber;
    }

    public void setRegNumber(String regNumber) {
        this.regNumber = regNumber;
    }

    public int getCountDrivers() {
        return countDrivers;
    }

    public void setCountDrivers(int countDrivers) {
        this.countDrivers = countDrivers;
    }

    public ClassCapacity getClassCapacity() {
        return classCapacity;
    }

    public void setClassCapacity(ClassCapacity classCapacity) {
        this.classCapacity = classCapacity;
    }

    public List<Driver> getDrivers() {
        return drivers;
    }

    public void setDrivers(List<Driver> drivers) {
        this.drivers = drivers;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }

    public StatusTruck getStatus() {
        return status;
    }

    public void setStatus(StatusTruck status) {
        this.status = status;
    }
}
