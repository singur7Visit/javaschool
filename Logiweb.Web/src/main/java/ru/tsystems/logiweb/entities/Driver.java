package ru.tsystems.logiweb.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;

import javax.persistence.*;
import javax.validation.constraints.Size;

/**
 * Created by dsing on 15.08.14.
 */

@Entity
@Table(name = "drivers")
public class Driver {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private long id;

    @Column(name = "driving_licence")
    private String licence;

    @Column(name = "SNM")
    private String SNM;

    @OneToOne
    @JoinColumn(name = "id_status")
    private StatusDriver status;

    @ManyToOne
    @JoinColumn(name = "id_truck")
    private Truck truck;

    @ManyToOne
    @JoinColumn(name = "id_order")
    private Order order;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getLicence() {
        return licence;
    }

    public void setLicence(String licence) {
        this.licence = licence;
    }

    public String getSNM() {
        return SNM;
    }

    public void setSNM(String SNM) {
        this.SNM = SNM;
    }

    public StatusDriver getStatus() {
        return status;
    }

    public void setStatus(StatusDriver status) {
        this.status = status;
    }

    public Truck getTruck() {
        return truck;
    }

    public void setTruck(Truck truck) {
        this.truck = truck;
    }

    public Order getOrder() {
        return order;
    }

    public void setOrder(Order order) {
        this.order = order;
    }
}