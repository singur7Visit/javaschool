package ru.tsystems.logiweb.service.exceptions.orders;

/**
 * Created by dsing on 25.09.14.
 */
public class OrderStatusDriverException extends RuntimeException {

    private final Long id;

    public OrderStatusDriverException(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }
}
