package ru.tsystems.logiweb.service.exceptions.drivers;

/**
 * Created by dsing on 16.09.14.
 */
public class DriverBusyException extends RuntimeException {

    private final Long id;

    public DriverBusyException(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }
}
