package ru.tsystems.logiweb.service.exceptions.drivers;

/**
 * Created by dsing on 16.09.14.
 */
public class DriverNotFoundException extends RuntimeException {
    private final Long id;
    private final String licence;

    public DriverNotFoundException(Long id) {
        this.id = id;
        licence = null;
    }

    public DriverNotFoundException(String licence) {
        this.licence = licence;
        id = 0L;
    }

    public Long getId() {
        return id;
    }

    public String getLicence() {
        return licence;
    }
}
