package ru.tsystems.logiweb.service.exceptions.trucks;

/**
 * Created by dsing on 15.09.14.
 */
public class NotUniqueTruckException extends RuntimeException {

    private final String regNumber;

    public NotUniqueTruckException(String regNumber) {
        this.regNumber = regNumber;
    }

    public String getRegNumber() {
        return regNumber;
    }
}
