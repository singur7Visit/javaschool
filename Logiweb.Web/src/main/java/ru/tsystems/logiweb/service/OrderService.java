package ru.tsystems.logiweb.service;


import ru.tsystems.logiweb.entities.Driver;
import ru.tsystems.logiweb.entities.Order;
import ru.tsystems.logiweb.entities.Stuff;
import ru.tsystems.logiweb.entities.Truck;

import java.util.List;

/**
 * Created by dsing on 22.08.14.
 */
public interface OrderService {
    Order createOrder();
    Order confirmOrder(long id,List<Stuff> stuffs);
    Order shipOrder(long id, long[] idDrivers, long idTruck);
    boolean completeOrder(Long number);
    Order closeOrder(long id);
    boolean removeOrder(long id);
    List<Order> getAllOrders();
    Order getOrderById(long id);
    boolean deliveredStuff(long id);
}
