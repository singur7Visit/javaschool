package ru.tsystems.logiweb.service.exceptions.orders;

import ru.tsystems.logiweb.entities.Driver;
import ru.tsystems.logiweb.entities.Truck;

import java.util.List;

/**
 * Created by dsing on 19.09.14.
 */
public class OrderShipException extends RuntimeException {

    private final Long id;
    private final Truck truck;
    private final List<Driver> drivers;

    public OrderShipException(Long id, List<Driver> drivers, Truck truck) {
        this.id = id;
        this.truck = truck;
        this.drivers = drivers;
    }

    public Long getId() {
        return id;
    }

    public Truck getTruck() {
        return truck;
    }

    public List<Driver> getDrivers() {
        return drivers;
    }
}
