package ru.tsystems.logiweb.service.exceptions.orders;

/**
 * Created by dsing on 16.09.14.
 */
public class OrderNotUniqueException extends RuntimeException {
    private final Long number;

    public OrderNotUniqueException(Long number) {
        this.number = number;
    }

    public Long getNumber() {
        return number;
    }
}
