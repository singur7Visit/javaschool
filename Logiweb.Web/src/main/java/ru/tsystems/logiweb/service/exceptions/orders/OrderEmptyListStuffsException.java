package ru.tsystems.logiweb.service.exceptions.orders;

/**
 * Created by dsing on 16.09.14.
 */
public class OrderEmptyListStuffsException extends RuntimeException {
    private final Long id;

    public OrderEmptyListStuffsException(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }
}
