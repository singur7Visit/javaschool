package ru.tsystems.logiweb.service;


import ru.tsystems.logiweb.entities.ClassCapacity;
import ru.tsystems.logiweb.entities.Truck;

import java.util.List;

/**
 * Created by dsing on 21.08.14.
 */
public interface TruckService {
    Long createTruck(Truck driver);
    boolean removeTruck(Long id);
    boolean isUniqueTruck(String regNumber);
    List<Truck> searchTrucks(int from, int to, String searchNumber);
    List<Truck> getAllTrucks();
    List<ClassCapacity> getClasses();
    List<Truck> getAvailableTrucks();
}
