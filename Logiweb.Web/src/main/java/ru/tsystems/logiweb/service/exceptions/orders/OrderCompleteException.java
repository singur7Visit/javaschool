package ru.tsystems.logiweb.service.exceptions.orders;

/**
 * Created by dsing on 25.09.14.
 */
public class OrderCompleteException extends RuntimeException {

    private final Long number;

    public OrderCompleteException(Long number) {
        this.number = number;
    }

    public Long getNumber() {
        return number;
    }
}
