package ru.tsystems.logiweb.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsystems.logiweb.dao.UserDao;

import java.util.ArrayList;
import java.util.Collection;

/**
 * Created by dsing on 04.09.14.
 */
@Service
@Transactional
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserDao userDao;

    @Override
    public UserDetails loadUserByUsername(String login) throws UsernameNotFoundException {
        try {
            ru.tsystems.logiweb.entities.User user = userDao.findByLogin(login);
            Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority>();
            String role;
            if (user.getRole().isManageMangers())
                role = "ROLE_ADMIN";
            else if (user.getRole().isManageDrivers())
                role = "ROLE_MANAGER";
            else if (user.getRole().isManageOrders())
                role = "ROLE_DRIVER";
            else
                role = "ROLE_ANONYMOUS";
            authorities.add(new SimpleGrantedAuthority(role));

            return new User(user.getLogin(), user.getPassword(), true,
                    true, true, true, authorities);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
