package ru.tsystems.logiweb.service.exceptions.drivers;

/**
 * Created by dsing on 16.09.14.
 */
public class DriverStatusException extends RuntimeException {
    private final String drivingLicence;

    public DriverStatusException(String drivingLicence) {
        this.drivingLicence = drivingLicence;
    }


    public String getDrivingLicence() {
        return drivingLicence;
    }
}
