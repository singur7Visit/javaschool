package ru.tsystems.logiweb.service.exceptions.drivers;

/**
 * Created by dsing on 16.09.14.
 */
public class NotUniqueDriverException extends RuntimeException {
    private final String drivingLicence;

    public NotUniqueDriverException(String drivingLicence) {
        this.drivingLicence = drivingLicence;
    }


    public String getDrivingLicence() {
        return drivingLicence;
    }
}
