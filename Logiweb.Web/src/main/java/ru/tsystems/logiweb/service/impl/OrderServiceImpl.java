package ru.tsystems.logiweb.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsystems.logiweb.dao.DriverDao;
import ru.tsystems.logiweb.dao.OrderDao;
import ru.tsystems.logiweb.dao.TruckDao;
import ru.tsystems.logiweb.entities.Driver;
import ru.tsystems.logiweb.entities.Order;
import ru.tsystems.logiweb.entities.Stuff;
import ru.tsystems.logiweb.entities.Truck;
import ru.tsystems.logiweb.service.OrderService;
import ru.tsystems.logiweb.service.exceptions.orders.*;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;

/**
 * Created by dsing on 22.08.14.
 */
@Service
public class OrderServiceImpl implements OrderService {

    @Autowired
    private OrderDao orderDao;

    @Autowired
    private TruckDao truckDao;

    @Autowired
    private DriverDao driverDao;

    @Override
    @Transactional(rollbackFor = OrderNotUniqueException.class)
    public Order createOrder() {
        Order order = new Order();
        orderDao.createOrder(order);
        order.setNumOrder(order.getId());
        order.setStatus(orderDao.findStatusByName("created"));
        return orderDao.updateOrder(order);
    }

    @Override
    @Transactional(rollbackFor = {OrderNotFoundException.class,OrderEmptyListStuffsException.class})
    public Order confirmOrder(long id, List<Stuff> stuffs) {
        Order order = orderDao.getOrderById(id);
        if(order == null)
            throw new OrderNotFoundException(id);
        if(stuffs.isEmpty())
            throw new OrderEmptyListStuffsException(id);
        order.setStuffs(new ArrayList<Stuff>());
        for(Stuff stuff : stuffs) {
            stuff.setOrder(order);
            orderDao.updateStuff(stuff);
            order.getStuffs().add(stuff);
        }
        order.setStatus(orderDao.findStatusByName("confirmed"));
        return orderDao.updateOrder(order);
    }

    @Override
    @Transactional(rollbackFor = {OrderNotFoundException.class,OrderShipException.class, OrderShipCountDriversException.class})
    public Order shipOrder(long id,long[] idDrivers, long idTruck) {
        Order order = orderDao.getOrderById(id);
        if(order == null)
            throw new OrderNotFoundException(id);
        Truck truck = truckDao.getById(idTruck);
        List<Driver> drivers = driverDao.getDriversByIds(idDrivers);
        if(truck == null|| drivers == null || drivers.isEmpty())
            throw new OrderShipException(id, drivers, truck);
        if(truck.getCountDrivers() != drivers.size())
            throw new OrderShipCountDriversException(truck.getCountDrivers(),drivers.size());
        order.setStatus(orderDao.findStatusByName("shipped"));
        for(Driver driver : drivers) {
            driver.setStatus(driverDao.findStatusByName("busy"));
            driver.setOrder(order);
            driver.setTruck(truck);
            driverDao.updateDriver(driver);
        }
        for(Stuff stuff : order.getStuffs()) {
            stuff.setOrder(order);
            orderDao.updateStuff(stuff);
        }
        truck.setStatus(truckDao.findStatusByName("busy"));
        truck.setOrder(order);
        order.setTruck(truckDao.updateTruck(truck));
        return orderDao.updateOrder(order);
    }

    @Override
    @Transactional(rollbackFor = {OrderCompleteException.class})
    public boolean completeOrder(Long number) {
        Order order = orderDao.findByNumber(number);
        if(!isValidOrder(order)) {
            throw new OrderCompleteException(number);
        }
        for(Stuff stuff : order.getStuffs()) {
            stuff.setStatus(true);
            orderDao.updateStuff(stuff);
        }
        order.setStatus(orderDao.findStatusByName("completed"));
        orderDao.updateOrder(order);
        return true;
    }

    public boolean isValidOrder(Order order) {
        return !(order.getTruck() == null || order.getDrivers() == null
                || order.getDrivers().isEmpty() || order.getStuffs() == null);
    }

    @Override
    @Transactional(rollbackFor = {OrderStatusDriverException.class})
    public Order closeOrder(long id) {
        Order order = orderDao.getOrderById(id);
        for(Driver driver : order.getDrivers()) {
            if(!isValidDriverStatus(driver)) {
                throw new OrderStatusDriverException(id);
            }
            else {
                driver.setStatus(driverDao.findStatusByName("free"));
                driver.setOrder(null);
                driver.setTruck(null);
                driverDao.updateDriver(driver);
            }
        }
        order.getDrivers().clear();
        order.getTruck().setStatus(truckDao.findStatusByName("free"));
        order.getTruck().setOrder(null);
        order.getTruck().setDrivers(null);
        truckDao.updateTruck(order.getTruck());
        order.setStatus(orderDao.findStatusByName("closed"));
        return orderDao.updateOrder(order);
    }

    public boolean isValidDriverStatus(Driver driver) {
        return !"driving".equals(driver.getStatus().getName());
    }

    @Override
    @Transactional(rollbackFor = OrderNotFoundException.class)
    public boolean removeOrder(long id) {
        Order order = orderDao.getOrderById(id);
        if(order == null)
            throw new OrderNotFoundException(id);
        if(order.getStuffs()!= null) {
            CopyOnWriteArrayList<Stuff> copyOnWriteArrayList = new CopyOnWriteArrayList<>(order.getStuffs());
            for(Stuff stuff : copyOnWriteArrayList) {
                orderDao.removeStuff(stuff);
            }
        }
        if(order.getTruck() != null) {
            order.getTruck().setStatus(truckDao.findStatusByName("free"));
            order.getTruck().setDrivers(null);
            order.getTruck().setOrder(null);
            truckDao.updateTruck(order.getTruck());
            order.setTruck(null);
        }
        if(order.getDrivers() != null) {
            for(Driver driver : order.getDrivers()) {
                driver.setTruck(null);
                driver.setStatus(driverDao.findStatusByName("free"));
                driver.setTruck(null);
                driverDao.updateDriver(driver);
            }
            order.getDrivers().clear();
        }
        orderDao.removeOrder(order);
        return true;
    }

    @Override
    @Transactional
    public List<Order> getAllOrders() {
        return orderDao.getAllOrder();
    }

    @Override
    @Transactional
    public Order getOrderById(long id) {
        return orderDao.getOrderById(id);
    }

    @Override
    @Transactional
    public boolean deliveredStuff(long id) {
        Stuff stuff = orderDao.getStuffById(id);
        stuff.setStatus(true);
        orderDao.updateStuff(stuff);
        return true;
    }
}
