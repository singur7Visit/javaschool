package ru.tsystems.logiweb.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsystems.logiweb.dao.UserDao;
import ru.tsystems.logiweb.entities.User;
import ru.tsystems.logiweb.service.UserService;

/**
 * Created by dsing on 20.08.14.
 */
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;

    @Override
    public User login(String login, String password) {
        return userDao.getUserByLoginPassword(login, password);
    }

    @Override
    @Transactional(rollbackFor = Exception.class)
    public User create(User user) {
        User old = userDao.findByLogin(user.getLogin());
        if(old != null)
            throw new RuntimeException("Cann't create user. User already exists");
        userDao.createUser(user);
        return user;
    }
}
