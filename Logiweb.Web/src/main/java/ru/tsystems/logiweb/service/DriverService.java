package ru.tsystems.logiweb.service;


import ru.tsystems.logiweb.entities.Driver;
import ru.tsystems.logiweb.entities.StatusDriver;

import java.util.List;

/**
 * Created by dsing on 20.08.14.
 */
public interface DriverService {
    Long createDriver(Driver driver);
    List<Driver> getAllDrivers();
    List<Driver> getCoDrivers(Driver driver);
    Driver findByLicence(String licence);
    List<Driver> getAvailableDrivers();
    boolean setStatus(String licence,StatusDriver statusDriver);
    boolean removeDriver(long id);
}
