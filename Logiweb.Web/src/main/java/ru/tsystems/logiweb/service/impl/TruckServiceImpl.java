package ru.tsystems.logiweb.service.impl;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsystems.logiweb.dao.TruckDao;
import ru.tsystems.logiweb.entities.ClassCapacity;
import ru.tsystems.logiweb.entities.Truck;
import ru.tsystems.logiweb.service.TruckService;
import ru.tsystems.logiweb.service.exceptions.trucks.TruckBusyException;
import ru.tsystems.logiweb.service.exceptions.trucks.NotUniqueTruckException;
import ru.tsystems.logiweb.service.exceptions.trucks.TruckNotFoundException;

import java.util.List;

/**
 * Created by dsing on 21.08.14.
 */

@Service
public class TruckServiceImpl implements TruckService {

    @Autowired
    private TruckDao truckDao;

    @Override
    @Transactional(rollbackFor=NotUniqueTruckException.class)
    public Long createTruck(Truck truck) {
        if(!isUniqueTruck(truck.getRegNumber()))
            throw new NotUniqueTruckException(truck.getRegNumber());
        truck.setClassCapacity(truckDao.findClassByName(truck.getClassCapacity().getName()));
        truck.setStatus(truckDao.findStatusByName("free"));
        Long id =  truckDao.createTruck(truck);
        return id;
    }


    @Override
    @Transactional(rollbackFor={TruckNotFoundException.class,TruckBusyException.class})
    public boolean removeTruck(Long id) {
        Truck foundTruck = truckDao.getById(id);
        if(foundTruck == null)
            throw new TruckNotFoundException(id);
        if(!foundTruck.getStatus().getName().equals("free"))
            throw new TruckBusyException(foundTruck.getId());
        truckDao.removeTruck(foundTruck);
        return true;
    }

    @Override
    public boolean isUniqueTruck(String regNumber) {
        return truckDao.findByNumber(regNumber) == null;
    }

    @Override
    public List<Truck> searchTrucks(int from, int to, String searchLicence) {
        return truckDao.getTrucksByLimitSearch(from, to, searchLicence);
    }

    @Override
    @Transactional
    public List<Truck> getAllTrucks() {
        return truckDao.getTrucks();
    }

    @Override
    @Transactional
    public List<ClassCapacity> getClasses() {
        return truckDao.getAllClassesCapacity();
    }

    @Override
    @Transactional
    public List<Truck> getAvailableTrucks() {
        return truckDao.getAvailableTruck();
    }
}
