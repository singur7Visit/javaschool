package ru.tsystems.logiweb.service.impl;

import org.hibernate.Hibernate;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsystems.logiweb.dao.DriverDao;
import ru.tsystems.logiweb.entities.Driver;
import ru.tsystems.logiweb.entities.StatusDriver;
import ru.tsystems.logiweb.service.DriverService;
import ru.tsystems.logiweb.service.exceptions.drivers.DriverBusyException;
import ru.tsystems.logiweb.service.exceptions.drivers.DriverNotFoundException;
import ru.tsystems.logiweb.service.exceptions.drivers.DriverStatusException;
import ru.tsystems.logiweb.service.exceptions.drivers.NotUniqueDriverException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by dsing on 20.08.14.
 */
@Service
public class DriverServiceImpl implements DriverService {

    @Autowired
    private DriverDao driverDao;

    @Override
    @Transactional(rollbackFor = NotUniqueDriverException.class)
    public Long createDriver(Driver driver) {
        if(driverDao.findByLicence(driver.getLicence()) != null)
            throw new NotUniqueDriverException(driver.getLicence());
        driver.setStatus(driverDao.findStatusByName("free"));
        Long id = driverDao.createDriver(driver);
        return id;
    }

    @Override
    @Transactional
    public List<Driver> getAllDrivers() {
        return driverDao.getAllDrivers();
    }

    @Override
    public List<Driver> getCoDrivers(Driver driver) {
        List<Driver> coDrivers = new ArrayList<>();
        List<Driver> allDrivers = driver.getOrder().getDrivers();
        for(Driver d : allDrivers)
            if(!driver.getLicence().equals(d.getLicence()))
                coDrivers.add(d);
        return coDrivers;
    }

    @Override
    @Transactional
    public Driver findByLicence(String licence) {
        return driverDao.findByLicence(licence);
    }

    @Override
    @Transactional
    public List<Driver> getAvailableDrivers() {
        return driverDao.getAvailableDrivers();
    }

    @Override
    @Transactional(rollbackFor = {DriverNotFoundException.class,DriverStatusException.class})
    public boolean setStatus(String licence,StatusDriver statusDriver) {
        Driver driver = driverDao.findByLicence(licence);
        if(driver == null)
            throw new DriverNotFoundException(licence);
        List<Driver> coDrivers = driver.getOrder().getDrivers();
        for(Driver d : coDrivers) {
            if(d.getStatus().getName().equals("driving")
                    && statusDriver.getName().equals("driving")
                    && !driver.getLicence().equals(d.getLicence()))
                throw new DriverStatusException(licence);
        }
        driver.setStatus(driverDao.findStatusByName(statusDriver.getName()));
        driverDao.updateDriver(driver);
        return true;
    }

    @Override
    @Transactional(rollbackFor = {DriverNotFoundException.class})
    public boolean removeDriver(long id) {
        Driver foundDriver = driverDao.findById(id);
        if(foundDriver == null)
            throw new DriverNotFoundException(id);
        else if(!foundDriver.getStatus().getName().equals("free"))
            throw new DriverBusyException(id);
        driverDao.removeDriver(foundDriver);
        return true;
    }

}
