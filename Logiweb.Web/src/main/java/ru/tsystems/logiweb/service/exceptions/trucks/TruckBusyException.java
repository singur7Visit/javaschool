package ru.tsystems.logiweb.service.exceptions.trucks;

/**
 * Created by dsing on 15.09.14.
 */
public class TruckBusyException extends RuntimeException {

    private final Long id;

    public TruckBusyException(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }
}
