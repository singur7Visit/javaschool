package ru.tsystems.logiweb.service.exceptions.orders;

/**
 * Created by dsing on 19.09.14.
 */
public class OrderShipCountDriversException extends RuntimeException {

    private final int countTruckDrivers;
    private final int countDrivers;

    public OrderShipCountDriversException(int countTruckDrivers, int countDrivers) {
        this.countTruckDrivers = countTruckDrivers;
        this.countDrivers = countDrivers;
    }

    public int getCountTruckDrivers() {
        return countTruckDrivers;
    }

    public int getCountDrivers() {
        return countDrivers;
    }
}
