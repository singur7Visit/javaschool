package ru.tsystems.logiweb.dao;


import ru.tsystems.logiweb.entities.Role;
import ru.tsystems.logiweb.entities.User;

/**
 * Created by dsing on 18.08.14.
 */
public interface UserDao {
    User getUserByLoginPassword(String login, String password);
    void createUser(User user);
    void removeUser(User user);
    User findByLogin(String login);
    Role getRoleByName(String name);
}
