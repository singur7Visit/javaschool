package ru.tsystems.logiweb.dao.hiberimpl;

import org.springframework.stereotype.Repository;
import ru.tsystems.logiweb.dao.DriverDao;
import ru.tsystems.logiweb.entities.Driver;
import ru.tsystems.logiweb.entities.StatusDriver;

import java.util.List;

/**
 * Created by dsing on 19.08.14.
 */
@Repository
public class DriverDaoImpl extends AbstractDao implements DriverDao {

    @Override
    public List<Driver> getDriversByLimit(final int from, final int to) {
        return getSession().createQuery("FROM Driver")
                .setFirstResult(from)
                .setMaxResults(to)
                .list();
    }

    @Override
    public List<Driver> getDriversByLimitSearch(final int from, final int to, final String search) {
        return getSession().createQuery("FROM Driver WHERE licence LIKE :licence")
                        .setParameter("licence","%" + search + "%")
                        .setFirstResult(from)
                        .setMaxResults(to)
                        .list();
    }

    @Override
    public Long createDriver(Driver driver) {
        return (Long) getSession().save(driver);
    }

    @Override
    public void removeDriver(Driver driver) {
        getSession().delete(driver);
    }

    @Override
    public Driver updateDriver(Driver driver) {
        return (Driver) getSession().merge(driver);
    }

    @Override
    public Driver findByLicence(String licence) {
        return getSingleResult(getSession().createQuery("FROM Driver WHERE licence = :licence")
                .setParameter("licence", licence)
                .list(), Driver.class);
    }

    @Override
    public Driver findById(Long id) {
        return (Driver) getSession().get(Driver.class, id);
    }

    @Override
    public StatusDriver findStatusByName(String nameStatus) {
        return getSingleResult(getSession().createQuery("FROM StatusDriver WHERE name = :name")
                .setParameter("name",nameStatus)
                .list(),StatusDriver.class);
    }

    @Override
    public List<Driver> getAvailableDrivers() {
        return getSession().createSQLQuery("SELECT {driver.*} FROM drivers as driver WHERE driver.id_status = " +
                "(SELECT id from statuses_drivers WHERE name = 'free')").addEntity("driver", Driver.class).list();
    }

    @Override
    public List<Driver> getDriversByIds(long[] ids) {
        StringBuilder stringBuilder = new StringBuilder();
        for(long id : ids) {
            stringBuilder.append(id).append(',');
        }
        int index = stringBuilder.lastIndexOf(",");
        if(index != -1)
            stringBuilder.replace(index,index + 1, "");
        return getSession().createSQLQuery("SELECT {driver.*} FROM drivers as driver WHERE driver.id IN " +
                "(" + (index == -1 ? null : stringBuilder.toString() ) + ")")
                .addEntity("driver",Driver.class).list();
    }

    @Override
    public List<Driver> getAllDrivers() {
        return getSession().createQuery("FROM Driver").list();
    }

}
