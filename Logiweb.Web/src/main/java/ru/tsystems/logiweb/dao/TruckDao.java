package ru.tsystems.logiweb.dao;


import ru.tsystems.logiweb.entities.ClassCapacity;
import ru.tsystems.logiweb.entities.StatusTruck;
import ru.tsystems.logiweb.entities.Truck;

import java.util.List;

/**
 * Created by dsing on 18.08.14.
 */
public interface TruckDao {
    List<Truck> getTrucksByLimit(int from, int to);

    List<Truck> getTrucks();

    List<Truck> getTrucksByLimitSearch(int from, int to, String search);

    Long createTruck(Truck truck);

    void removeTruck(Truck truck);

    Truck findByNumber(String number);

    Truck getById(long id);

    Truck updateTruck(Truck truck);

    StatusTruck findStatusByName(String nameStatus);

    List<ClassCapacity> getAllClassesCapacity();

    ClassCapacity findClassByName(String name);

    List<Truck> getAvailableTruck();
}
