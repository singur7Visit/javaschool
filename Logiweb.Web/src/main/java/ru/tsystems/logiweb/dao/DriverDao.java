package ru.tsystems.logiweb.dao;

import ru.tsystems.logiweb.entities.Driver;
import ru.tsystems.logiweb.entities.StatusDriver;

import java.util.List;

/**
 * Created by dsing on 19.08.14.
 */
public interface DriverDao {
    List<Driver> getDriversByLimit(int from, int to);

    List<Driver> getDriversByLimitSearch(int from, int to, String search);

    Long createDriver(Driver driver);

    void removeDriver(Driver driver);

    Driver updateDriver(Driver driver);

    Driver findByLicence(String licence);

    Driver findById(Long id);

    StatusDriver findStatusByName(String nameStatus);

    List<Driver> getAvailableDrivers();

    List<Driver> getDriversByIds(long[] ids);

    List<Driver> getAllDrivers();
}
