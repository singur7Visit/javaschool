package ru.tsystems.logiweb.dao.hiberimpl;


import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.tsystems.logiweb.dao.TruckDao;
import ru.tsystems.logiweb.entities.ClassCapacity;
import ru.tsystems.logiweb.entities.StatusTruck;
import ru.tsystems.logiweb.entities.Truck;

import java.util.List;

/**
 * Created by dsing on 18.08.14.
 */
@Repository
public class TruckDaoImpl extends AbstractDao implements TruckDao {

    @Override
    public List<Truck> getTrucksByLimit(int from, int to) {
        return getSession().createQuery("FROM Truck")
                .setFirstResult(from)
                .setMaxResults(to)
                .list();
    }

    @Override
    public List<Truck> getTrucks() {
        return getSession().createQuery("FROM Truck").list();
    }

    @Override
    public List<Truck> getTrucksByLimitSearch(int from, int to, String search) {
        return getSession().createQuery("FROM Truck WHERE regNumber LIKE :number")
                .setParameter("number","%" + search + "%")
                .setFirstResult(from)
                .setMaxResults(to)
                .list();
    }

    @Override
    public Long createTruck(Truck truck) {
        return (Long) getSession().save(truck);
    }

    @Override
    public void removeTruck(Truck truck) {
        getSession().delete(truck);
    }

    @Override
    public Truck findByNumber(String number) {
        return getSingleResult(getSession().createQuery("FROM Truck WHERE regNumber = :number")
                .setParameter("number", number)
                .list(), Truck.class);
    }

    @Override
    public Truck getById(long id) {
        return (Truck) getSession().get(Truck.class,id);
    }

    @Override
    public Truck updateTruck(Truck truck) {
        return (Truck) getSession().merge(truck);
    }

    @Override
    public StatusTruck findStatusByName(String nameStatus) {
        return getSingleResult(getSession().createQuery("FROM StatusTruck WHERE name=:name")
                .setParameter("name", nameStatus)
                .list(), StatusTruck.class);
    }

    @Override
    public List<ClassCapacity> getAllClassesCapacity() {
        return getSession().createQuery("FROM ClassCapacity")
                .list();
    }

    @Override
    public ClassCapacity findClassByName(String name) {
        return getSingleResult(getSession().createQuery("FROM ClassCapacity WHERE name=:name")
                .setParameter("name", name).list(), ClassCapacity.class);
    }

    @Override
    public List<Truck> getAvailableTruck() {
        return getSession().createSQLQuery("SELECT {truck.*} FROM trucks as truck WHERE truck.id_status = " +
                "(SELECT id from statuses_trucks WHERE name = 'free')").addEntity("truck",Truck.class).list();
    }
}
