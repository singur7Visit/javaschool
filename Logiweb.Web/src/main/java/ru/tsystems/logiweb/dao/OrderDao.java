package ru.tsystems.logiweb.dao;

import ru.tsystems.logiweb.entities.Order;
import ru.tsystems.logiweb.entities.StatusOrder;
import ru.tsystems.logiweb.entities.Stuff;

import java.util.List;

/**
 * Created by dsing on 21.08.14.
 */
public interface OrderDao {

    void createOrder(Order order);

    Order updateOrder(Order order);

    void removeOrder(Order order);

    Order findByNumber(Long number);

    StatusOrder findStatusByName(String nameStatus);

    Order getOrderById(long id);

    void removeStuff(Stuff stuff);

    void updateStuff(Stuff stuff);

    Stuff getStuffById(long id);

    List<Order> getAllOrder();
}
