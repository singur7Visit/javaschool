package ru.tsystems.logiweb.dao.hiberimpl;


import org.springframework.stereotype.Repository;
import ru.tsystems.logiweb.dao.OrderDao;
import ru.tsystems.logiweb.entities.Order;
import ru.tsystems.logiweb.entities.StatusOrder;
import ru.tsystems.logiweb.entities.Stuff;

import java.util.List;

/**
 * Created by dsing on 22.08.14.
 */
@Repository
public class OrderDaoImpl extends AbstractDao implements OrderDao {

    @Override
    public void createOrder(Order order) {
        getSession().persist(order);
    }

    @Override
    public Order updateOrder(Order order) {
        return (Order) getSession().merge(order);
    }

    @Override
    public void removeOrder(Order order) {
        getSession().delete(order);
    }

    @Override
    public Order findByNumber(Long number) {
        return getSingleResult(getSession().createQuery("FROM Order WHERE numOrder = :number")
                .setParameter("number", number)
                .list(),Order.class);
    }

    @Override
    public StatusOrder findStatusByName(String nameStatus) {
        return getSingleResult(getSession().createQuery("FROM StatusOrder WHERE name = :name")
                .setParameter("name", nameStatus)
                .list(), StatusOrder.class);
    }

    @Override
    public Order getOrderById(long id) {
        getSession().clear();
        return (Order) getSession().get(Order.class,id);
    }

    @Override
    public void removeStuff(Stuff stuff) {
        getSession().delete(stuff);
    }

    @Override
    public void updateStuff(Stuff stuff) {
        getSession().merge(stuff);
    }

    @Override
    public Stuff getStuffById(long id) {
        return (Stuff) getSession().get(Stuff.class,id);
    }

    @Override
    public List<Order> getAllOrder() {
        getSession().clear();
        return getSession().createQuery("FROM Order").list();
    }
}
