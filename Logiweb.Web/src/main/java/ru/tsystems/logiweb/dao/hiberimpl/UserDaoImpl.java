package ru.tsystems.logiweb.dao.hiberimpl;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import ru.tsystems.logiweb.dao.UserDao;
import ru.tsystems.logiweb.entities.Role;
import ru.tsystems.logiweb.entities.User;

import java.util.List;

/**
 * Created by dsing on 18.08.14.
 */
@Repository
public class UserDaoImpl extends AbstractDao implements UserDao {

    @Override
    public User getUserByLoginPassword(final String login, final String password) {
        return getSingleResult(getSession().createQuery(
                "FROM User WHERE login = :login AND password = :password")
                .setParameter("login", login)
                .setParameter("password", password)
                .list(), User.class);
    }

    @Override
    public void createUser(User user) {
        getSession().persist(user);
    }

    @Override
    public void removeUser(User user) {
        getSession().delete(user);
    }

    @Override
    public User findByLogin(String login) {
        return getSingleResult(getSession().createQuery("FROM User WHERE login = :login")
                .setParameter("login", login)
                .list(),User.class);
    }

    @Override
    public Role getRoleByName(String name) {
        return getSingleResult(getSession().createQuery("FROM Role WHERE name = :name")
                .setParameter("name",name)
                .list(),Role.class);
    }


}
