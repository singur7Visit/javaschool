<%@ page language="java" contentType="text/html; charset=utf8"
         pageEncoding="utf8" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <title>Logiweb - Login</title>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap/bootstrap.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/login.css"/>
    <link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap/bootstrap-responsive.css"/>
    <script src="${pageContext.request.contextPath}/js/bootstrap.js"></script>
</head>

<body>
<div class="background-image"></div>
<div class="main_block">
    <div class="container login center-div">
        <div class="row ">
            <div class="center span4 well">
                <legend><span>Welcome to Logiweb</span></legend>
                <% if (request.getParameter("error") != null) { %>
                <div class="alert alert-danger">
                    <a class="close" data-dismiss="alert" href="#">×</a>Incorrect Login or Password!
                </div>
                <% } %>
                <form method="POST" autocomplete="off" action="<c:url value="/j_spring_security_check" />">
                    <input type="text" id="username" class="form-control" name="j_username" placeholder="Login"/>
                    <input type="password" id="password" class="form-control" name="j_password" placeholder="Password"/>
                    <div id="drivers">
                        <a href="${pageContext.request.contextPath}/driver/index">For drivers&gt&gt</a>
                    </div>
                    <div class="input-group">
                        <div class="checkbox">
                            <label>
                                <input type="checkbox" name="_spring_security_remember_me"/> Remember me
                            </label>
                        </div>
                    </div>
                    <button type="submit" name="submit" class="btn btn-primary btn-block">Sign in</button>
                </form>
            </div>
        </div>
    </div>
</div>
</body>
</html>