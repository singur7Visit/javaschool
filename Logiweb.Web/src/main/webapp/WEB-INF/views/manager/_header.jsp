
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="navbar nav-top">
    <div class="navbar-inner">
        <div class="collapse navbar-collapse navcentre" id="bs-example-navbar-collapse-1">
            <div>
                <img id="logo" src="${pageContext.request.contextPath}/images/logo.png">
                <label id="label_logo" for="logo">Logiweb</label>
            </div>
            <div>
                <ul id="main_menu" class="nav navbar-nav">
                    <li <c:if test="${activeMenu eq 'trucks'}">class="active"</c:if>><a href="${pageContext.request.contextPath}/manager/trucks">Trucks</a></li>
                    <li <c:if test="${activeMenu eq 'drivers'}">class="active"</c:if>><a href="${pageContext.request.contextPath}/manager/drivers">Drivers</a></li>
                    <li <c:if test="${activeMenu eq 'orders'}">class="active"</c:if>><a href="${pageContext.request.contextPath}/manager/orders">Orders</a></li>
                </ul>
            </div>
            <ul class="nav navbar-nav pull-right">
                <li id="fat-menu" class="dropdown">
                    <a href="" id="drop3" role="button" class="dropdown-toggle" data-toggle="dropdown">
                        <img id="user_logo" src="${pageContext.request.contextPath}/images/user_logo.png">
                        <span>${pageContext.request.userPrincipal.name}</span>
                        <b class="caret"></b>
                    </a>
                    <ul class="dropdown-menu" role="menu" aria-labelledby="drop3">
                        <li role="presentation"><a role="menuitem" tabindex="-1"
                                                   href="<c:url value="/logout" />">Logout</a></li>
                    </ul>
                </li>
            </ul>
        </div>
    </div>
</div>
