
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Manager - Trucks</title>
    <%@ include file="/WEB-INF/views/manager/_head.jsp" %>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/table.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/manager/trucks.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-ui.min.js"></script>
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/css/bootstrap/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/bootstrap/jquery-ui.min.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/trucks.css">
</head>
<body onload="createDataTableTruck()">
<c:set var="activeMenu" value="trucks"/>
<%@ include file="/WEB-INF/views/manager/_header.jsp" %>
<div class="hero-unit popin">
    <table id="trucks_table" class="display" cellspacing="0" width="100%">
        <button id="create" class="button-tools">New</button>
        <button id="remove" class="button-tools" disabled>Remove</button>
        <thead>
        <tr>
            <th>Register Number</th>
            <th>Count drivers</th>
            <th>Class capacity</th>
            <th>Status</th>
        </tr>
        </thead>
        <tbody id="trucks_body" class="table table-hover">

        </tbody>
    </table>
    <div id="dialog-form" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button id="closeCreateDialog" type="button" class="close">&times;</button>
                    <h4 class="modal-title">New Truck</h4>
                </div>
                <div class="modal-body">
                    <div id="block_error" class="alert alert-danger invisible">
                        <a id="close_message" class="close" href="#">×</a><span id="errorMessage"></span>
                    </div>
                    <form id="truck_form">
                        <fieldset>
                            <label for="reg_number" class="form-label">Register Number</label>
                            <input type="text" name="reg_number" id="reg_number"
                                   class="form-field text ui-widget-content ui-corner-all">
                            <label for="count_drivers" class="form-label">Count drivers</label>
                            <input type="text" name="count_drivers" id="count_drivers"
                                   class="form-field text ui-widget-content ui-corner-all">
                            <label for="capacity" class="form-label">Class capacity</label>
                            <select name="capacity" id="capacity"
                                    class="form-field text ui-widget-content ui-corner-all"> </select>
                        </fieldset>
                    </form>
                </div>
                <div class="modal-footer">
                    <button id="closeCreateDialogA" type="button" class="btn btn-default">Close</button>
                    <button type="button" id="createTruckButton" class="btn btn-primary">Create</button>
                </div>
            </div>
        </div>
    </div>

    <div id="dialog-remove" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Delete Truck</h4>
                </div>
                <div class="modal-body">
                    <div id="delete_dialog_message">Do you want really remove truck with number :<span
                            id="removeTruck"></span> ?
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button id="removeTruckButton" type="button" class="btn btn-primary">Delete</button>
                </div>
            </div>
        </div>
    </div>

</div>

<div class="error-server-block invisible">
    <div class="alert alert-danger" id="alert">
        <a href="#" class="close" id="close_server_message">&times;</a>
        <strong>Error!</strong> <span id="error_server_message"></span>
    </div>
</div>
</body>
</html>
