
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Manager - Orders</title>
    <%@ include file="/WEB-INF/views/manager/_head.jsp" %>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/table.js"></script>
    <script type="text/javascript" src="http://maps.google.com/maps/api/js?sensor=true"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/gmaps.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/manager/orders.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-ui.min.js"></script>
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/css/bootstrap/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/bootstrap/jquery-ui.min.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/trucks.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/orders.css">
</head>
<body onload="createDataTableOrder()">
<c:set var="activeMenu" value="orders"/>
<%@ include file="/WEB-INF/views/manager/_header.jsp" %>
<div id="block-table" class="hero-unit popin">
    <table id="orders_table" class="display" cellspacing="0" width="100%">
        <button id="create" class="button-tools">New</button>
        <button id="remove" class="button-tools" disabled>Remove</button>
        <button id="actionOrder" class="button-tools" disabled>Ship</button>
        <thead>
        <tr>
            <th>Number</th>
            <th>Status</th>
        </tr>
        </thead>
        <tbody id="orders_body" class="table table-hover">

        </tbody>
    </table>
    <div id="dialog-confirm" class="modal fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close closeConfirmDialog">&times;</button>
                    <h4 class="modal-title">Confirm Order</h4>
                </div>
                <div class="modal-body">
                    <div id="block_error_confirm" class="alert alert-danger invisible">
                        <a id="close_message_confirm" class="close" href="#">×</a><span
                            id="error_message_confirm"></span>
                    </div>
                    <div class="hero-unit popin list-stuffs">

                        <div id="block-stuff">
                            <div class="page-header fix-header">
                                Stuffs
                            </div>
                            <ul class="list-group">
                            </ul>
                        </div>

                        <div class="remove-stuff-button-block">
                            <button id="remove-stuff" class="btn button-tools" disabled>Remove</button>
                        </div>

                    </div>

                    <div class="hero-unit popin create-stuff clearfix">
                        <div class="page-header fix-header">
                            New Stuff
                        </div>
                        <div class="form-group">
                            <label for="nameField">Name</label>
                            <input type="text" class="form-control" id="nameField" placeholder="Enter name">
                        </div>
                        <div class="form-group">
                            <label for="weightField">Weight</label>
                            <input type="text" class="form-control" id="weightField" placeholder="Weight">
                        </div>
                        <div class="form-group">
                            <label for="gpsField">GPS</label>
                            <input type="text" class="form-control" id="gpsField" placeholder="GPS">
                        </div>
                        <div>
                            <div class="remove-stuff-button-block">
                                <button id="create-stuff" class="btn button-tools">Create</button>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default closeConfirmDialog">Close</button>
                    <button type="button" id="confirmOrder" class="btn btn-primary">Confirm</button>
                </div>
            </div>
        </div>
    </div>

    <div id="dialog-ship" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close closeShipDialog">&times;</button>
                    <h4 class="modal-title">Ship Order</h4>
                </div>
                <div class="modal-body clearfix">
                    <div id="block_error" class="alert alert-danger invisible">
                        <a id="close_message" class="close" href="#">×</a><span id="errorMessage"></span>
                    </div>
                    <div class="hero-unit popin ship-block left">
                        <div id="block-available-trucks">
                            <div class="page-header fix-header">
                                Available Trucks
                            </div>
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Register Number</th>
                                    <th>Count Drivers</th>
                                    <th>Class Capacity</th>
                                </tr>
                                </thead>
                                <tbody id="available-trucks" class="fix-td"></tbody>
                            </table>
                        </div>
                        <div>
                            <div class="select-truck-order">
                                <button id="select-truck" class="btn button-tools" disabled>Select</button>
                            </div>
                        </div>
                    </div>
                    <div class="hero-unit popin ship-block center">
                        <div id="block-selected-trucks-drivers">
                            <div class="page-header fix-header">
                                Order
                            </div>
                            <ul class="list-group fix-panel">
                                <li class="list-group-item disabled"><strong >Selected Truck</strong></li>
                                <li class="list-group-item" id="selected-truck"></li>
                                <li class="list-group-item disabled"><strong>Selected Drivers</strong></li>
                                <div id="selected-drivers">
                                </div>
                            </ul>
                        </div>
                        <div>
                            <div class="remove-truck-driver">
                                <button id="remove-truck-driver" class="btn button-tools" disabled>Remove</button>
                            </div>
                        </div>
                    </div>
                    <div class="hero-unit popin ship-block right">
                        <div id="block-free-drivers">
                            <div class="page-header fix-header">
                                Free Drivers
                            </div>
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>Surname Name (Miidle)</th>
                                    <th>Driving licence</th>
                                </tr>
                                </thead>
                                <tbody id="available-drivers" class="fix-td"></tbody>
                            </table>
                        </div>
                        <div>
                            <div class="add-driver-order">
                                <button id="add-driver" class="btn button-tools" disabled>Add</button>
                            </div>
                        </div>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default closeShipDialog">Close</button>
                    <button type="button" id="shipOrderButton" class="btn btn-primary">Ship</button>
                </div>
            </div>
        </div>
    </div>

</div>

<div id="dialog-remove" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Delete Order</h4>
            </div>
            <div class="modal-body">
                <div id="delete_dialog_message">Do you want really remove order with number :<span
                        id="removeOrder"></span> ?
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button id="removeOrderButton" type="button" class="btn btn-primary">Delete</button>
            </div>
        </div>
    </div>
</div>

<div id="dialog-close" class="modal fade">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title">Close Order</h4>
            </div>
            <div class="modal-body">
                <div id="close_dialog_message">Do you want really close order with number :<span
                        id="closeOrder"></span> ?
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                <button id="closeOrderButton" type="button" class="btn btn-primary">Close</button>
            </div>
        </div>
    </div>
</div>

<div class="block-info hero-unit popin invisible">
    <a id="closeInfo" class="close" href="#">×</a><span></span>

    <h1>Order Info</h1>
    <ul class="nav nav-tabs" role="tablist" id="myTab">
        <li class="active"><a href="#stuffs" role="tab" data-toggle="tab">Stuffs</a></li>
        <li><a href="#drivers" role="tab" data-toggle="tab">Drivers</a></li>
        <li><a href="#truck" role="tab" data-toggle="tab">Truck</a></li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane active" id="stuffs">
            <table class="table">
                <thead>
                <tr>
                    <th>Name</th>
                    <th>GPS</th>
                    <th>Weight</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody id="info-stuffs" class="fix-td"></tbody>
            </table>
        </div>
        <div class="tab-pane" id="drivers">
            <table class="table">
                <thead>
                <tr>
                    <th>Surname (Middle name) Name</th>
                    <th>Driving licence</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody id="info-drivers" class="fix-td"></tbody>
            </table>
        </div>
        <div class="tab-pane" id="truck">
            <h4>Register Number</h4>
            <p id="reg-number"></p>
            <h4>Count drivers</h4>
            <p id="count-drivers"></p>
            <h4>Status Truck</h4>
            <p id="status-truck"></p>
        </div>
    </div>
</div>
</div>
<div id="contentCreateStuffMap">
    <div id="gmapsCreateStuff"></div>
</div>
<div class="error-server-block invisible">
    <div class="alert alert-danger" id="alert">
        <a href="#" class="close" id="close_server_message">&times;</a>
        <strong>Error!</strong> <span id="error_server_message"></span>
    </div>
</div>
</body>
</html>
