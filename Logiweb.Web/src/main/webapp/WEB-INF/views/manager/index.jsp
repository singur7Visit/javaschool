
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@ include file="/WEB-INF/views/manager/_head.jsp" %>
    <style type="text/css">
        body {
            background: rgb(220, 220, 220); /* Old browsers */
            text-align: center;
            font-family: Georgia;
        }
        H1 {
            font-size: 50px;
            font-weight: normal;
            text-shadow: rgba(255,255,255,0.5) 0px 2px 3px;
            color: #9b9b9b;
            transition: all 1s;
        }

        .main-div {
            height: 70%;
            vertical-align: middle;
        }

    </style>
</head>
<body>
    <%@ include file="/WEB-INF/views/manager/_header.jsp" %>
    <div class="main-div">
        <div class="div-text">
            <h1>Welcome! Please use tabs for control trucks, drivers and orders.</h1>
        </div>
    </div>
</body>
</html>
