
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Manager - Drivers</title>
    <%@ include file="/WEB-INF/views/manager/_head.jsp" %>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.dataTables.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/table.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/manager/drivers.js"></script>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-ui.min.js"></script>
    <link rel="stylesheet" type="text/css"
          href="${pageContext.request.contextPath}/css/bootstrap/jquery.dataTables.min.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/bootstrap/jquery-ui.min.css">
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/trucks.css">
</head>
<body onload="createDataTableDriver()">
<c:set var="activeMenu" value="drivers" />
<%@ include file="/WEB-INF/views/manager/_header.jsp" %>
<div class="hero-unit popin">
    <table id="drivers_table" class="display" cellspacing="0" width="100%">
        <button id="create" class="button-tools">New</button>
        <button id="remove" class="button-tools" disabled>Remove</button>
        <thead>
        <tr>
            <th>Surname (Middle name) Name</th>
            <th>Driving licence</th>
            <th>Status</th>
        </tr>
        </thead>
        <tbody id="drivers_body" class="table table-hover">

        </tbody>
    </table>
    <div id="dialog-form" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button id="closeCreateDialog" type="button" class="close">&times;</button>
                    <h4 class="modal-title">New Driver</h4>
                </div>
                <div class="modal-body">
                    <div id="block_error" class="alert alert-danger invisible">
                        <a id="close_message" class="close" href="#">×</a><span id="errorMessage"></span>
                    </div>
                    <form id="driver_form">
                        <fieldset>
                            <label for="licence" class="form-label">Driving licence</label>
                            <input type="text" name="licence" id="licence"
                                   class="form-field text ui-widget-content ui-corner-all">
                            <label for="surname" class="form-label">Surname</label>
                            <input type="text" name="surname" id="surname"
                                   class="form-field text ui-widget-content ui-corner-all">
                            <label for="name" class="form-label">Name</label>
                            <input type="text" name="name" id="name"
                                    class="form-field text ui-widget-content ui-corner-all"> </input>
                            <label for="middle_name" class="form-label">Middle name</label>
                            <input type="text" name="middle_name" id="middle_name"
                                   class="form-field text ui-widget-content ui-corner-all"> </input>
                        </fieldset>
                    </form>
                </div>
                <div class="modal-footer">
                    <button id="closeCreateDialogA" type="button" class="btn btn-default">Close</button>
                    <button type="button" id="createDriverButton" class="btn btn-primary">Create</button>
                </div>
            </div>
        </div>
    </div>

    <div id="dialog-remove" class="modal fade">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title">Delete Driver</h4>
                </div>
                <div class="modal-body">
                    <div id="delete_dialog_message">Do you want really remove driver with licence :<span
                            id="removeDriver"></span> ?
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Cancel</button>
                    <button id="removeDriverButton" type="button" class="btn btn-primary">Delete</button>
                </div>
            </div>
        </div>
    </div>

</div>

<div class="error-server-block invisible">
    <div class="alert alert-danger" id="alert">
        <a href="#" class="close" id="close_server_message">&times;</a>
        <strong>Error!</strong> <span id="error_server_message"></span>
    </div>
</div>
</body>
</html>
