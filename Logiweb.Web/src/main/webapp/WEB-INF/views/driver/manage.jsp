
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@ include file="/WEB-INF/views/driver/_head.jsp" %>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/driver/manage.js"></script>
</head>
<body>
<c:set var="activeMenu" value="manage"/>
<%@ include file="/WEB-INF/views/driver/_header.jsp" %>
<div class="hero-unit popin">
    <div id="status">
        <h3>Status</h3>
        <div class="onoffswitch">
            <input  id="block_check" type="checkbox" name="onoffswitch" class="onoffswitch-checkbox">
            <label class="onoffswitch-label" for="block_check">
                <span class="onoffswitch-inner"></span>
                <span class="onoffswitch-switch"></span>
            </label>
        </div>
    </div>
    <h3>Stuffs</h3>
    <div>
        <table class="display" cellspacing="0" width="100%" id="stuff_table">
            <button id="deliver" class="button-tools" disabled>Deliver</button>
            <button id="complete" class="button-tools">Complete</button>
            <thead>
            <tr>
                <th>Name</th>
                <th>GPS</th>
                <th>Weight</th>
                <th>Status</th>
            </tr>
            </thead>
            <tbody id="info-stuffs" class="table table-hover"></tbody>
        </table>
    </div>
</div>
<div class="error-server-block invisible">
    <div class="alert alert-danger" id="alert">
        <a href="#" class="close" id="close_server_message">&times;</a>
        <strong>Error!</strong> <span id="error_server_message"></span>
    </div>
</div>
</body>
</html>
