
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<div class="navbar nav-top">
    <div class="navbar-inner">
        <div class="collapse navbar-collapse navcentre" id="bs-example-navbar-collapse-1">
            <div>
                <img id="logo" src="${pageContext.request.contextPath}/images/logo.png">
                <label id="label_logo" for="logo">Logiweb</label>
            </div>
            <ul id="main_menu" class="nav navbar-nav">
                <li <c:if test="${activeMenu eq 'info'}">class="active"</c:if>><a href="${pageContext.request.contextPath}/driver/index">Info</a></li>
                <li <c:if test="${activeMenu eq 'manage'}">class="active"</c:if>><a href="${pageContext.request.contextPath}/driver/manage">Manage</a></li>
            </ul>
            <a class="pull-right" href="${pageContext.request.contextPath}/driver/logout">Logout</a>
        </div>
    </div>
</div>

</body>
</html>
