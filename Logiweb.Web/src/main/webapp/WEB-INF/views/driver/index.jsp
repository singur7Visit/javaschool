
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <%@ include file="/WEB-INF/views/driver/_head.jsp" %>
    <script type="text/javascript" src="${pageContext.request.contextPath}/js/driver/info.js"></script>
</head>
<body>
<% if(session.getAttribute("driver") == null) { %>
    <div id="signIn">
        <div id="signIn-inner">
            <form role="form" method="POST" action="${pageContext.request.contextPath}/driver/regDriver">
                <div class="form-group">
                    <label id="label-licence" for="licence">Driving licence: </label>
                    <input type="text" class="form-control licence-input" name = "licence" id="licence" placeholder="Enter your licence">
                </div>
                <button id="signIn-button" type="submit" class="btn btn-default">Sign In</button>
            </form>
        </div>
    </div>
<% } else {%>
    <c:set var="activeMenu" value="info" />
    <%@ include file="/WEB-INF/views/driver/_header.jsp" %>
<div class="hero-unit popin">
    <ul class="nav nav-tabs" role="tablist">
        <li class="active"><a href="#personal_info" role="tab" data-toggle="tab">Personal Info</a></li>
        <li><a href="#order_info" role="tab" data-toggle="tab">Order Info</a></li>
        <li><a href="#drivers_info" role="tab" data-toggle="tab">Drivers Info</a></li>
        <li><a href="#truck_info" role="tab" data-toggle="tab">Truck Info</a></li>
    </ul>

    <div class="tab-content">
        <div class="tab-pane active" id="personal_info">
            <h4>Driving Licence</h4>
            <p id="driving-licence"></p>
            <h4>Surname Name (Middle name)</h4>
            <p id="snm"></p>
            <h4>Status</h4>
            <p id="status-driver"></p>
        </div>
        <div class="tab-pane" id="drivers_info">
            <table class="display" cellspacing="0" width="100%" id ="drivers_table">
                <thead>
                <tr>
                    <th>Surname Name (Middle name)</th>
                    <th>Driving licence</th>
                    <th>Status</th>
                </tr>
                </thead>
                <tbody id="info-drivers" class="table table-hover"></tbody>
            </table>
        </div>
        <div class="tab-pane" id="order_info">
            <div id="order">
                <h4>Number Order</h4>
                <p id="order-number"></p>
            </div>
            <div>
                <table class="display" cellspacing="0" width="100%" id ="stuff_table">
                    <thead>
                    <tr>
                        <th>Name</th>
                        <th>GPS</th>
                        <th>Weight</th>
                        <th>Status</th>
                    </tr>
                    </thead>
                    <tbody id="info-stuffs" class="table table-hover"></tbody>
                </table>
            </div>
        </div>
        <div class="tab-pane" id="truck_info">
            <h4>Register Number</h4>
            <p id="reg-number"></p>
            <h4>Count drivers</h4>
            <p id="count-drivers"></p>
            <h4>Status Truck</h4>
            <p id="status-truck"></p>
        </div>
    </div>
</div>
<% } %>
</body>
</html>
