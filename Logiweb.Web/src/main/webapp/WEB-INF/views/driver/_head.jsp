
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri='http://java.sun.com/jsp/jstl/core' prefix='c'%>
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap/bootstrap.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/head.css" />
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/bootstrap/bootstrap-responsive.css" />
<script src="${pageContext.request.contextPath}/js/jquery.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/bootstrap.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/head.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/driver/objs.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.dataTables.js"></script>
<script type="text/javascript" src="${pageContext.request.contextPath}/js/table.js"></script>
<link rel="stylesheet" type="text/css"
      href="${pageContext.request.contextPath}/css/bootstrap/jquery.dataTables.min.css">
<script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery-ui.min.js"></script>
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/bootstrap/jquery-ui.min.css">
<link rel="stylesheet" href="${pageContext.request.contextPath}/css/driver.css"/>
<%@ include file="/WEB-INF/views/driver/init_driver.jsp" %>
