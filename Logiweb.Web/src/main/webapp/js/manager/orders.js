function SimpleOrder(id, numOrder) {
    this.id = id;
    this.numOrder = numOrder;
}

function RequestShip(idsDrivers, idTruck) {
    var drivers = [];
    idsDrivers.forEach(function(value){
       drivers.push(parseInt(value));
    });
    this.idsDrivers = drivers;
    this.idTruck = parseInt(idTruck);
}

function Status(name) {
    this.name = name;
}

function Driver(id, licence, status, snm) {
    this.id = id;
    this.licence = licence;
    this.status = new Status(status);
    this.snm = snm;
}


function Truck(id, regNumber, countDrivers, classCapacity, status) {
    this.id = id;
    this.regNumber = regNumber;
    this.countDrivers = countDrivers;
    this.classCapacity = new Status(classCapacity);
    this.status = new Status(status);
}

function Stuff(id, gps, name, weight, status) {
    this.id = id;
    this.gps = gps;
    this.name = name;
    this.weight = weight;
    this.status = status;
}

function FullOrder(id, numOrder, status, drivers, truck, stuffs) {
    this.id = id;
    this.numOrder = numOrder;
    this.status = new Status(status);
    this.drivers = drivers;
    this.truck = truck;
    this.stuffs = stuffs;
}

var Order = {

    DEFAULT_ERROR_MESSAGE: "Error response server",
    DEFAULT_ERROR_MESSAGE_LICENCE: "Licence is not valid (11 numbers)",
    DEFAULT_ERROR_MESSAGE_EMPTY: "Surname, name and middle name must be not empty",
    orders: [],
    newStuffs: [],
    selectionTruck: null,
    selectionDriver: null,
    selectedIdsDrivers: [],

    init: function () {

        function fillStuffs(stuffs) {
            var body = "";
            $(stuffs).each(function (index, value) {
                body += "<tr><td title = \"" + value.name + "\">" + value.name + "</td>";
                body += "<td title = \"" + value.gps + "\">" + value.gps + "</td>";
                body += "<td title = \"" + value.weight + "\">" + value.weight + "</td>";
                body += "<td>" + value.status + "</td></tr>";
            });
            $("#info-stuffs").html(body);
        }

        function fillDrivers(drivers) {
            var body = "";
            $(drivers).each(function (index, value) {
                body += "<tr><td>" + value.snm + "</td>";
                body += "<td>" + value.licence + "</td>";
                body += "<td>" + value.status.name + "</td></tr>";
            });
            $("#info-drivers").html(body);
        }

        function fillTruck(truck) {
            if(truck == null) {
                $("#reg-number").html("");
                $("#count-drivers").html("");
                $("#status-truck").html("");
                return;
            }

            $("#reg-number").html(truck.regNumber);
            $("#count-drivers").html(truck.countDrivers);
            $("#status-truck").html(truck.status.name);
        }

        $("#actionOrder").button();
        Order.createDataTable().on('click', 'tr', function () {
            Order.selectedOrder = new SimpleOrder($(this)[0].id, $(this)[0].childNodes.item(0).textContent);
            var stuffs = Order.orders["'" + $(this)[0].id + "'"].stuffs;
            var drivers = Order.orders["'" + $(this)[0].id + "'"].drivers;
            var truck = Order.orders["'" + $(this)[0].id + "'"].truck;
            fillStuffs(stuffs);
            fillDrivers(drivers);
            fillTruck(truck);
            if ($(this).hasClass("selected")) {
                $(this).toggleClass("selected");
                Order.selectedOrder = null;
                $('#remove').button("disable");
                return;
            }
            $(this).parent().find(".selected").each(function () {
                $(this).toggleClass("selected");
            });
            if ($(this)[0].childNodes.item(1).textContent == "closed")
                $('#actionOrder').button("disable");
            else if ($(this)[0].childNodes.item(1).textContent == "created") {
                $('#actionOrder').button("enable").html("Confirm");
                $('#actionOrder').off("click");
                $("#actionOrder").button().on("click", function () {
                    Order.openConfirmDialog();
                });
            }
            else if ($(this)[0].childNodes.item(1).textContent == "confirmed") {
                $('#actionOrder').button("enable").html("Ship");
                $('#actionOrder').off("click");
                $("#actionOrder").button().on("click", function () {
                    Order.openShipDialog();
                });
            }
            else if($(this)[0].childNodes.item(1).textContent == "completed"){
                $('#actionOrder').button("enable").html("Close");
                $('#actionOrder').off("click");
                $("#actionOrder").button().on("click", function () {
                    Order.openCloseDialog();
                });
            }
            else
                $('#actionOrder').button("disable");

            $(this).toggleClass("selected");
            $("#remove").button("enable");
            Order.toggleBlockInfo(true);
        });

        Order.reloadTable();


        $("#closeInfo").on("click", function () {
            Order.toggleBlockInfo(false);
        });

        $("#create").button().on("click", function () {
            Order.createOrder();
        });

        $("#remove").button().on("click", function () {
            Order.openRemoveDialog()
        });

        $("#closeOrderButton").button().on("click", function() {
            Order.closeOrder();
        });

        $("#removeOrderButton").button().on("click", function () {
            Order.removeOrder();
        });

        $("#remove-stuff").button().on("click", function () {
            Order.removeNewStuff();
        });


        $("#close_server_message").on("click", function () {
            $(".error-server-block").toggleClass("invisible");
        });

        $("#close_message_confirm").on("click", function () {
            $("#block_error_confirm").toggleClass("invisible");
        });

        $(".closeConfirmDialog").on("click", function () {
            Order.closeConfirmDialog();
        });

        $(".closeShipDialog").on("click", function () {
            Order.closeShipDialog();
        });

        $("#shipOrderButton").button().on("click", function() {
            Order.shipOrder();
        });

        $("#confirmOrder").button().on("click", function () {
            Order.confirmOrder();
        });

        $("#close_message").on("click", function () {
            $("#block_error").toggleClass("invisible");
        });

        Order.selectedOrder = null;

        $("#block-stuff").on('click', 'li', function () {
            $(this).parent().find(".selected").each(function () {
                $(this).toggleClass("selected");
            });
            $("#remove-stuff").button("enable");
            $(this).toggleClass("selected");
        });

        $("#gpsField").popover({
            html: true,
            content: function () {
                if (Order.newStuffMapHtml == undefined) {
                    Order.newStuffMapHtml = $("#contentCreateStuffMap").html();
                    $("#contentCreateStuffMap").remove();
                }
                return Order.newStuffMapHtml;
            },
            placement: 'bottom',
            container: '#dialog-confirm'
        }).on('shown.bs.popover', function () {
            Order.createStuffMap = new GMaps({
                div: '#gmapsCreateStuff',
                zoomControl: true,
                lat: 59.9793409,
                lng: 30.193847,
                zoom: 10,
                click: function (e) {
                    $("#gpsField").val(e.latLng.B + ";" + e.latLng.k).popover('hide')
                        .attr("title", e.latLng.B + ";" + e.latLng.k);
                }
            });
        });

        function clearAvailableTruck() {
            $("#available-trucks").find(".selected").each(function () {
                $(this).removeClass("selected");
            });
            $("#select-truck").button("disable");
        }
        function clearAvailableDrivers() {
            $("#available-drivers").find(".selected").each(function () {
                $(this).removeClass("selected");
            });
            $("#add-driver").button("disable");
        }
        $("#available-trucks").on('click', 'tr', function () {
            $(this).parent().find(".selected").each(function () {
                $(this).toggleClass("selected");
            });
            Order.selectionTruck = new Truck($($(this)[0]).attr("truck-id"), $(this)[0].childNodes[0].textContent,
                $(this)[0].childNodes[1].textContent);
            $("#select-truck").button("enable");
            clearAvailableDrivers();
            $(this).toggleClass("selected");
        });

        $("#available-drivers").on('click', 'tr', function () {
            $(this).parent().find(".selected").each(function () {
                $(this).toggleClass("selected");
            });
            Order.selectionDriver = new Driver($($(this)[0]).attr("driver-id"), $(this)[0].childNodes[1].textContent,
                null,$(this)[0].childNodes[0].textContent)
            $("#add-driver").button("enable");
            if($.inArray(Order.selectionDriver.id,Order.selectedIdsDrivers) != -1 || Order.selectionTruck == null ||
                Order.selectionTruck.countDrivers == Order.selectedIdsDrivers.length) {
                $("#add-driver").button("disable");
                Order.selectionDriver = null;
                return;
            }
            clearAvailableTruck();
            $(this).toggleClass("selected");
        });

        $("#selected-drivers").on('click', 'li', function () {
            $(this).parent().find(".selected").each(function () {
                $(this).toggleClass("selected");
            });
            $("#selected-truck").removeClass('selected');
            clearAvailableDrivers();
            clearAvailableTruck();
            $('#remove-truck-driver').button('enable');
            $(this).toggleClass("selected");
        });

        $("#dialog-confirm").on('hidden.bs.modal', function () {
            $("#gpsField").popover('hide');
        });

        $("#create-stuff").button().on('click', function () {
            Order.createStuff();
        });

        $("#select-truck").button().on('click', function () {
            $("#selected-drivers").html("");
            $('#remove-truck-driver').button('disable');
            Order.selectedIdsDrivers = [];
            $("#selected-truck").html("<span class='strong'>Register number: </span>" + Order.selectionTruck.regNumber
                + "<br> <span class='strong'>Count drivers :</span>" + Order.selectionTruck.countDrivers).removeClass('selected');
        });

        $("#selected-truck").on('click', function () {
            if($(this).html() == "")
                return;
            $("#selected-drivers").find(".selected").each(function () {
                $(this).removeClass("selected");
            });
            clearAvailableDrivers();
            clearAvailableTruck();
            $('#remove-truck-driver').button('enable');
            $(this).toggleClass("selected");
        });

        $("#add-driver").button().on('click', function () {
            if($.inArray(Order.selectionDriver.id,Order.selectedIdsDrivers) != -1 || Order.selectionTruck == null ||
                Order.selectionTruck.countDrivers == Order.selectedIdsDrivers.length)
                return;
            Order.selectedIdsDrivers.push(Order.selectionDriver.id);
            $("#selected-drivers").append("<li selected-driver-id = \"" + Order.selectionDriver.id + "\" class=\"list-group-item\">" +
                "<span class='strong'>Surname Name (Middle Name): </span>" + Order.selectionDriver.snm
                + "<br> <span class='strong'>Driving licence :</span>" + Order.selectionDriver.licence + "</li>");
            clearAvailableDrivers();
            $(this).button("disable");

        });

        $("#remove-truck-driver").button().on('click', function () {
            if($("#selected-truck").hasClass("selected")) {
                $("#selected-drivers").html("");
                $("#selected-truck").html("");
                $("#selected-truck").removeClass("selected");
                Order.selectionTruck = null;
                Order.selectedIdsDrivers = [];
                $('#remove-truck-driver').button('disable');
            } else {
                $("#selected-drivers").find(".selected").each(function() {
                    var index = Order.selectedIdsDrivers.indexOf($(this).attr("selected-driver-id"));
                    Order.selectedIdsDrivers.splice(index, 1);
                    $(this).remove();
                })
            }
        });
    },


// open dialogs

    openShipDialog: function () {
        var drivers = Order.getAvailableDrivers();
        var trucks = Order.getAvailableTrucks();
        if (drivers == null || trucks == null)
            return;
        var response = "";
        drivers.forEach(function (driver) {
            response += "<tr driver-id=\"" + driver.id + "\"><td title = \"" + driver.snm + "\">" + driver.snm + "</td>";
            response += "<td title = \"" + driver.licence + "\">" + driver.licence + "</td></tr>";
        });
        $("#available-drivers").html(response);
        response = "";
        trucks.forEach(function (truck) {
            response += "<tr truck-id=\"" + truck.id + "\"><td title = \"" + truck.regNumber + "\">" + truck.regNumber + "</td>";
            response += "<td title = \"" + truck.countDrivers + "\">" + truck.countDrivers + "</td>";
            response += "<td title = \"" + truck.classCapacity + "\">" + truck.classCapacity + "</td></tr>";
        });
        $("#available-trucks").html(response);
        $("#dialog-ship").modal('show');
    },

    openConfirmDialog: function () {
        $("#dialog-confirm").modal('show');
    },

    openCloseDialog: function () {
        $("#closeOrder").html(Order.selectedOrder.numOrder);
        $("#dialog-close").modal('show');
    },
    openRemoveDialog: function () {
        $("#removeOrder").html(Order.selectedOrder.numOrder);
        $("#dialog-remove").modal('show');
    },

//end open dialogs


// Actions order

    createOrder: function () {
        var response = null;
        $.ajax({
            type: "PUT",
            url: "orders/create",
            contentType: "application/json",
            async: false,
            dataType: 'json',
            success: function (data) {
                response = data;
            }
        });
        if (response == null || response.type == 1) {
            $("#errorMessage").html(response.type == 1 ? response.message : Order.DEFAULT_ERROR_MESSAGE);
            $("#block_error").toggleClass("invisible")
        }
        else
            Order.reloadTable();
    },

    confirmOrder: function () {
        var response = null;
        var request = [];
        for (var key in Order.newStuffs) {
            request.push(Order.newStuffs[key]);
        }
        $.ajax({
            type: "PUT",
            contentType: "application/json",
            url: "orders/" + Order.selectedOrder.id + "/confirm",
            data: JSON.stringify(request),
            async: false,
            success: function (data) {
                response = data;
            }
        });
        if (response == null || response.type == 1) {
            $("#error_message_confirm").html(response.type == 1 ? response.message : Order.DEFAULT_ERROR_MESSAGE);
            $("#block_error_confirm").toggleClass("invisible")
        }
        else {
            Order.reloadTable();
            Order.closeConfirmDialog();
        }
    },

    shipOrder: function () {
        var request = new RequestShip(Order.selectedIdsDrivers,
            Order.selectionTruck == null ? null : Order.selectionTruck.id);
        var response = "";
        $.ajax({
            type: "PUT",
            url: "orders/" + Order.selectedOrder.id + "/ship",
            async: false,
            contentType: "application/json",
            dataType : 'json',
            data : JSON.stringify(request),
            success: function (data) {
                response = data;
            }
        });
        if (response == null || response.type == 1) {
            $("#error_server_message").html(response == null ? Order.DEFAULT_ERROR_MESSAGE : response.message);
            $(".error-server-block").toggleClass("invisible");
        }
        else
            Order.reloadTable();
        Order.closeShipDialog();

    },

    closeOrder: function () {
        var response = "";
        $.ajax({
            type: "PUT",
            url: "orders/" + Order.selectedOrder.id + "/close",
            async: false,
            success: function (data) {
                response = data;
            }
        });
        if (response == null || response.type == 1) {
            $("#error_server_message").html(response == null ? Order.DEFAULT_ERROR_MESSAGE : response.message);
            $(".error-server-block").toggleClass("invisible");
        }
        else
            Order.reloadTable();
        Order.closeCloseDialog();
    },

    removeOrder: function () {
        var response = null;
        $.ajax({
            type: "DELETE",
            url: "orders/delete/" + Order.selectedOrder.id,
            async: false,
            success: function (data) {
                response = data;
            }
        });
        if (response == null || response.type == 1) {
            $("#error_server_message").html(response == null ? Order.DEFAULT_ERROR_MESSAGE : response.message);
            $(".error-server-block").toggleClass("invisible");
        }
        else
            Order.reloadTable();
        $("#remove").button("disable");
        $("#actionOrder").button("disable");
        Order.closeRemoveDialog();

    },

// end actions order


// close dialogs

    closeRemoveDialog: function () {
        $("#dialog-remove").modal('hide');
    },

    closeConfirmDialog: function () {
        $("#gpsField").val("");
        $("#nameField").val("");
        $("#weightField").val("");
        $("#block-stuff").find("ul").find("li").each(function (index, value) {
            $(value).remove();
        });
        $("#dialog-confirm").modal('hide');
    },


    closeShipDialog: function () {
        $("#dialog-ship").modal('hide');
    },

    closeCloseDialog: function () {
        $("#dialog-close").modal('hide');
    },


//end close orders

    removeNewStuff: function () {
        var id = $("#block-stuff").find("ul").find(".selected").attr("id");
        delete Order.newStuffs[id];
        $("#block-stuff").find("ul").find(".selected").remove()
    },

    createStuff: function () {
        var newStuff = new Stuff(0, $("#gpsField").val(), $("#nameField").val(), $("#weightField").val(), null);
        var id = $("#block-stuff").find("ul").find("li").last().attr("id");
        Order.newStuffs["stuff" + (id == undefined ? 1 : (parseInt(id.substring(5, 6)) + 1))] = newStuff;
        $("#block-stuff").find("ul").append("<li id = \"stuff" + (id == undefined ? 1 : (parseInt(id.substring(5, 6)) + 1)) + "\" class=\"list-group-item\">" + newStuff.name + "</li>");
    },

    reloadTable: function () {
        $('#orders_table').dataTable().fnDestroy();
        $("#orders_body").html("");
        $.ajax({
            type: "GET",
            url: "orders/all",
            async: false,
            success: function (data) {
                var response = "";
                Order.orders.length = 0;
                $.each(data, function (index, value) {
                    Order.pushOrder(value);
                    response += "<tr id=\"" + value.id + "\"><td>" + value.numOrder + "</td>";
                    response += "<td>" + value.status + "</td></tr>";
                });
                $("#orders_body").html(response);
                Order.createDataTable();
            }
        });
    },

    pushOrder: function (data) {
        var drivers = [];
        var stuffs = [];
        $(data.drivers).each(function (index, value) {
            drivers.push(new Driver(value.id, value.licence, value.status, value.snm));
        });
        $(data.stuffs).each(function (index, value) {
            stuffs.push(new Stuff(value.id, value.gps, value.name, value.weight, value.status));
        });
        var truck = data.truck == null ? null :
            new Truck(data.truck.id, data.truck.regNumber, data.truck.countDrivers, data.truck.classCapacity, data.truck.status);
        Order.orders["'" + data.id + "'"] = new FullOrder(data.id, data.numOrder, data.status, drivers, truck, stuffs);

    },

    createDataTable: function () {
        return $('#orders_table').dataTable({
            "sDom": "<'row'<'span4'l><'span4 search_table'f>r>t<'row'<'span4'i><'span4 offset4'p>>",
            "sPaginationType": "bootstrap",
            "oLanguage": {
                "sLengthMenu": "_MENU_ records per page"
            }
        });
    },

    toggleBlockInfo: function (flag) {
        $("#block-table").removeClass("block-table");
        $(".block-info").removeClass("invisible");
        if (flag)
            $("#block-table").addClass("block-table");
        else
            $(".block-info").addClass("invisible");
    },

    getAvailableTrucks: function () {
        var response = null;
        $.ajax({
            type: "GET",
            url: "trucks/available",
            async: false,
            dataType: 'json',
            success: function (data) {
                response = data;
            }
        });
        return response;
    },

    getAvailableDrivers: function () {
        var response = null;
        $.ajax({
            type: "GET",
            url: "drivers/available",
            dataType: 'json',
            async: false,
            success: function (data) {
                response = data;
            }
        });
        return response;
    }

};

function createDataTableOrder() {
    Order.init();
}