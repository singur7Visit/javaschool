function extend(Child, Parent) {
    var F = function() { };
    F.prototype = Parent.prototype;
    Child.prototype = new F();
    Child.prototype.constructor = Child;
    Child.superclass = Parent.prototype;
}


function StatusTruck(name) {
    this.name = name;
}

function ClassCapacity(name) {
    this.name = name;
}

function SimpleTruck(regNumber,count,className) {
    this.regNumber = regNumber;
    this.countDrivers = count;
    this.classCapacity = new ClassCapacity(className);
}

function SelectedTruck(id, regNumber, count, status, className) {
    this.id = id;
    this.regNumber = regNumber;
    this.countDrivers = count;
    this.classCapacity = new ClassCapacity(className);
    this.status = new StatusTruck(status);
} extend(SelectedTruck,SimpleTruck);

var Truck = {

    DEFAULT_ERROR_MESSAGE : "Error response server",
    DEFAULT_ERROR_MESSAGE_LICENCE : "Number is not valid (2 letter + 5 number)",
    DEFAULT_ERROR_MESSAGE_EMPTY :"Count is not valid (Only numbers)",

    init: function () {

        Truck.createDataTable().on('click', 'tr', function() {
            Truck.selectedTruck = new SelectedTruck($(this)[0].id,$(this)[0].childNodes.item(0).textContent,
                $(this)[0].childNodes.item(1).textContent,$(this)[0].childNodes.item(2).textContent,
                $(this)[0].childNodes.item(3).textContent);
            if ($(this).hasClass("selected")) {
                $(this).toggleClass("selected");
                Truck.selectedTruck = null;
                $('#remove').button("disable");
                return;
            }
            $(".selected").each(function(index) {
                $(this).removeClass("selected");
            });
            $(this).addClass("selected");
            $('#remove').button("enable");
        });

        Truck.reloadTable();

        $("#create").button().on("click", function () {
            Truck.openCreateDialog();
        });

        $("#remove").button().on("click", function () {
            Truck.openRemoveDialog()
        });

        $("#removeTruckButton").button().on("click", function () {
            Truck.removeTruck();
        });

        $("#createTruckButton").button().on("click", function () {
            Truck.createTruck();
        });

        $("#close_server_message").on("click", function() {
            $(".error-server-block").addClass("invisible");
        });

        $("#closeCreateDialog").on("click", function() {
            Truck.closeCreateDialog();
        });

        $("#closeCreateDialogA").on("click", function() {
            Truck.closeCreateDialog();
        });

        $("#close_message").on("click", function() {
            $("#block_error").addClass("invisible");
        });

        Truck.selectedTruck = null;

    },

    openCreateDialog: function () {
        var classes = Truck.getClassesCapacity();
        if(classes == null)
            return;
        if($("#capacity").find("option").size() == 0)
            $(classes).each(function(index,value) {
                $("#capacity").append(new Option(value,value));
            });
        $("#dialog-form").modal('show');
    },

    getClassesCapacity: function () {
        var response = null;
        $.ajax({
            type: "GET",
            url: "trucks/classes/all",
            async: false,
            success: function (data) {
                response = data;
            }
        });
        return response;
    },

    openRemoveDialog : function() {
        $("#removeTruck").html(Truck.selectedTruck.regNumber);
        $("#dialog-remove").modal('show');
    },

    createTruck: function () {
        var newTruck = new SimpleTruck($("#reg_number").val(),$("#count_drivers").val(),$("#capacity").val());
        var message;
        if((message = Truck.validateTruck(newTruck)) != 0) {
            $("#errorMessage").html(message);
            $("#block_error").removeClass("invisible")
            return;
        }
        var response = null;
        $.ajax({
            type: "PUT",
            url: "trucks/add",
            contentType: "application/json",
            async: false,
            data: JSON.stringify(newTruck),
            dataType: 'json',
            success: function (data) {
                response = data;
            }
        });
        if(response == null) {
            $("#errorMessage").html(Truck.DEFAULT_ERROR_MESSAGE);
            $("#block_error").removeClass("invisible")
        }
        else if(response.type == 1) {
            $("#errorMessage").html(response.message);
            $("#block_error").removeClass("invisible");
        }
        else {
            Truck.closeCreateDialog();
            Truck.reloadTable();
        }
    },

    removeTruck: function() {
        var response = null;
        $.ajax({
            type: "DELETE",
            url: "trucks/delete/" + Truck.selectedTruck.id,
            async: false,
            success: function (data) {
                response = data;
            }
        });
        if(response == null || response.type == 1) {
            $("#error_server_message").html(response == null ? Truck.DEFAULT_ERROR_MESSAGE : response.message);
            $(".error-server-block").removeClass("invisible");
        }
        else
            Truck.reloadTable();
        Truck.closeRemoveDialog();

    },

    closeRemoveDialog : function() {
        $("#dialog-remove").modal('hide');
    },

    closeCreateDialog : function() {
        $("#truck_form")[0].reset();
        $("#block_error").addClass("invisible");
        $("#dialog-form").modal('hide');
    },

    validateTruck : function(truck) {
        var xRegNumber = new RegExp("[A-Za-z]{2}\\d{5}");
        var xCount = new RegExp("\\d{1,99999}");
        if(!xRegNumber.test(truck.regNumber))
            return Truck.DEFAULT_ERROR_MESSAGE_LICENCE;
        else if(!xCount.test(truck.countDrivers))
            return Truck.DEFAULT_ERROR_MESSAGE_EMPTY;
        else
            return 0;
    },

    reloadTable : function() {
        $('#trucks_table').dataTable().fnDestroy();
        $("#trucks_body").html("");
        $.ajax({
            type: "GET",
            url: "trucks/all",
            async: false,
            success: function (data) {
                var response = "";
                $.each(data, function (index, value) {
                    response += "<tr id=\"" + value.id + "\"><td>" + value.regNumber + "</td>";
                    response += "<td>" + value.countDrivers + "</td>";
                    response += "<td>" + value.classCapacity + "</td>";
                    response += "<td>" + value.status + "</td></tr>";
                });
                $("#trucks_body").html(response);
                Truck.createDataTable();
            }
        });
    },

    createDataTable : function () {
        return $('#trucks_table').dataTable({
            "sDom": "<'row'<'span4'l><'span4 search_table'f>r>t<'row'<'span4'i><'span4 offset4'p>>",
            "sPaginationType": "bootstrap",
            "oLanguage": {
                "sLengthMenu": "_MENU_ records per page"
            }
        });
    }

}

function createDataTableTruck() {
    Truck.init();
}