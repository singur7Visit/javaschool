function SimpleDriver(id, licence, snm) {
    this.id = id;
    this.licence = licence;
    this.snm = snm;
}

var Driver = {

    DEFAULT_ERROR_MESSAGE: "Error response server",
    DEFAULT_ERROR_MESSAGE_LICENCE: "Licence is not valid (11 numbers)",
    DEFAULT_ERROR_MESSAGE_EMPTY: "Surname, name and middle name must be not empty",

    init: function () {

        Driver.createDataTable().on('click', 'tr', function () {
            Driver.selectedDriver = new SimpleDriver($(this)[0].id, $(this)[0].childNodes.item(1).textContent,null);
            if ($(this).hasClass("selected")) {
                $(this).toggleClass("selected");
                Driver.selectedDriver = null;
                $('#remove').button("disable");
                return;
            }
            $(".selected").each(function () {
                $(this).removeClass("selected");
            });
            $(this).addClass("selected");
            $('#remove').button("enable");
        });

        Driver.reloadTable();

        $("#create").button().on("click", function () {
            Driver.openCreateDialog();
        });

        $("#remove").button().on("click", function () {
            Driver.openRemoveDialog()
        });

        $("#removeDriverButton").button().on("click", function () {
            Driver.removeDriver();
        });

        $("#createDriverButton").button().on("click", function () {
            Driver.createDriver();
        });

        $("#close_server_message").on("click", function () {
            $(".error-server-block").addClass("invisible");
        });

        $("#closeCreateDialog").on("click", function () {
            Driver.closeCreateDialog();
        });

        $("#closeCreateDialogA").on("click", function () {
            Driver.closeCreateDialog();
        });

        $("#close_message").on("click", function () {
            $("#block_error").addClass("invisible");
        });

        Driver.selectedDriver = null;

    },

    openCreateDialog: function () {
        $("#dialog-form").modal('show');
    },

    openRemoveDialog: function () {
        $("#removeDriver").html(Driver.selectedDriver.licence);
        $("#dialog-remove").modal('show');
    },

    createDriver: function () {
        var newDriver = new SimpleDriver(0, $("#licence").val(),
            $("#surname").val() + " " + $("#name").val() + " " + $("#middle_name").val());
        var message;
        if ((message = Driver.validateDriver(newDriver)) != 0) {
            $("#errorMessage").html(message);
            $("#block_error").removeClass("invisible");
            return;
        }
        var response = null;
        $.ajax({
            type: "PUT",
            url: "drivers/add",
            contentType: "application/json",
            async: false,
            data: JSON.stringify(newDriver),
            dataType: 'json',
            success: function (data) {
                response = data;
            }
        });
        if (response == null) {
            $("#errorMessage").html(Driver.DEFAULT_ERROR_MESSAGE);
            $("#block_error").removeClass("invisible")
        }
        else if (response.type == 1) {
            $("#errorMessage").html(response.message);
            $("#block_error").removeClass("invisible");
        }
        else {
            Driver.closeCreateDialog();
            Driver.reloadTable();
        }
    },

    removeDriver: function () {
        var response = null;
        $.ajax({
            type: "DELETE",
            url: "drivers/delete/" + Driver.selectedDriver.id,
            async: false,
            success: function (data) {
                response = data;
            }
        });
        if (response == null || response.type == 1) {
            $("#error_server_message").html(response == null ? Driver.DEFAULT_ERROR_MESSAGE : response.message);
            $(".error-server-block").removeClass("invisible");
        }
        else
            Driver.reloadTable();
        Driver.closeRemoveDialog();

    },

    closeRemoveDialog: function () {
        $("#dialog-remove").modal('hide');
    },

    closeCreateDialog: function () {
        $("#driver_form")[0].reset();
        $("#block_error").addClass("invisible");
        $("#dialog-form").modal('hide');
    },

    validateDriver: function (driver) {
        var xLicence = new RegExp("\\d{11}");
        if (!xLicence.test(driver.licence))
            return Driver.DEFAULT_ERROR_MESSAGE_LICENCE;
        else if (driver.snm == "   ")
            return Driver.DEFAULT_ERROR_MESSAGE_EMPTY;
        else
            return 0;
    },

    reloadTable: function () {
        $('#drivers_table').dataTable().fnDestroy();
        $("#drivers_body").html("");
        $.ajax({
            type: "GET",
            url: "drivers/all",
            async: false,
            success: function (data) {
                var response = "";
                $.each(data, function (index, value) {
                    response += "<tr id=\"" + value.id + "\"><td>" + value.snm + "</td>";
                    response += "<td>" + value.licence + "</td>";
                    response += "<td>" + value.status + "</td></tr>";
                });
                $("#drivers_body").html(response);
                Driver.createDataTable();
            }
        });
    },

    createDataTable : function () {
        return $('#drivers_table').dataTable({
            "sDom": "<'row'<'span4'l><'span4 search_table'f>r>t<'row'<'span4'i><'span4 offset4'p>>",
            "sPaginationType": "bootstrap",
            "oLanguage": {
                "sLengthMenu": "_MENU_ records per page"
            }
        });
    }

};

function createDataTableDriver() {
    Driver.init();
}