/**
 * Created by dsing on 25.09.14.
 */

var driverInfo;

function initTruckInfo () {
    var truck = driverInfo.truck;
    if(truck == null) {
        $("#reg-number").html("");
        $("#count-drivers").html("");
        $("#status-truck").html("");
        return;
    }

    $("#reg-number").html(truck.regNumber);
    $("#count-drivers").html(truck.countDrivers);
    $("#status-truck").html(truck.status.name);
}

function initPersonalInfo () {
    if(driverInfo == null) {
        $("#driving-licence").html("");
        $("#snm").html("");
        $("#status-driver").html("");
        return;
    }

    $("#driving-licence").html(driverInfo.licence);
    $("#snm").html(driverInfo.snm);
    $("#status-driver").html(driverInfo.status.name);
}

function initDriversInfo() {
    var body = "";
    $(driverInfo.coDrivers).each(function (index, value) {
        body += "<tr><td title = \"" + value.snm + "\">" + value.snm + "</td>";
        body += "<td title = \"" + value.licence + "\">" + value.licence + "</td>";
        body += "<td>" + value.status.name + "</td></tr>";
    });
    $("#info-drivers").html(body);
    $('#drivers_table').dataTable({
        "sDom": "<'row'<'span4'l><'span4 search_table'f>r>t<'row'<'span4'i><'span4 offset4'p>>",
        "sPaginationType": "bootstrap",
        "oLanguage": {
            "sLengthMenu": "_MENU_ records per page"
        }
    });
}

function initOrderInfo() {
    $("#order-number").html(driverInfo.numOrder == null ? "No order" : driverInfo.numOrder);
    var body = "";
    $(driverInfo.stuffs).each(function (index, value) {
        body += "<tr><td title = \"" + value.name + "\">" + value.name + "</td>";
        body += "<td title = \"" + value.gps + "\">" + value.gps + "</td>";
        body += "<td title = \"" + value.weight + "\">" + value.weight + "</td>";
        body += "<td>" + value.status + "</td></tr>";
    });
    $("#info-stuffs").html(body);
    $('#stuff_table').dataTable({
        "sDom": "<'row'<'span4'l><'span4 search_table'f>r>t<'row'<'span4'i><'span4 offset4'p>>",
        "sPaginationType": "bootstrap",
        "oLanguage": {
            "sLengthMenu": "_MENU_ records per page"
        }
    });
}

$(document).ready(function () {
    $.ajax({
        async: false,
        url: driverLicence + "/info",
        success: function (data) {
            var stuffs = [];
            if (data.stuffs != null)
                data.stuffs.forEach(function (value) {
                    stuffs.push(new Stuff(value.id, value.gps, value.weight, value.name, value.status));
                });
            var truck = null;
            if(data.truck != null)
                truck = new Truck(data.truck.id,data.truck.regNumber,data.truck.countDrivers,
                    data.truck.classCapacity,data.truck.status);
            var coDrivers = [];
            if(data.coDrivers != null)
                data.coDrivers.forEach(function(value) {
                   coDrivers.push(new Driver(value.id,value.snm,value.licence,value.status)) ;
                });
            driverInfo = new DriverExtend(data.driver.id,data.driver.snm,data.driver.licence,
                data.driver.status,data.numOrder,truck,stuffs,coDrivers);
        }
    });
    initPersonalInfo();
    initOrderInfo();
    initDriversInfo();
    initTruckInfo();
});