/**
 * Created by dsing on 25.09.14.
 */
var stuffs = [];
var STATUS_DRIVER;
var SELECTED_STUFF;
var NUM_ORDER;

function initStuffs() {
    var body ="";
    $(stuffs).each(function (index, value) {
        body += "<tr id=\"" + value.id + "\"><td title = \"" + value.name + "\">" + value.name + "</td>";
        body += "<td title = \"" + value.gps + "\">" + value.gps + "</td>";
        body += "<td title = \"" + value.weight + "\">" + value.weight + "</td>";
        body += "<td>" + value.status + "</td></tr>";
    });
    $("#info-stuffs").html(body);
    $('#stuff_table').dataTable({
        "sDom": "<'row'<'span4'l><'span4 search_table'f>r>t<'row'<'span4'i><'span4 offset4'p>>",
        "sPaginationType": "bootstrap",
        "oLanguage": {
            "sLengthMenu": "_MENU_ records per page"
        }
    });
}

function initStuffsInfo() {
    $.ajax({
        async: false,
        url: driverLicence + "/info",
        success: function (data) {
            stuffs = [];
            if (data.stuffs != null)
                data.stuffs.forEach(function (value) {
                    stuffs.push(new Stuff(value.id, value.gps, value.weight, value.name, value.status));
                });
            STATUS_DRIVER = new Description(data.driver.status);
            NUM_ORDER = data.numOrder;
        }
    });
}

function setStatus() {
    var response = null;
    var status = (STATUS_DRIVER.name == "busy" ? new Description("driving") : new Description("busy"));
    $.ajax({
        type: "PUT",
        url: driverLicence + "/status",
        async: false,
        contentType: "application/json",
        dataType : 'json',
        data: JSON.stringify(status),
        success: function (data) {
            response = data;
        }
    });
    if (response == null || response.type == 1) {
        $("#error_server_message").html(response == null ? Driver.DEFAULT_ERROR_MESSAGE : response.message);
        $(".error-server-block").removeClass("invisible");
        $("#block_check").prop('checked', STATUS_DRIVER.name != "busy");
    } else {
        STATUS_DRIVER = status;
        $("#block_check").prop('checked', STATUS_DRIVER.name != "busy");
    }

}

function completeOrder() {
    var response = null;
    $.ajax({
        type: "PUT",
        url: "order/" + NUM_ORDER + "/complete",
        async: false,
        success: function (data) {
            response = data;
        }
    });
    if (response == null || response.type == 1) {
        $("#error_server_message").html(response == null ? Driver.DEFAULT_ERROR_MESSAGE : response.message);
        $(".error-server-block").removeClass("invisible");
    }
    else
        location.reload();
}

$(document).ready(function () {

    initStuffsInfo();
    initStuffs();
    $("#info-stuffs").on('click', 'tr', function () {
        SELECTED_STUFF = new Stuff($(this)[0].id, $(this)[0].childNodes.item(0).textContent,
            $(this)[0].childNodes.item(1).textContent,
            $(this)[0].childNodes.item(2).textContent,$(this)[0].childNodes.item(3).textContent);
        if ($(this).hasClass("selected")) {
            $(this).toggleClass("selected");
            SELECTED_STUFF = null;
            $('#deliver').button("disable");
            return;
        }
        $(".selected").each(function () {
            $(this).removeClass("selected");
        });
        $(this).addClass("selected");
        $('#deliver').button("enable");
    });
    $("#deliver").button().on('click', function() {
        var response = null;
        var isComplete = true;
        stuffs.forEach(function(value) {
           isComplete &= (SELECTED_STUFF.id == value.id ? true : value.status);
        });
        if(isComplete) {
            completeOrder();
            return;
        }
        $.ajax({
            type: "PUT",
            url: "stuff/" + SELECTED_STUFF.id + "/deliver",
            async: false,
            success: function (data) {
                response = data;
            }
        });
        if (response == null || response.type == 1) {
            $("#error_server_message").html(response == null ? Driver.DEFAULT_ERROR_MESSAGE : response.message);
            $(".error-server-block").removeClass("invisible");
        }
        else
            location.reload();
    });

    $("#close_server_message").on("click", function () {
        $(".error-server-block").addClass("invisible");
    });

    $("#complete").button().on('click', function() {
        completeOrder();
    });

    $("#block_check").prop('checked', STATUS_DRIVER.name != "busy").on("click", function() {
        setStatus();
    });
});

