/**
 * Created by dsing on 25.09.14.
 */

var driverLicence;

function Description(name) {
    this.name = name;
}

function Driver(id,snm,licence,status) {
    this.id = id;
    this.snm = snm;
    this.licence = licence;
    this.status = new Description(status);
}

function Truck(id,regNumber,countDrivers,classCapacity,status) {
    this.id = id;
    this.regNumber = regNumber;
    this.countDrivers = countDrivers;
    this.classCapacity = new Description(classCapacity);
    this.status = new Description(status);
}

function Stuff(id,gps,weight,name,status) {
    this.id= id;
    this.gps = gps;
    this.weight = weight;
    this.name = name;
    this.status = status;
}


function DriverExtend(id,snm,licence,status,numOrder,truck,stuffs,coDrivers) {
    this.id = id;
    this.snm = snm;
    this.licence = licence;
    this.status = new Description(status);
    this.truck = truck;
    this.stuffs = stuffs;
    this.coDrivers = coDrivers;
    this.numOrder = numOrder;
}