package ru.tsystems.logiweb.service.impl;

import org.junit.Before;
import org.junit.Test;
import ru.tsystems.logiweb.entities.Driver;
import ru.tsystems.logiweb.entities.Order;
import ru.tsystems.logiweb.entities.StatusDriver;

/**
 * Created by dsing on 25.09.14.
 */
public class OrderServiceImplTest {

    private OrderServiceImpl orderService;

    @Before
    public void setUp() throws Exception {
        orderService = new OrderServiceImpl();
    }

    @Test
    public void isValidOrderTest() {
        Order order = new Order();
        assert !orderService.isValidOrder(order);
    }

    @Test
    public void isValidDriverStatusTest() {
        Driver driver = new Driver();
        StatusDriver statusDriver = new StatusDriver();
        statusDriver.setName("driving");
        driver.setStatus(statusDriver);
        assert !orderService.isValidDriverStatus(driver);
    }
}
