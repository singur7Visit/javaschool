package ru.tsystems.logiweb.controllers.driver;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

public class ViewsDriverControllerTest {

    @InjectMocks
    private ViewsDriverController viewsDriverController;

    private MockMvc mockMvc;

    @Before
    public void setUp() {

        MockitoAnnotations.initMocks(this);

        this.mockMvc = MockMvcBuilders.standaloneSetup(viewsDriverController).build();

    }

    @Test
    public void testIndex() throws Exception {
        mockMvc.perform(get("/driver/index"))
                .andExpect(view().name("views/driver/index"))
                .andExpect(status().isOk());
    }

    @Test
    public void testRegDriver() throws Exception {
        mockMvc.perform(post("/driver/regDriver")
                .param("licence", "123"))
                .andExpect(status().is(302))
                .andExpect(request().sessionAttribute("driver", "123"));
    }


    @Test
    public void testManager() throws Exception {
        mockMvc.perform(get("/driver/manage").sessionAttr("driver","123"))
                .andExpect(view().name("views/driver/manage"))
                .andExpect(status().isOk());
        mockMvc.perform(get("/driver/manage"))
                .andExpect(status().is(302));
    }
}
