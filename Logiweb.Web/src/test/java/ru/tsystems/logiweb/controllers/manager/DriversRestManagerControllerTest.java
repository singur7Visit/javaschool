package ru.tsystems.logiweb.controllers.manager;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import ru.tsystems.logiweb.entities.Driver;
import ru.tsystems.logiweb.service.DriverService;
import ru.tsystems.logiweb.service.OrderService;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class DriversRestManagerControllerTest {

    @Mock
    private OrderService orderService;

    @Mock
    private DriverService driverService;

    @InjectMocks
    private DriversRestManagerController driversRestManagerController;

    private MockMvc mockMvc;

    @Before
    public void setUp() {

        MockitoAnnotations.initMocks(this);

        this.mockMvc = MockMvcBuilders.standaloneSetup(driversRestManagerController).build();

    }

    @Test
    public void testGetDrivers() throws Exception {
        List<Driver> drivers = new ArrayList<>();
        Driver driver = new Driver();
        driver.setId(0);
        drivers.add(driver);
        when(driverService.getAllDrivers()).thenReturn(drivers);
        this.mockMvc.perform(get("/manager/drivers/all"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$",hasSize(1)));
    }

    @Test
    public void testAddDriver() throws Exception {

    }

    @Test
    public void testRemoveDriver() throws Exception {

    }
}
