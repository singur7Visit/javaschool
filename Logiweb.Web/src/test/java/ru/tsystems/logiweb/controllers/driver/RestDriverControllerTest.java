package ru.tsystems.logiweb.controllers.driver;

import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import ru.tsystems.logiweb.entities.Driver;
import ru.tsystems.logiweb.entities.StatusDriver;
import ru.tsystems.logiweb.service.DriverService;
import ru.tsystems.logiweb.service.OrderService;

import static org.mockito.Matchers.anyLong;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

public class RestDriverControllerTest {

    @Mock
    private OrderService orderService;

    @Mock
    private DriverService driverService;

    @InjectMocks
    private RestDriverController restDriverController;

    private MockMvc mockMvc;

    @Before
    public void setUp() {

        MockitoAnnotations.initMocks(this);

        this.mockMvc = MockMvcBuilders.standaloneSetup(restDriverController).build();

    }

    @Test
    public void testGetInfo() throws Exception {
        Driver driver = new Driver();
        StatusDriver statusDriver = new StatusDriver();
        statusDriver.setName("free");
        driver.setSNM("122"); driver.setLicence("123"); driver.setStatus(statusDriver);
        when(driverService.findByLicence("123")).thenReturn(driver);
        mockMvc.perform(get("/driver/{licence}/info", "123").accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("driver.licence").value("123"));
    }

    @Test
    public void testDeliverStuff() throws Exception {
        when(orderService.deliveredStuff(anyLong())).thenReturn(true);
        mockMvc.perform(put("/driver/stuff/{id}/deliver", 1L).accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("type").value(0));
    }
}
