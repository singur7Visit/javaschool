DROP DATABASE IF EXISTS logiweb;

CREATE DATABASE logiweb CHARSET UTF8 COLLATE utf8_general_ci;

USE logiweb;

CREATE TABLE classes_capacity (
  id   INT         NOT NULL AUTO_INCREMENT,
  name VARCHAR(20) NOT NULL UNIQUE,
  PRIMARY KEY (id)
)
  ENGINE =InnoDB
  CHARACTER SET =UTF8;

CREATE TABLE statuses_order (
  id   INT NOT NULL AUTO_INCREMENT,
  name VARCHAR(20) UNIQUE,
  PRIMARY KEY (id)
)
  ENGINE =InnoDB
  CHARACTER SET =UTF8;

CREATE TABLE roles (
  id             INT NOT NULL AUTO_INCREMENT,
  name           VARCHAR(20) UNIQUE,
  manage_manager BOOLEAN,
  manage_orders  BOOLEAN,
  manage_drivers BOOLEAN,
  PRIMARY KEY (id)
)
  ENGINE =InnoDB
  CHARACTER SET =UTF8;

CREATE TABLE users (
  id       INT NOT NULL AUTO_INCREMENT,
  login    VARCHAR(20) UNIQUE,
  password varchar (30),
  id_role  INT NOT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (id_role) REFERENCES roles (id)
    ON UPDATE CASCADE
    ON DELETE CASCADE
)
  ENGINE =InnoDB
  CHARACTER SET =UTF8;

CREATE TABLE orders (
  id        INT    NOT NULL AUTO_INCREMENT,
  num_order BIGINT UNIQUE,
  id_status INT,
  PRIMARY KEY (id),
  FOREIGN KEY (id_status) REFERENCES statuses_order (id)
    ON UPDATE CASCADE
    ON DELETE SET NULL
)
  ENGINE =InnoDB
  CHARACTER SET =UTF8;

CREATE TABLE statuses_trucks (
  id INT NOT NULL AUTO_INCREMENT,
  name varchar(50) UNIQUE,
  PRIMARY KEY (id)
)
  ENGINE =InnoDB
  CHARACTER SET =UTF8;


CREATE TABLE trucks (
  id            INT        NOT NULL AUTO_INCREMENT,
  regnumber     VARCHAR(7) NOT NULL UNIQUE,
  count_drivers INT DEFAULT 0,
  id_class      INT        NOT NULL,
  id_order      INT,
  id_status     INT,
  PRIMARY KEY (id),
  FOREIGN KEY (id_class) REFERENCES classes_capacity (id)
    ON UPDATE CASCADE
    ON DELETE RESTRICT,
  FOREIGN KEY (id_order) REFERENCES orders (id)
    ON UPDATE CASCADE
    ON DELETE SET NULL,
  FOREIGN KEY (id_status) REFERENCES statuses_trucks (id)
    ON UPDATE CASCADE
    ON DELETE SET NULL
)
  ENGINE =InnoDB
  CHARACTER SET =UTF8;


CREATE TABLE statuses_drivers (
  id INT NOT NULL AUTO_INCREMENT,
  name varchar(50) UNIQUE,
  PRIMARY KEY (id)
)
  ENGINE =InnoDB
  CHARACTER SET =UTF8;

CREATE TABLE drivers (
  id              INT     NOT NULL AUTO_INCREMENT,
  driving_licence varchar (11) NOT NULL UNIQUE,
  SNM             VARCHAR(128), /* Surname Name Middlename*/
  id_status          INT,
  id_truck      INT DEFAULT NULL,
  id_order        INT DEFAULT NULL,
  PRIMARY KEY (id),
  FOREIGN KEY (id_truck) REFERENCES trucks (id)
    ON UPDATE CASCADE
    ON DELETE SET NULL,
  FOREIGN KEY (id_order) REFERENCES orders (id)
    ON UPDATE CASCADE
    ON DELETE SET NULL,
  FOREIGN KEY (id_status) REFERENCES statuses_drivers (id)
    ON UPDATE CASCADE
    ON DELETE SET NULL
)
  ENGINE =InnoDB
  CHARACTER SET =UTF8;

CREATE TABLE stuffs (
  id     INT NOT NULL AUTO_INCREMENT,
  gps    VARCHAR(100),
  name   VARCHAR(255),
  weight DECIMAL(20, 3) UNSIGNED,
  id_order INT,
  status BOOLEAN,
  PRIMARY KEY (id),
  FOREIGN KEY (id_order) REFERENCES orders(id)
    ON UPDATE CASCADE
    ON DELETE CASCADE
)
  ENGINE =InnoDB
  CHARACTER SET =UTF8;


